﻿namespace OrigineIT.SaveAChildToday.Common.Enumerables
{
    public enum ChildStatus
    {
        Missing, Found
    }
}
