﻿namespace OrigineIT.SaveAChildToday.Common.Enumerables
{
    public enum UserRole
    {
        User, Administrator, System, Anonymous
    }
}
