﻿namespace OrigineIT.SaveAChildToday.Common.Enumerables
{
    public enum LogType
    {
        System, Audit, Error, Debug
    }
}
