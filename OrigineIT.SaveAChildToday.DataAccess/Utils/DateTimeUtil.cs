﻿using System;
using System.Text.RegularExpressions;

namespace OrigineIT.SaveAChildToday.DataAccess.Utils
{
    public static class DateTimeUtil
    {
        private static readonly String[] EvalPatterns = new String[]
            {
                // Regex tool: http://utilitymill.com/utility/Regex_For_Range

                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})-(0*[1-9]|1[0-2])-(0*[1-9]|[12][0-9]|3[01])::(0*[0-9]|1[0-9]|2[0-3]):(0*[0-9]|[1-5][0-9]):(0*[0-9]|[1-5][0-9])",
                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})-(0*[1-9]|1[0-2])-(0*[1-9]|[12][0-9]|3[01])::(0*[0-9]|1[0-9]|2[0-3]):(0*[0-9]|[1-5][0-9])",
                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})-(0*[1-9]|1[0-2])-(0*[1-9]|[12][0-9]|3[01])::(0*[0-9]|1[0-9]|2[0-3])",
                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})-(0*[1-9]|1[0-2])-(0*[1-9]|[12][0-9]|3[01])",
                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})-(0*[1-9]|1[0-2])",
                @"(lt|le|eq|ne|ge|gt)::(0*[1-9][0-9]{0,3})",
                @"(lt|le|eq|ne|ge|gt)::now"
            };

        public static readonly DateTime UnixEpochDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        public static DateTime GetSimpleDateTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);
        }

        public static Boolean Eval(String arg, DateTime dateTime)
        {
            foreach (String m in EvalPatterns)
            {
                Match match = Regex.Match(arg, m, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                if (match.Success)
                {
                    GroupCollection matchGroups = match.Groups;
                    int matchCount = match.Groups.Count - 1;

                    DateTime argDateTime;
                    DateTime cmpDateTime;

                    String cmpOperator = matchGroups[1].Value.Trim().ToLower();

                    switch (matchCount)
                    {
                        case 7:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    Int32.Parse(matchGroups[3].Value),
                                    Int32.Parse(matchGroups[4].Value),
                                    Int32.Parse(matchGroups[5].Value),
                                    Int32.Parse(matchGroups[6].Value),
                                    Int32.Parse(matchGroups[7].Value));

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    dateTime.Month,
                                    dateTime.Day,
                                    dateTime.Hour,
                                    dateTime.Minute,
                                    dateTime.Second);
                            break;

                        case 6:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    Int32.Parse(matchGroups[3].Value),
                                    Int32.Parse(matchGroups[4].Value),
                                    Int32.Parse(matchGroups[5].Value),
                                    Int32.Parse(matchGroups[6].Value),
                                    0);

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    dateTime.Month,
                                    dateTime.Day,
                                    dateTime.Hour,
                                    dateTime.Minute,
                                    0);
                            break;

                        case 5:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    Int32.Parse(matchGroups[3].Value),
                                    Int32.Parse(matchGroups[4].Value),
                                    Int32.Parse(matchGroups[5].Value),
                                    0,
                                    0);

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    dateTime.Month,
                                    dateTime.Day,
                                    dateTime.Hour,
                                    0,
                                    0);
                            break;

                        case 4:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    Int32.Parse(matchGroups[3].Value),
                                    Int32.Parse(matchGroups[4].Value));

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    dateTime.Month,
                                    dateTime.Day);
                            break;

                        case 3:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    Int32.Parse(matchGroups[3].Value),
                                    1);

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    dateTime.Month,
                                    1);
                            break;

                        case 2:
                            argDateTime =
                                new DateTime(
                                    Int32.Parse(matchGroups[2].Value),
                                    1,
                                    1);

                            cmpDateTime =
                                new DateTime(
                                    dateTime.Year,
                                    1,
                                    1);
                            break;

                        case 1:
                            argDateTime = DateTime.UtcNow;
                            cmpDateTime = dateTime;
                            break;

                        default:
                            return false;
                    }

                    switch (cmpOperator)
                    {
                        case "lt":
                            return (cmpDateTime.CompareTo(argDateTime) < 0);

                        case "le":
                            return (cmpDateTime.CompareTo(argDateTime) <= 0);

                        case "eq":
                            return (cmpDateTime.CompareTo(argDateTime) == 0);

                        case "ne":
                            return (cmpDateTime.CompareTo(argDateTime) != 0);

                        case "ge":
                            return (cmpDateTime.CompareTo(argDateTime) >= 0);

                        case "gt":
                            return (cmpDateTime.CompareTo(argDateTime) > 0);
                    }
                }
            }

            return false;
        }
    }
}
