﻿namespace OrigineIT.SaveAChildToday.DataAccess.Enumerables
{
    public enum SchemaConfiguration
    {
        NoChange, Update, Rebuild
    }
}
