﻿using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Comment : IEntityKey<String>
    {
        public virtual String ID { get; protected set; }
        public virtual User Owner { get; set; }
        public virtual String Message { get; set; }
        public virtual DateTime CreationDateTime { get; set; }

        public Comment()
        {
            CreationDateTime = DateTime.UtcNow;
        }
    }
}
