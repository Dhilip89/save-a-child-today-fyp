﻿using Iesi.Collections.Generic;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Photo : IEntityKey<String>
    {
        private ISet<Comment> comments;
        private ISet<Face> faces;

        public virtual String ID { get; set; }
        public virtual Byte[] Image { get; set; }
        public virtual Int32 Width { get; set; }
        public virtual Int32 Height { get; set; }
        public virtual User Owner { get; set; }
        public virtual Face MarkedFace { get; set; }
        public virtual Child MarkedChild { get; set; }
        public virtual String Caption { get; set; }
        public virtual String Description { get; set; }
        public virtual Double Longitude { get; set; }
        public virtual Double Latitude { get; set; }
        public virtual String Country { get; set; }
        public virtual DateTime CaptureDateTime { get; set; }
        public virtual DateTime UploadDateTime { get; set; }
        public virtual DateTime LastUpdateDateTime { get; set; }

        public virtual ISet<Comment> Comments
        {
            get { return comments ?? (comments = new HashedSet<Comment>()); }
            protected set { comments = value; }
        }

        public virtual ISet<Face> Faces
        {
            get { return faces ?? (faces = new HashedSet<Face>()); }
            protected set { faces = value; }
        }

        public Photo()
        {
            UploadDateTime = DateTimeUtil.UnixEpochDateTime;
            CaptureDateTime = DateTimeUtil.UnixEpochDateTime;
            LastUpdateDateTime = DateTimeUtil.UnixEpochDateTime;
        }
    }
}
