﻿using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Log : IEntityKey<Int64>
    {
        public virtual Int64 ID { get; protected set; }
        public virtual LogType Type { get; set; }
        public virtual String Source { get; set; }
        public virtual String Message { get; set; }
        public virtual DateTime CreationDateTime { get; set; }

        public Log()
        {
            CreationDateTime = DateTime.UtcNow;
        }
    }
}
