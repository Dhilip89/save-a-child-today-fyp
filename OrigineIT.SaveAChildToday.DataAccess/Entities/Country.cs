﻿using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;
using System.Runtime.Serialization;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Country : IEntityKey<String>
    {
        public virtual String ID { get; set; }
        public virtual String Name { get; set; }
        public virtual Double Longitude { get; set; }
        public virtual Double Latitude { get; set; }
    }
}
