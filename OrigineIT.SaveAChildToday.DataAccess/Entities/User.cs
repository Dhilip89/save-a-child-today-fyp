﻿using Iesi.Collections.Generic;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class User : IEntityKey<String>
    {
        private ISet<Child> children;
        private ISet<Comment> comments;
        private ISet<Photo> photos;


        public virtual String ID { get; protected set; }

        // Personal Information
        public virtual String Name { get; set; }
        public virtual Byte[] ProfilePicture { get; set; }
        public virtual String ContactNumber { get; set; }
        public virtual String Website { get; set; }
        //public virtual Country Country { get; set; }
        public virtual String Country { get; set; }

        // Account Information
        public virtual String Email { get; set; }
        public virtual String HashedPassword { get; set; }
        public virtual String VerificationToken { get; protected set; }
        public virtual Boolean Verified { get; set; }
        public virtual DateTime CreationDateTime { get; set; }
        public virtual DateTime VerificationDateTime { get; set; }
        public virtual DateTime LastLoginDateTime { get; set; }
        public virtual DateTime LastUpdateDateTime { get; set; }
        public virtual UserRole Role { get; set; }

        // User's Data
        public virtual ISet<Child> Children
        {
            get { return children ?? (children = new HashedSet<Child>()); }
            protected set { children = value; }
        }

        public virtual ISet<Comment> Comments
        {
            get { return comments ?? (comments = new HashedSet<Comment>()); }
            protected set { comments = value; }
        }

        public virtual ISet<Photo> Photos
        {
            get { return photos ?? (photos = new HashedSet<Photo>()); }
            protected set { photos = value; }
        }

        public User()
        {
            CreationDateTime = DateTime.UtcNow;
            LastUpdateDateTime = DateTimeUtil.UnixEpochDateTime;
            LastLoginDateTime = DateTimeUtil.UnixEpochDateTime;
            VerificationDateTime = DateTimeUtil.UnixEpochDateTime;
            RenewVerificationToken();
        }

        public virtual User RenewVerificationToken()
        {
            VerificationToken = Guid.NewGuid().ToString("N");
            return this;
        }
    }
}
