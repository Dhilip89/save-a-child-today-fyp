﻿using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Face : IEntityKey<String>
    {
        public virtual String ID { get; protected set; }
        public virtual Byte[] Image { get; set; }
        public virtual Int32 Width { get; set; }
        public virtual Int32 Height { get; set; }
        public virtual Int32 OffsetX { get; set; }
        public virtual Int32 OffsetY { get; set; }
        public virtual Photo SourcePhoto { get; set; }
        public virtual Boolean Valid { get; set; }
        public virtual Boolean Marked { get; set; }
        public virtual DateTime CreationDateTime { get; set; }

        public Face()
        {
            CreationDateTime = DateTime.UtcNow;
        }
    }
}
