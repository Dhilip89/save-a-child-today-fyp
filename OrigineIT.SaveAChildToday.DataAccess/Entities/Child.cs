﻿using Iesi.Collections.Generic;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Entities
{
    public class Child : IEntityKey<String>
    {
        private ISet<Comment> comments;

        public virtual String ID { get; protected set; }
        public virtual String Name { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual String Description { get; set; }
        public virtual Photo ProfilePicture { get; set; }
        public virtual ChildStatus Status { get; set; }
        public virtual String MissingDescription { get; set; }
        public virtual String MissingCountry { get; set; }
        public virtual DateTime MissingDateTime { get; set; }
        public virtual String FoundDescription { get; set; }
        public virtual String FoundCountry { get; set; }
        public virtual DateTime FoundDateTime { get; set; }
        public virtual DateTime CreationDateTime { get; set; }
        public virtual DateTime LastUpdateDateTime { get; set; }
        public virtual User Owner { get; set; }

        public virtual ISet<Comment> Comments
        {
            get { return comments ?? (comments = new HashedSet<Comment>()); }
            protected set { comments = value; }
        }

        public Child()
        {
            Status = ChildStatus.Missing;
            Birthday = DateTimeUtil.UnixEpochDateTime;
            MissingDateTime = DateTimeUtil.UnixEpochDateTime;
            FoundDateTime = DateTimeUtil.UnixEpochDateTime;
            CreationDateTime = DateTime.UtcNow;
            LastUpdateDateTime = DateTimeUtil.UnixEpochDateTime;
        }

        public virtual Int32 GetAgeByDate(DateTime date)
        {
            Int32 a = (date.Year * 100 + date.Month) * 100 + date.Day;
            Int32 b = (Birthday.Year * 100 + Birthday.Month) * 100 + Birthday.Day;

            return (a - b) / 10000;
        }
    }
}
