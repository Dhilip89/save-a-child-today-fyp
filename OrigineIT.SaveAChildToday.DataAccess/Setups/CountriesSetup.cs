﻿using NHibernate;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Setups
{
    public class CountriesSetup : ISetup
    {
        private ISessionFactory SessionFactory { get; set; }
        private NHRepository<String, Country> Repository { get; set; }
        private NHUnitOfWork UnitOfWork { get; set; }

        public CountriesSetup(ISessionFactory sessionFactory)
        {
            SessionFactory = sessionFactory;
        }

        public void Initialize()
        {
            UnitOfWork = new NHUnitOfWork(SessionFactory);
            Repository = new NHRepository<String, Country>(UnitOfWork);
        }

        public void Perform()
        {
            Repository.Add(new Country { ID = "AD", Name = "Andorra", Longitude = 1.601554, Latitude = 42.546245 });
            Repository.Add(new Country { ID = "AE", Name = "United Arab Emirates", Longitude = 53.847818, Latitude = 23.424076 });
            Repository.Add(new Country { ID = "AF", Name = "Afghanistan", Longitude = 67.709953, Latitude = 33.93911 });
            Repository.Add(new Country { ID = "AG", Name = "Antigua and Barbuda", Longitude = -61.796428, Latitude = 17.060816 });
            Repository.Add(new Country { ID = "AI", Name = "Anguilla", Longitude = -63.068615, Latitude = 18.220554 });
            Repository.Add(new Country { ID = "AL", Name = "Albania", Longitude = 20.168331, Latitude = 41.153332 });
            Repository.Add(new Country { ID = "AM", Name = "Armenia", Longitude = 45.038189, Latitude = 40.069099 });
            Repository.Add(new Country { ID = "AN", Name = "Netherlands Antilles", Longitude = -69.060087, Latitude = 12.226079 });
            Repository.Add(new Country { ID = "AO", Name = "Angola", Longitude = 17.873887, Latitude = -11.202692 });
            Repository.Add(new Country { ID = "AQ", Name = "Antarctica", Longitude = -0.071389, Latitude = -75.250973 });
            Repository.Add(new Country { ID = "AR", Name = "Argentina", Longitude = -63.616672, Latitude = -38.416097 });
            Repository.Add(new Country { ID = "AS", Name = "American Samoa", Longitude = -170.132217, Latitude = -14.270972 });
            Repository.Add(new Country { ID = "AT", Name = "Austria", Longitude = 14.550072, Latitude = 47.516231 });
            Repository.Add(new Country { ID = "AU", Name = "Australia", Longitude = 133.775136, Latitude = -25.274398 });
            Repository.Add(new Country { ID = "AW", Name = "Aruba", Longitude = -69.968338, Latitude = 12.52111 });
            Repository.Add(new Country { ID = "AZ", Name = "Azerbaijan", Longitude = 47.576927, Latitude = 40.143105 });
            Repository.Add(new Country { ID = "BA", Name = "Bosnia and Herzegovina", Longitude = 17.679076, Latitude = 43.915886 });
            Repository.Add(new Country { ID = "BB", Name = "Barbados", Longitude = -59.543198, Latitude = 13.193887 });
            Repository.Add(new Country { ID = "BD", Name = "Bangladesh", Longitude = 90.356331, Latitude = 23.684994 });
            Repository.Add(new Country { ID = "BE", Name = "Belgium", Longitude = 4.469936, Latitude = 50.503887 });
            Repository.Add(new Country { ID = "BF", Name = "Burkina Faso", Longitude = -1.561593, Latitude = 12.238333 });
            Repository.Add(new Country { ID = "BG", Name = "Bulgaria", Longitude = 25.48583, Latitude = 42.733883 });
            Repository.Add(new Country { ID = "BH", Name = "Bahrain", Longitude = 50.637772, Latitude = 25.930414 });
            Repository.Add(new Country { ID = "BI", Name = "Burundi", Longitude = 29.918886, Latitude = -3.373056 });
            Repository.Add(new Country { ID = "BJ", Name = "Benin", Longitude = 2.315834, Latitude = 9.30769 });
            Repository.Add(new Country { ID = "BM", Name = "Bermuda", Longitude = -64.75737, Latitude = 32.321384 });
            Repository.Add(new Country { ID = "BN", Name = "Brunei", Longitude = 114.727669, Latitude = 4.535277 });
            Repository.Add(new Country { ID = "BO", Name = "Bolivia", Longitude = -63.588653, Latitude = -16.290154 });
            Repository.Add(new Country { ID = "BR", Name = "Brazil", Longitude = -51.92528, Latitude = -14.235004 });
            Repository.Add(new Country { ID = "BS", Name = "Bahamas", Longitude = -77.39628, Latitude = 25.03428 });
            Repository.Add(new Country { ID = "BT", Name = "Bhutan", Longitude = 90.433601, Latitude = 27.514162 });
            Repository.Add(new Country { ID = "BV", Name = "Bouvet Island", Longitude = 3.413194, Latitude = -54.423199 });
            Repository.Add(new Country { ID = "BW", Name = "Botswana", Longitude = 24.684866, Latitude = -22.328474 });
            Repository.Add(new Country { ID = "BY", Name = "Belarus", Longitude = 27.953389, Latitude = 53.709807 });
            Repository.Add(new Country { ID = "BZ", Name = "Belize", Longitude = -88.49765, Latitude = 17.189877 });
            Repository.Add(new Country { ID = "CA", Name = "Canada", Longitude = -106.346771, Latitude = 56.130366 });
            Repository.Add(new Country { ID = "CC", Name = "Cocos (Keeling) Islands", Longitude = 96.870956, Latitude = -12.164165 });
            Repository.Add(new Country { ID = "CD", Name = "Democratic Republic of the Congo", Longitude = 21.758664, Latitude = -4.038333 });
            Repository.Add(new Country { ID = "CF", Name = "Central African Republic", Longitude = 20.939444, Latitude = 6.611111 });
            Repository.Add(new Country { ID = "CG", Name = "Republic of the Congo", Longitude = 15.827659, Latitude = -0.228021 });
            Repository.Add(new Country { ID = "CH", Name = "Switzerland", Longitude = 8.227512, Latitude = 46.818188 });
            Repository.Add(new Country { ID = "CI", Name = "Côte d'Ivoire", Longitude = -5.54708, Latitude = 7.539989 });
            Repository.Add(new Country { ID = "CK", Name = "Cook Islands", Longitude = -159.777671, Latitude = -21.236736 });
            Repository.Add(new Country { ID = "CL", Name = "Chile", Longitude = -71.542969, Latitude = -35.675147 });
            Repository.Add(new Country { ID = "CM", Name = "Cameroon", Longitude = 12.354722, Latitude = 7.369722 });
            Repository.Add(new Country { ID = "CN", Name = "China", Longitude = 104.195397, Latitude = 35.86166 });
            Repository.Add(new Country { ID = "CO", Name = "Colombia", Longitude = -74.297333, Latitude = 4.570868 });
            Repository.Add(new Country { ID = "CR", Name = "Costa Rica", Longitude = -83.753428, Latitude = 9.748917 });
            Repository.Add(new Country { ID = "CU", Name = "Cuba", Longitude = -77.781167, Latitude = 21.521757 });
            Repository.Add(new Country { ID = "CV", Name = "Cape Verde", Longitude = -24.013197, Latitude = 16.002082 });
            Repository.Add(new Country { ID = "CX", Name = "Christmas Island", Longitude = 105.690449, Latitude = -10.447525 });
            Repository.Add(new Country { ID = "CY", Name = "Cyprus", Longitude = 33.429859, Latitude = 35.126413 });
            Repository.Add(new Country { ID = "CZ", Name = "Czech Republic", Longitude = 15.472962, Latitude = 49.817492 });
            Repository.Add(new Country { ID = "DE", Name = "Germany", Longitude = 10.451526, Latitude = 51.165691 });
            Repository.Add(new Country { ID = "DJ", Name = "Djibouti", Longitude = 42.590275, Latitude = 11.825138 });
            Repository.Add(new Country { ID = "DK", Name = "Denmark", Longitude = 9.501785, Latitude = 56.26392 });
            Repository.Add(new Country { ID = "DM", Name = "Dominica", Longitude = -61.370976, Latitude = 15.414999 });
            Repository.Add(new Country { ID = "DO", Name = "Dominican Republic", Longitude = -70.162651, Latitude = 18.735693 });
            Repository.Add(new Country { ID = "DZ", Name = "Algeria", Longitude = 1.659626, Latitude = 28.033886 });
            Repository.Add(new Country { ID = "EC", Name = "Ecuador", Longitude = -78.183406, Latitude = -1.831239 });
            Repository.Add(new Country { ID = "EE", Name = "Estonia", Longitude = 25.013607, Latitude = 58.595272 });
            Repository.Add(new Country { ID = "EG", Name = "Egypt", Longitude = 30.802498, Latitude = 26.820553 });
            Repository.Add(new Country { ID = "EH", Name = "Western Sahara", Longitude = -12.885834, Latitude = 24.215527 });
            Repository.Add(new Country { ID = "ER", Name = "Eritrea", Longitude = 39.782334, Latitude = 15.179384 });
            Repository.Add(new Country { ID = "ES", Name = "Spain", Longitude = -3.74922, Latitude = 40.463667 });
            Repository.Add(new Country { ID = "ET", Name = "Ethiopia", Longitude = 40.489673, Latitude = 9.145 });
            Repository.Add(new Country { ID = "FI", Name = "Finland", Longitude = 25.748151, Latitude = 61.92411 });
            Repository.Add(new Country { ID = "FJ", Name = "Fiji", Longitude = 179.414413, Latitude = -16.578193 });
            Repository.Add(new Country { ID = "FK", Name = "Falkland Islands", Longitude = -59.523613, Latitude = -51.796253 });
            Repository.Add(new Country { ID = "FM", Name = "Micronesia", Longitude = 150.550812, Latitude = 7.425554 });
            Repository.Add(new Country { ID = "FO", Name = "Faroe Islands", Longitude = -6.911806, Latitude = 61.892635 });
            Repository.Add(new Country { ID = "FR", Name = "France", Longitude = 2.213749, Latitude = 46.227638 });
            Repository.Add(new Country { ID = "GA", Name = "Gabon", Longitude = 11.609444, Latitude = -0.803689 });
            Repository.Add(new Country { ID = "GB", Name = "United Kingdom", Longitude = -3.435973, Latitude = 55.378051 });
            Repository.Add(new Country { ID = "GD", Name = "Grenada", Longitude = -61.604171, Latitude = 12.262776 });
            Repository.Add(new Country { ID = "GE", Name = "Georgia", Longitude = 43.356892, Latitude = 42.315407 });
            Repository.Add(new Country { ID = "GF", Name = "French Guiana", Longitude = -53.125782, Latitude = 3.933889 });
            Repository.Add(new Country { ID = "GG", Name = "Guernsey", Longitude = -2.585278, Latitude = 49.465691 });
            Repository.Add(new Country { ID = "GH", Name = "Ghana", Longitude = -1.023194, Latitude = 7.946527 });
            Repository.Add(new Country { ID = "GI", Name = "Gibraltar", Longitude = -5.345374, Latitude = 36.137741 });
            Repository.Add(new Country { ID = "GL", Name = "Greenland", Longitude = -42.604303, Latitude = 71.706936 });
            Repository.Add(new Country { ID = "GM", Name = "Gambia", Longitude = -15.310139, Latitude = 13.443182 });
            Repository.Add(new Country { ID = "GN", Name = "Guinea", Longitude = -9.696645, Latitude = 9.945587 });
            Repository.Add(new Country { ID = "GP", Name = "Guadeloupe", Longitude = -62.067641, Latitude = 16.995971 });
            Repository.Add(new Country { ID = "GQ", Name = "Equatorial Guinea", Longitude = 10.267895, Latitude = 1.650801 });
            Repository.Add(new Country { ID = "GR", Name = "Greece", Longitude = 21.824312, Latitude = 39.074208 });
            Repository.Add(new Country { ID = "GS", Name = "South Georgia and the South Sandwich Islands", Longitude = -36.587909, Latitude = -54.429579 });
            Repository.Add(new Country { ID = "GT", Name = "Guatemala", Longitude = -90.230759, Latitude = 15.783471 });
            Repository.Add(new Country { ID = "GU", Name = "Guam", Longitude = 144.793731, Latitude = 13.444304 });
            Repository.Add(new Country { ID = "GW", Name = "Guinea-Bissau", Longitude = -15.180413, Latitude = 11.803749 });
            Repository.Add(new Country { ID = "GY", Name = "Guyana", Longitude = -58.93018, Latitude = 4.860416 });
            Repository.Add(new Country { ID = "GZ", Name = "Gaza Strip", Longitude = 34.308825, Latitude = 31.354676 });
            Repository.Add(new Country { ID = "HK", Name = "Hong Kong", Longitude = 114.109497, Latitude = 22.396428 });
            Repository.Add(new Country { ID = "HM", Name = "Heard Island and McDonald Islands", Longitude = 73.504158, Latitude = -53.08181 });
            Repository.Add(new Country { ID = "HN", Name = "Honduras", Longitude = -86.241905, Latitude = 15.199999 });
            Repository.Add(new Country { ID = "HR", Name = "Croatia", Longitude = 15.2, Latitude = 45.1 });
            Repository.Add(new Country { ID = "HT", Name = "Haiti", Longitude = -72.285215, Latitude = 18.971187 });
            Repository.Add(new Country { ID = "HU", Name = "Hungary", Longitude = 19.503304, Latitude = 47.162494 });
            Repository.Add(new Country { ID = "ID", Name = "Indonesia", Longitude = 113.921327, Latitude = -0.789275 });
            Repository.Add(new Country { ID = "IE", Name = "Ireland", Longitude = -8.24389, Latitude = 53.41291 });
            Repository.Add(new Country { ID = "IL", Name = "Israel", Longitude = 34.851612, Latitude = 31.046051 });
            Repository.Add(new Country { ID = "IM", Name = "Isle of Man", Longitude = -4.548056, Latitude = 54.236107 });
            Repository.Add(new Country { ID = "IN", Name = "India", Longitude = 78.96288, Latitude = 20.593684 });
            Repository.Add(new Country { ID = "IO", Name = "British Indian Ocean Territory", Longitude = 71.876519, Latitude = -6.343194 });
            Repository.Add(new Country { ID = "IQ", Name = "Iraq", Longitude = 43.679291, Latitude = 33.223191 });
            Repository.Add(new Country { ID = "IR", Name = "Iran", Longitude = 53.688046, Latitude = 32.427908 });
            Repository.Add(new Country { ID = "IS", Name = "Iceland", Longitude = -19.020835, Latitude = 64.963051 });
            Repository.Add(new Country { ID = "IT", Name = "Italy", Longitude = 12.56738, Latitude = 41.87194 });
            Repository.Add(new Country { ID = "JE", Name = "Jersey", Longitude = -2.13125, Latitude = 49.214439 });
            Repository.Add(new Country { ID = "JM", Name = "Jamaica", Longitude = -77.297508, Latitude = 18.109581 });
            Repository.Add(new Country { ID = "JO", Name = "Jordan", Longitude = 36.238414, Latitude = 30.585164 });
            Repository.Add(new Country { ID = "JP", Name = "Japan", Longitude = 138.252924, Latitude = 36.204824 });
            Repository.Add(new Country { ID = "KE", Name = "Kenya", Longitude = 37.906193, Latitude = -0.023559 });
            Repository.Add(new Country { ID = "KG", Name = "Kyrgyzstan", Longitude = 74.766098, Latitude = 41.20438 });
            Repository.Add(new Country { ID = "KH", Name = "Cambodia", Longitude = 104.990963, Latitude = 12.565679 });
            Repository.Add(new Country { ID = "KI", Name = "Kiribati", Longitude = -168.734039, Latitude = -3.370417 });
            Repository.Add(new Country { ID = "KM", Name = "Comoros", Longitude = 43.872219, Latitude = -11.875001 });
            Repository.Add(new Country { ID = "KN", Name = "Saint Kitts and Nevis", Longitude = -62.782998, Latitude = 17.357822 });
            Repository.Add(new Country { ID = "KP", Name = "North Korea", Longitude = 127.510093, Latitude = 40.339852 });
            Repository.Add(new Country { ID = "KR", Name = "South Korea", Longitude = 127.766922, Latitude = 35.907757 });
            Repository.Add(new Country { ID = "KW", Name = "Kuwait", Longitude = 47.481766, Latitude = 29.31166 });
            Repository.Add(new Country { ID = "KY", Name = "Cayman Islands", Longitude = -80.566956, Latitude = 19.513469 });
            Repository.Add(new Country { ID = "KZ", Name = "Kazakhstan", Longitude = 66.923684, Latitude = 48.019573 });
            Repository.Add(new Country { ID = "LA", Name = "Laos", Longitude = 102.495496, Latitude = 19.85627 });
            Repository.Add(new Country { ID = "LB", Name = "Lebanon", Longitude = 35.862285, Latitude = 33.854721 });
            Repository.Add(new Country { ID = "LC", Name = "Saint Lucia", Longitude = -60.978893, Latitude = 13.909444 });
            Repository.Add(new Country { ID = "LI", Name = "Liechtenstein", Longitude = 9.555373, Latitude = 47.166 });
            Repository.Add(new Country { ID = "LK", Name = "Sri Lanka", Longitude = 80.771797, Latitude = 7.873054 });
            Repository.Add(new Country { ID = "LR", Name = "Liberia", Longitude = -9.429499, Latitude = 6.428055 });
            Repository.Add(new Country { ID = "LS", Name = "Lesotho", Longitude = 28.233608, Latitude = -29.609988 });
            Repository.Add(new Country { ID = "LT", Name = "Lithuania", Longitude = 23.881275, Latitude = 55.169438 });
            Repository.Add(new Country { ID = "LU", Name = "Luxembourg", Longitude = 6.129583, Latitude = 49.815273 });
            Repository.Add(new Country { ID = "LV", Name = "Latvia", Longitude = 24.603189, Latitude = 56.879635 });
            Repository.Add(new Country { ID = "LY", Name = "Libya", Longitude = 17.228331, Latitude = 26.3351 });
            Repository.Add(new Country { ID = "MA", Name = "Morocco", Longitude = -7.09262, Latitude = 31.791702 });
            Repository.Add(new Country { ID = "MC", Name = "Monaco", Longitude = 7.412841, Latitude = 43.750298 });
            Repository.Add(new Country { ID = "MD", Name = "Moldova", Longitude = 28.369885, Latitude = 47.411631 });
            Repository.Add(new Country { ID = "ME", Name = "Montenegro", Longitude = 19.37439, Latitude = 42.708678 });
            Repository.Add(new Country { ID = "MG", Name = "Madagascar", Longitude = 46.869107, Latitude = -18.766947 });
            Repository.Add(new Country { ID = "MH", Name = "Marshall Islands", Longitude = 171.184478, Latitude = 7.131474 });
            Repository.Add(new Country { ID = "MK", Name = "Macedonia [FYROM]", Longitude = 21.745275, Latitude = 41.608635 });
            Repository.Add(new Country { ID = "ML", Name = "Mali", Longitude = -3.996166, Latitude = 17.570692 });
            Repository.Add(new Country { ID = "MM", Name = "Myanmar [Burma]", Longitude = 95.956223, Latitude = 21.913965 });
            Repository.Add(new Country { ID = "MN", Name = "Mongolia", Longitude = 103.846656, Latitude = 46.862496 });
            Repository.Add(new Country { ID = "MO", Name = "Macau", Longitude = 113.543873, Latitude = 22.198745 });
            Repository.Add(new Country { ID = "MP", Name = "Northern Mariana Islands", Longitude = 145.38469, Latitude = 17.33083 });
            Repository.Add(new Country { ID = "MQ", Name = "Martinique", Longitude = -61.024174, Latitude = 14.641528 });
            Repository.Add(new Country { ID = "MR", Name = "Mauritania", Longitude = -10.940835, Latitude = 21.00789 });
            Repository.Add(new Country { ID = "MS", Name = "Montserrat", Longitude = -62.187366, Latitude = 16.742498 });
            Repository.Add(new Country { ID = "MT", Name = "Malta", Longitude = 14.375416, Latitude = 35.937496 });
            Repository.Add(new Country { ID = "MU", Name = "Mauritius", Longitude = 57.552152, Latitude = -20.348404 });
            Repository.Add(new Country { ID = "MV", Name = "Maldives", Longitude = 73.22068, Latitude = 3.202778 });
            Repository.Add(new Country { ID = "MW", Name = "Malawi", Longitude = 34.301525, Latitude = -13.254308 });
            Repository.Add(new Country { ID = "MX", Name = "Mexico", Longitude = -102.552784, Latitude = 23.634501 });
            Repository.Add(new Country { ID = "MY", Name = "Malaysia", Longitude = 101.975766, Latitude = 4.210484 });
            Repository.Add(new Country { ID = "MZ", Name = "Mozambique", Longitude = 35.529562, Latitude = -18.665695 });
            Repository.Add(new Country { ID = "NA", Name = "Namibia", Longitude = 18.49041, Latitude = -22.95764 });
            Repository.Add(new Country { ID = "NC", Name = "New Caledonia", Longitude = 165.618042, Latitude = -20.904305 });
            Repository.Add(new Country { ID = "NE", Name = "Niger", Longitude = 8.081666, Latitude = 17.607789 });
            Repository.Add(new Country { ID = "NF", Name = "Norfolk Island", Longitude = 167.954712, Latitude = -29.040835 });
            Repository.Add(new Country { ID = "NG", Name = "Nigeria", Longitude = 8.675277, Latitude = 9.081999 });
            Repository.Add(new Country { ID = "NI", Name = "Nicaragua", Longitude = -85.207229, Latitude = 12.865416 });
            Repository.Add(new Country { ID = "NL", Name = "Netherlands", Longitude = 5.291266, Latitude = 52.132633 });
            Repository.Add(new Country { ID = "NO", Name = "Norway", Longitude = 8.468946, Latitude = 60.472024 });
            Repository.Add(new Country { ID = "NP", Name = "Nepal", Longitude = 84.124008, Latitude = 28.394857 });
            Repository.Add(new Country { ID = "NR", Name = "Nauru", Longitude = 166.931503, Latitude = -0.522778 });
            Repository.Add(new Country { ID = "NU", Name = "Niue", Longitude = -169.867233, Latitude = -19.054445 });
            Repository.Add(new Country { ID = "NZ", Name = "New Zealand", Longitude = 174.885971, Latitude = -40.900557 });
            Repository.Add(new Country { ID = "OM", Name = "Oman", Longitude = 55.923255, Latitude = 21.512583 });
            Repository.Add(new Country { ID = "PA", Name = "Panama", Longitude = -80.782127, Latitude = 8.537981 });
            Repository.Add(new Country { ID = "PE", Name = "Peru", Longitude = -75.015152, Latitude = -9.189967 });
            Repository.Add(new Country { ID = "PF", Name = "French Polynesia", Longitude = -149.406843, Latitude = -17.679742 });
            Repository.Add(new Country { ID = "PG", Name = "Papua New Guinea", Longitude = 143.95555, Latitude = -6.314993 });
            Repository.Add(new Country { ID = "PH", Name = "Philippines", Longitude = 121.774017, Latitude = 12.879721 });
            Repository.Add(new Country { ID = "PK", Name = "Pakistan", Longitude = 69.345116, Latitude = 30.375321 });
            Repository.Add(new Country { ID = "PL", Name = "Poland", Longitude = 19.145136, Latitude = 51.919438 });
            Repository.Add(new Country { ID = "PM", Name = "Saint Pierre and Miquelon", Longitude = -56.27111, Latitude = 46.941936 });
            Repository.Add(new Country { ID = "PN", Name = "Pitcairn Islands", Longitude = -127.439308, Latitude = -24.703615 });
            Repository.Add(new Country { ID = "PR", Name = "Puerto Rico", Longitude = -66.590149, Latitude = 18.220833 });
            Repository.Add(new Country { ID = "PS", Name = "Palestinian Territories", Longitude = 35.233154, Latitude = 31.952162 });
            Repository.Add(new Country { ID = "PT", Name = "Portugal", Longitude = -8.224454, Latitude = 39.399872 });
            Repository.Add(new Country { ID = "PW", Name = "Palau", Longitude = 134.58252, Latitude = 7.51498 });
            Repository.Add(new Country { ID = "PY", Name = "Paraguay", Longitude = -58.443832, Latitude = -23.442503 });
            Repository.Add(new Country { ID = "QA", Name = "Qatar", Longitude = 51.183884, Latitude = 25.354826 });
            Repository.Add(new Country { ID = "RE", Name = "Réunion", Longitude = 55.536384, Latitude = -21.115141 });
            Repository.Add(new Country { ID = "RO", Name = "Romania", Longitude = 24.96676, Latitude = 45.943161 });
            Repository.Add(new Country { ID = "RS", Name = "Serbia", Longitude = 21.005859, Latitude = 44.016521 });
            Repository.Add(new Country { ID = "RU", Name = "Russia", Longitude = 105.318756, Latitude = 61.52401 });
            Repository.Add(new Country { ID = "RW", Name = "Rwanda", Longitude = 29.873888, Latitude = -1.940278 });
            Repository.Add(new Country { ID = "SA", Name = "Saudi Arabia", Longitude = 45.079162, Latitude = 23.885942 });
            Repository.Add(new Country { ID = "SB", Name = "Solomon Islands", Longitude = 160.156194, Latitude = -9.64571 });
            Repository.Add(new Country { ID = "SC", Name = "Seychelles", Longitude = 55.491977, Latitude = -4.679574 });
            Repository.Add(new Country { ID = "SD", Name = "Sudan", Longitude = 30.217636, Latitude = 12.862807 });
            Repository.Add(new Country { ID = "SE", Name = "Sweden", Longitude = 18.643501, Latitude = 60.128161 });
            Repository.Add(new Country { ID = "SG", Name = "Singapore", Longitude = 103.819836, Latitude = 1.352083 });
            Repository.Add(new Country { ID = "SH", Name = "Saint Helena", Longitude = -10.030696, Latitude = -24.143474 });
            Repository.Add(new Country { ID = "SI", Name = "Slovenia", Longitude = 14.995463, Latitude = 46.151241 });
            Repository.Add(new Country { ID = "SJ", Name = "Svalbard and Jan Mayen", Longitude = 23.670272, Latitude = 77.553604 });
            Repository.Add(new Country { ID = "SK", Name = "Slovakia", Longitude = 19.699024, Latitude = 48.669026 });
            Repository.Add(new Country { ID = "SL", Name = "Sierra Leone", Longitude = -11.779889, Latitude = 8.460555 });
            Repository.Add(new Country { ID = "SM", Name = "San Marino", Longitude = 12.457777, Latitude = 43.94236 });
            Repository.Add(new Country { ID = "SN", Name = "Senegal", Longitude = -14.452362, Latitude = 14.497401 });
            Repository.Add(new Country { ID = "SO", Name = "Somalia", Longitude = 46.199616, Latitude = 5.152149 });
            Repository.Add(new Country { ID = "SR", Name = "Suriname", Longitude = -56.027783, Latitude = 3.919305 });
            Repository.Add(new Country { ID = "ST", Name = "São Tomé and Príncipe", Longitude = 6.613081, Latitude = 0.18636 });
            Repository.Add(new Country { ID = "SV", Name = "El Salvador", Longitude = -88.89653, Latitude = 13.794185 });
            Repository.Add(new Country { ID = "SY", Name = "Syria", Longitude = 38.996815, Latitude = 34.802075 });
            Repository.Add(new Country { ID = "SZ", Name = "Swaziland", Longitude = 31.465866, Latitude = -26.522503 });
            Repository.Add(new Country { ID = "TC", Name = "Turks and Caicos Islands", Longitude = -71.797928, Latitude = 21.694025 });
            Repository.Add(new Country { ID = "TD", Name = "Chad", Longitude = 18.732207, Latitude = 15.454166 });
            Repository.Add(new Country { ID = "TF", Name = "French Southern Territories", Longitude = 69.348557, Latitude = -49.280366 });
            Repository.Add(new Country { ID = "TG", Name = "Togo", Longitude = 0.824782, Latitude = 8.619543 });
            Repository.Add(new Country { ID = "TH", Name = "Thailand", Longitude = 100.992541, Latitude = 15.870032 });
            Repository.Add(new Country { ID = "TJ", Name = "Tajikistan", Longitude = 71.276093, Latitude = 38.861034 });
            Repository.Add(new Country { ID = "TK", Name = "Tokelau", Longitude = -171.855881, Latitude = -8.967363 });
            Repository.Add(new Country { ID = "TL", Name = "Timor-Leste", Longitude = 125.727539, Latitude = -8.874217 });
            Repository.Add(new Country { ID = "TM", Name = "Turkmenistan", Longitude = 59.556278, Latitude = 38.969719 });
            Repository.Add(new Country { ID = "TN", Name = "Tunisia", Longitude = 9.537499, Latitude = 33.886917 });
            Repository.Add(new Country { ID = "TO", Name = "Tonga", Longitude = -175.198242, Latitude = -21.178986 });
            Repository.Add(new Country { ID = "TR", Name = "Turkey", Longitude = 35.243322, Latitude = 38.963745 });
            Repository.Add(new Country { ID = "TT", Name = "Trinidad and Tobago", Longitude = -61.222503, Latitude = 10.691803 });
            Repository.Add(new Country { ID = "TV", Name = "Tuvalu", Longitude = 177.64933, Latitude = -7.109535 });
            Repository.Add(new Country { ID = "TW", Name = "Taiwan", Longitude = 120.960515, Latitude = 23.69781 });
            Repository.Add(new Country { ID = "TZ", Name = "Tanzania", Longitude = 34.888822, Latitude = -6.369028 });
            Repository.Add(new Country { ID = "UA", Name = "Ukraine", Longitude = 31.16558, Latitude = 48.379433 });
            Repository.Add(new Country { ID = "UG", Name = "Uganda", Longitude = 32.290275, Latitude = 1.373333 });
            Repository.Add(new Country { ID = "UM", Name = "U.S. Minor Outlying Islands", Longitude = 166.6, Latitude = 19.2833 }); /* no location data */
            Repository.Add(new Country { ID = "US", Name = "United States", Longitude = -95.712891, Latitude = 37.09024 });
            Repository.Add(new Country { ID = "UY", Name = "Uruguay", Longitude = -55.765835, Latitude = -32.522779 });
            Repository.Add(new Country { ID = "UZ", Name = "Uzbekistan", Longitude = 64.585262, Latitude = 41.377491 });
            Repository.Add(new Country { ID = "VA", Name = "Vatican City", Longitude = 12.453389, Latitude = 41.902916 });
            Repository.Add(new Country { ID = "VC", Name = "Saint Vincent and the Grenadines", Longitude = -61.287228, Latitude = 12.984305 });
            Repository.Add(new Country { ID = "VE", Name = "Venezuela", Longitude = -66.58973, Latitude = 6.42375 });
            Repository.Add(new Country { ID = "VG", Name = "British Virgin Islands", Longitude = -64.639968, Latitude = 18.420695 });
            Repository.Add(new Country { ID = "VI", Name = "U.S. Virgin Islands", Longitude = -64.896335, Latitude = 18.335765 });
            Repository.Add(new Country { ID = "VN", Name = "Vietnam", Longitude = 108.277199, Latitude = 14.058324 });
            Repository.Add(new Country { ID = "VU", Name = "Vanuatu", Longitude = 166.959158, Latitude = -15.376706 });
            Repository.Add(new Country { ID = "WF", Name = "Wallis and Futuna", Longitude = -177.156097, Latitude = -13.768752 });
            Repository.Add(new Country { ID = "WS", Name = "Samoa", Longitude = -172.104629, Latitude = -13.759029 });
            Repository.Add(new Country { ID = "XK", Name = "Kosovo", Longitude = 20.902977, Latitude = 42.602636 });
            Repository.Add(new Country { ID = "YE", Name = "Yemen", Longitude = 48.516388, Latitude = 15.552727 });
            Repository.Add(new Country { ID = "YT", Name = "Mayotte", Longitude = 45.166244, Latitude = -12.8275 });
            Repository.Add(new Country { ID = "ZA", Name = "South Africa", Longitude = 22.937506, Latitude = -30.559482 });
            Repository.Add(new Country { ID = "ZM", Name = "Zambia", Longitude = 27.849332, Latitude = -13.133897 });
            Repository.Add(new Country { ID = "ZW", Name = "Zimbabwe", Longitude = 29.154857, Latitude = -19.015438 });

        }

        public void CleanUp()
        {
            UnitOfWork.Commit();
            UnitOfWork.Dispose();
        }
    }
}
