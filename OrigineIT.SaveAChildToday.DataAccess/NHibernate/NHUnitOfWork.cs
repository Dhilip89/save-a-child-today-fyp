﻿using NHibernate;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess
{
    public class NHUnitOfWork : IUnitOfWork
    {
        private readonly ISessionFactory SessionFactory;
        private readonly ITransaction Transaction;

        public ISession CurrentSession { get; private set; }

        public NHUnitOfWork(ISessionFactory sessionFactory)
        {
            SessionFactory = sessionFactory;
            CurrentSession = SessionFactory.OpenSession();
            Transaction = CurrentSession.BeginTransaction();
        }

        public IUnitOfWork Commit()
        {
            if (!Transaction.IsActive)
            {
                throw new InvalidOperationException("Transaction is inactive.");
            }

            Transaction.Commit();

            return this;
        }

        public IUnitOfWork Rollback()
        {
            if (Transaction.IsActive)
            {
                Transaction.Rollback();
            }

            return this;
        }

        public void Dispose()
        {
            if (CurrentSession.IsOpen)
            {
                CurrentSession.Close();
            }

            CurrentSession = null;
        }
    }
}
