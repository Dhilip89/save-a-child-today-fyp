﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using OrigineIT.SaveAChildToday.DataAccess.EntityMaps;
using OrigineIT.SaveAChildToday.DataAccess.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;

namespace OrigineIT.SaveAChildToday.DataAccess
{
    public static class NHPersistentStore
    {
        public static ISessionFactory SessionFactory { get; private set; }
        public static FluentConfiguration FluentConfiguration { get; private set; }
        public static IPersistenceConfigurer DatabaseConfiguration { get; private set; }
        
        public static void Configure(IPersistenceConfigurer databaseConfiguration)
        {
            Configure(databaseConfiguration, SchemaConfiguration.NoChange);
        }

        public static void Configure(IPersistenceConfigurer databaseConfiguration, SchemaConfiguration schemaConfiguration)
        {
            DatabaseConfiguration = databaseConfiguration;
            SessionFactory = CreateSessionFactory(schemaConfiguration);
        }

        public static void Close()
        {
            if (!SessionFactory.IsClosed)
            {
                SessionFactory.Close();
            }

            SessionFactory.Dispose();
        }

        public static Boolean IsConfigured
        {
            get
            {
                if ((SessionFactory == null) || (SessionFactory.IsClosed))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static void ExecuteSetup(ISetup setup)
        {
            setup.Initialize();
            setup.Perform();
            setup.CleanUp();
        }

        public static NHUnitOfWork NewUnitOfWork()
        {
            return new NHUnitOfWork(SessionFactory);
        }

        #region Private Methods
        private static ISessionFactory CreateSessionFactory(SchemaConfiguration schemaConfiguration)
        {
            FluentConfiguration = Fluently.Configure();
            FluentConfiguration.Database(DatabaseConfiguration);
            FluentConfiguration.CurrentSessionContext(typeof(NHPersistentStore).GetType().Namespace);
            FluentConfiguration.Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>());

            switch (schemaConfiguration)
            {
                case SchemaConfiguration.Update:
                    FluentConfiguration.ExposeConfiguration(c => new SchemaUpdate(c).Execute(false, true));
                    break;
                case SchemaConfiguration.Rebuild:
                    FluentConfiguration.ExposeConfiguration(CreateSchema);
                    break;
                default:
                    break;
            }

            return FluentConfiguration.BuildSessionFactory();
        }

        private static void CreateSchema(Configuration cfg)
        {
            SchemaExport schema = new SchemaExport(cfg);
            schema.Drop(false, true);
            schema.Create(false, true);
        }
        #endregion
    }
}
