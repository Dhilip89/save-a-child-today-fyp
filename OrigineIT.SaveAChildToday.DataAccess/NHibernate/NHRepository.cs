﻿using NHibernate;
using NHibernate.Linq;
using OrigineIT.SaveAChildToday.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OrigineIT.SaveAChildToday.DataAccess
{
    public class NHRepository<TKey, T> : IPersistentRepository<TKey, T>, IReadOnlyRepository<TKey, T> where T : class, IEntityKey<TKey>
    {
        private readonly ISession Session;

        public NHRepository(NHUnitOfWork unitOfWork)
        {
            Session = unitOfWork.CurrentSession;
        }

        public TKey Add(T entity)
        {
            return (TKey) Session.Save(entity);
        }

        public TKey[] Add(IEnumerable<T> items)
        {
            ISet<TKey> keys = new HashSet<TKey>();

            foreach (T item in items)
            {
                keys.Add((TKey) Session.Save(item));
            }
            return keys.ToArray<TKey>();
        }

        public void AddOrUpdate(T entity)
        {
            Session.SaveOrUpdate(entity);
        }

        public void Update(T entity)
        {
            Session.Update(entity);
        }

        public void Delete(T entity)
        {
            Session.Delete(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                Session.Delete(entity);
            }
        }

        public IQueryable<T> All()
        {
            return Session.Query<T>();
        }

        public IQueryable<T> Page(Int32 page, Int32 limit)
        {
            return All().Skip((page - 1) * limit).Take(limit).AsQueryable<T>();
        }

        public IQueryable<T> PageFilterBy(Expression<Func<T, bool>> expression, int page, int limit)
        {
            return All().Where(expression).Skip((page - 1) * limit).Take(limit).AsQueryable<T>();
        }

        public T FindBy(Expression<Func<T, Boolean>> expression)
        {
            return FilterBy(expression).SingleOrDefault<T>();
        }

        public IQueryable<T> FilterBy(Expression<Func<T, Boolean>> expression)
        {
            return All().Where<T>(expression).AsQueryable<T>();
        }

        public T FindBy(TKey id)
        {
            return Session.Get<T>(id);
        }

    }
}
