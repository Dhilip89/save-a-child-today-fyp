﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrigineIT.SaveAChildToday.DataAccess.Interfaces
{
    public interface IEntityKey<TKey>
    {
        TKey ID { get; }
    }
}
