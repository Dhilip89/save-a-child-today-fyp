﻿using System;

namespace OrigineIT.SaveAChildToday.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUnitOfWork Commit();
        IUnitOfWork Rollback();
    }
}
