﻿namespace OrigineIT.SaveAChildToday.DataAccess.Interfaces
{
    public interface ISetup
    {
        void Initialize();
        void Perform();
        void CleanUp();
    }
}
