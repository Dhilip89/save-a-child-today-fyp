﻿using System;
using System.Collections.Generic;

namespace OrigineIT.SaveAChildToday.DataAccess.Interfaces
{
    public interface IPersistentRepository<TKey, TEntity> where TEntity : class, IEntityKey<TKey>
    {
        TKey Add(TEntity entity);
        TKey[] Add(IEnumerable<TEntity> items);
        void AddOrUpdate(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(IEnumerable<TEntity> entities);
    }
}
