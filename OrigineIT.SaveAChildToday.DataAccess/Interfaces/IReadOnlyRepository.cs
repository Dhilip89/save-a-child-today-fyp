﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace OrigineIT.SaveAChildToday.DataAccess.Interfaces
{
    public interface IReadOnlyRepository<TKey, TEntity> where TEntity : class, IEntityKey<TKey>
    {
        IQueryable<TEntity> All();
        IQueryable<TEntity> Page(Int32 page, Int32 limit);
        IQueryable<TEntity> PageFilterBy(Expression<Func<TEntity, Boolean>> expression, Int32 page, Int32 limit);
        TEntity FindBy(Expression<Func<TEntity, Boolean>> expression);
        IQueryable<TEntity> FilterBy(Expression<Func<TEntity, Boolean>> expression);
        TEntity FindBy(TKey id);
    }
}
