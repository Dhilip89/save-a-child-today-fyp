﻿using FluentNHibernate.Cfg.Db;

namespace OrigineIT.SaveAChildToday.DataAccess.Configurations
{
    public static class DatabaseConfiguration
    {
        public static readonly IPersistenceConfigurer MySqlProduction =
            MySQLConfiguration.Standard.ConnectionString(x => x
                    .Server("192.168.0.200")
                    .Database("origineit_sact")
                    .Username("origineit")
                    .Password("dev#pass123"));

        public static readonly IPersistenceConfigurer MySqlUnitTest =
            MySQLConfiguration.Standard.ConnectionString(x => x
                    .Server("192.168.0.200")
                    .Database("origineit_sact_test")
                    .Username("origineit")
                    .Password("dev#pass123"));

        public static readonly IPersistenceConfigurer AppHarborMySqlFree =
            MySQLConfiguration.Standard.ConnectionString(x => x
                    .Server("50d4ea38-70a4-48be-8dc3-a29700c1b108.mysql.sequelizer.com")
                    .Database("db50d4ea3870a448be8dc3a29700c1b108")
                    .Username("rsdeffkoelwsfpgm")
                    .Password("fVFfZpo4ehow5sRjgiJofD5QV4N6TKqUFP8jsx8zCofe8cMYSRJdbaxJZYs6HriA"));
    }
}
