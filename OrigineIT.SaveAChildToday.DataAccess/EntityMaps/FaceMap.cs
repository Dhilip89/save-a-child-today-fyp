﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class FaceMap : ClassMap<Face>
    {
        public FaceMap()
        {
            Table("faces");

            Id(x => x.ID)
                .Column("face_id")
                .Length(32)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.UuidHex("N");

            Map(x => x.Image)
                .Column("image")
                .Not.Nullable();

            Map(x => x.Width)
                .Column("width")
                .Not.Nullable();

            Map(x => x.Height)
                .Column("height")
                .Not.Nullable();

            Map(x => x.OffsetX)
                .Column("offset_x")
                .Not.Nullable();

            Map(x => x.OffsetY)
                .Column("offset_y")
                .Not.Nullable();

            References(x => x.SourcePhoto)
                .Column("source_photo_id")
                .Not.Nullable();

            Map(x => x.Valid)
                .Column("valid")
                .Not.Nullable();

            Map(x => x.Marked)
                .Column("marked")
                .Not.Nullable();

            Map(x => x.CreationDateTime)
                .Column("creation_date_time")
                .Not.Nullable();
        }
    }
}
