﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class ChildMap : ClassMap<Child>
    {
        public ChildMap()
        {
            Table("children");

            Id(x => x.ID)
                .Column("child_id")
                .Length(32)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.UuidHex("N");

            Map(x => x.Name)
                .Column("name")
                .Length(256)
                .Not.Nullable();

            Map(x => x.Gender)
                .Column("gender")
                .Length(32)
                .Not.Nullable();

            Map(x => x.Birthday)
                .Column("birthday")
                .Not.Nullable();

            Map(x => x.Description)
                .Column("profile_description")
                .Length(8192)
                .Nullable();

            // mod from ref
            References(x => x.ProfilePicture)
                .Column("profile_picture_id")
                //.PropertyRef(r => r.ID)
                .Nullable();
                //.PropertyRef(r => r.ID);

            Map(x => x.Status)
                .Column("status")
                .Length(32)
                .Not.Nullable();

            Map(x => x.MissingDescription)
                .Column("missing_description")
                .Length(8192)
                .Nullable();

            //References(x => x.MissingCountry)
            //    .Column("missing_country_id")
            //    .PropertyRef(r => r.ID)
            //    .Not.Nullable();

            Map(x => x.MissingCountry)
                .Column("missing_country_id")
                .Length(2)
                .Nullable();

            Map(x => x.MissingDateTime)
                .Column("missing_date_time")
                .Not.Nullable();

            Map(x => x.FoundDescription)
                .Column("found_description")
                .Length(8192)
                .Nullable();

            //References(x => x.FoundCountry)
            //    .Column("found_country_id")
            //    .PropertyRef(r => r.ID)
            //    .Not.Nullable();

            Map(x => x.FoundCountry)
                .Column("found_country_id")
                .Length(2)
                .Nullable();

            Map(x => x.FoundDateTime)
                .Column("found_date_time")
                .Not.Nullable();

            Map(x => x.CreationDateTime)
                .Column("creation_date_time")
                .Not.Nullable();

            Map(x => x.LastUpdateDateTime)
                .Column("last_update_date_time")
                .Not.Nullable();

            // mod from References
            References(x => x.Owner)
                //.PropertyRef(r => r.ID)
                .Column("owner_id")
                .Not.Nullable();

            
            HasMany(x => x.Comments)
                .KeyColumn("child_id")
                .KeyNullable();
        }
    }
}
