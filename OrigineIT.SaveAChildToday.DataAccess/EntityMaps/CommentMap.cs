﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class CommentMap : ClassMap<Comment>
    {
        public CommentMap()
        {
            Table("comments");

            Id(x => x.ID)
                .Column("comment_id")
                .Length(32)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.UuidHex("N");

            References(x => x.Owner)
                .Column("owner_id")
                .Not.Nullable()
                .Not.LazyLoad();

            Map(x => x.Message)
                .Column("message")
                .Length(4096)
                .Not.Nullable();

            Map(x => x.CreationDateTime)
                .Column("creation_date_time")
                .Not.Nullable();
        }
    }
}
