﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class PhotoMap : ClassMap<Photo>
    {
        public PhotoMap()
        {
            Table("photos");

            Id(x => x.ID)
                .Column("photo_id")
                .Length(32)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.UuidHex("N");

            Map(x => x.Image)
                .Column("image")
                .Not.Nullable();

            Map(x => x.Width)
                .Column("width")
                .Not.Nullable();

            Map(x => x.Height)
                .Column("height")
                .Not.Nullable();

            References(x => x.Owner)
                .Column("owner_id")
                .Not.Nullable();

            References(x => x.MarkedFace)
                .Column("marked_face_id")
                //.PropertyRef(r => r.ID)
                .Nullable();

            References(x => x.MarkedChild)
                .Column("marked_child_id")
                //.PropertyRef(r => r.ID)
                
                .Nullable()
                .Not.LazyLoad();

            Map(x => x.Caption)
                .Column("caption")
                .Length(256)
                .Nullable();

            Map(x => x.Description)
                .Column("description")
                .Length(4096)
                .Nullable();

            Map(x => x.Longitude)
                .Column("longitude")
                .Precision(8)
                .Nullable();

            Map(x => x.Latitude)
                .Column("latitude")
                .Precision(8)
                .Nullable();

            //References(x => x.Country)
            //    .Column("country_id")
            //    .PropertyRef(r => r.ID)
            //    .Not.Nullable();

            Map(x => x.Country)
                .Column("country_id")
                .Length(2)
                .Nullable();

            Map(x => x.CaptureDateTime)
                .Column("capture_date_time")
                .Not.Nullable();

            Map(x => x.UploadDateTime)
                .Column("upload_date_time")
                .Not.Nullable();

            Map(x => x.LastUpdateDateTime)
                .Column("last_update_date_time")
                .Not.Nullable();

            HasMany(x => x.Comments)
                .KeyColumn("photo_id")
                .KeyNullable();

            HasMany(x => x.Faces)
                .KeyColumn("source_photo_id")
                .Not.KeyNullable()
                .Inverse()
                .Cascade.AllDeleteOrphan();
        }
    }
}
