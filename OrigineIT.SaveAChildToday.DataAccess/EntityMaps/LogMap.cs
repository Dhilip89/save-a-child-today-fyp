﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class LogMap : ClassMap<Log>
    {
        public LogMap()
        {
            Table("logs");

            Id(x => x.ID)
                .Column("log_id")
                .Unique()
                .Not.Nullable()
                .GeneratedBy.Increment();

            Map(x => x.Type)
                .Column("type")
                .Length(64)
                .Not.Nullable();

            Map(x => x.Source)
                .Column("source")
                .Length(256)
                .Not.Nullable();

            Map(x => x.Message)
                .Column("message")
                .Length(4096)
                .Nullable();

            Map(x => x.CreationDateTime)
                .Column("creation_date")
                .Not.Nullable();
        }
    }
}
