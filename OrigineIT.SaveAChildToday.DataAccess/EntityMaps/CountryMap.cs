﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Table("countries");

            Id(x => x.ID)
                .Column("country_id")
                .Length(2)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.Assigned();

            Map(x => x.Name)
                .Column("name")
                .Length(256)
                .Not.Nullable();

            Map(x => x.Longitude)
                .Column("longitude")
                .Precision(8)
                .Not.Nullable();

            Map(x => x.Latitude)
                .Column("latitude")
                .Precision(8)
                .Not.Nullable();
        }
    }
}
