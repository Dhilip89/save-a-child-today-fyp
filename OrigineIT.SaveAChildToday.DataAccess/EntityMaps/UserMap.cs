﻿using FluentNHibernate.Mapping;
using OrigineIT.SaveAChildToday.DataAccess.Entities;

namespace OrigineIT.SaveAChildToday.DataAccess.EntityMaps
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("users");

            Id(x => x.ID)
                .Column("user_id")
                .Length(32)
                .Unique()
                .Not.Nullable()
                .GeneratedBy.UuidHex("N");

            Map(x => x.Name)
                .Column("name")
                .Length(256)
                .Not.Nullable();

            Map(x => x.ProfilePicture)
                .Column("profile_picture")
                .Nullable();

            Map(x => x.ContactNumber)
                .Column("contact_number")
                .Length(64)
                .Nullable();

            Map(x => x.Website)
                .Column("website")
                .Length(256)
                .Nullable();

            
           // References(x => x.Country)
           //     .Column("country_id")
           //     .PropertyRef(r => r.ID)
           //     .Nullable();

            Map(x => x.Country)
               .Column("country_id")
               .Length(2)
               .Nullable();

            Map(x => x.Email)
                .Column("email")
                .Length(128)
                .Unique()
                .Not.Nullable();

            Map(x => x.HashedPassword)
                .Column("hashed_password")
                .Length(4096)
                .Not.Nullable();

            Map(x => x.VerificationToken)
                .Column("verification_token")
                .Length(32)
                .Not.Nullable();

            Map(x => x.Verified)
                .Column("verified")
                .Not.Nullable();

            Map(x => x.CreationDateTime)
                .Column("creation_date_time")
                .Not.Nullable();

            Map(x => x.VerificationDateTime)
                .Column("verification_date_time")
                .Not.Nullable();

            Map(x => x.LastLoginDateTime)
                .Column("last_login_date_time")
                .Not.Nullable();

            Map(x => x.LastUpdateDateTime)
                .Column("last_update_date_time")
                .Not.Nullable();

            Map(x => x.Role)
                .Column("role")
                .Length(64)
                .Not.Nullable();

            HasMany(x => x.Children)
                .KeyColumn("owner_id")
                .Not.KeyNullable()
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
  
            HasMany(x => x.Comments)
                .KeyColumn("owner_id")
                .Not.KeyNullable()
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
                
            HasMany(x => x.Photos)
                .KeyColumn("owner_id")
                .Not.KeyNullable()
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
        }
    }
}
