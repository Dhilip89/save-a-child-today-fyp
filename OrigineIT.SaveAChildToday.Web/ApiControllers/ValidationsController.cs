﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class ValidationsController : ApiController
    {
        [HttpPost]
        [POST("api/validations/email")]
        public Boolean CheckEmailAvailability([FromBody] FormDataCollection data)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                String email = data.GetValues("email").FirstOrDefault();

                if (userRepository.FindBy(user => user.Email.Equals(email)) != null)
                    return false;
                else
                    return true;
            }
        }
    }
}