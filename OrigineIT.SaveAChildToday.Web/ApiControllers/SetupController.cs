﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class SetupController : ApiController
    {
        [HttpGet]
        [GET("api/setup/reset")]
        public String Reset()
        {
            Authentication.Logout();
            Global._ResetDatabase();
            
            return "Done";
        }
    }
}