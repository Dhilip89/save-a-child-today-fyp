﻿using AForge.Imaging.Filters;
using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using OrigineIT.SaveAChildToday.Web.Imaging;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class FacesController : ApiController
    {
        // api/faces
        [HttpGet]
        [GET("api/faces")]
        public dynamic GetFaces(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String sourcePhoto = null,
            [FromUri] Boolean? valid = null,
            [FromUri] Boolean? marked = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "sourcephoto",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                var faces = faceRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    faces = faces.Where(face => face.ID.Equals(id));

                if (!String.IsNullOrWhiteSpace(sourcePhoto))
                    faces = faces.Where(face => face.SourcePhoto.ID.Equals(sourcePhoto));

                if (valid != null)
                    faces = faces.Where(face => face.Valid == valid);

                if (marked != null)
                    faces = faces.Where(face => face.Marked == marked);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            faces = faces.OrderBy(face => face.ID);
                            break;

                        case "width":
                            faces = faces.OrderBy(face => face.Width);
                            break;

                        case "height":
                            faces = faces.OrderBy(face => face.Height);
                            break;

                        case "marked":
                            faces = faces.OrderBy(face => face.Marked);
                            break;

                        case "valid":
                            faces = faces.OrderBy(face => face.Valid);
                            break;

                        case "offsetx":
                            faces = faces.OrderBy(face => face.OffsetX);
                            break;

                        case "offsety":
                            faces = faces.OrderBy(face => face.OffsetY);
                            break;

                        case "sourcephoto":
                        default:
                            faces = faces.OrderBy(face => face.SourcePhoto);
                            break;
                    }
                }

                var result = faces.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }

                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(face => DateTimeUtil.Eval(creationDateTime, face.CreationDateTime));

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return from face in result.ToList()
                       select new
                       {
                           ID = face.ID,
                           Width = face.Width,
                           Height = face.Height,
                           OffsetX = face.OffsetX,
                           OffsetY = face.OffsetY,
                           SourcePhoto = face.SourcePhoto.ID,
                           Valid = face.Valid,
                           Marked = face.Marked,
                           CreationDateTime = face.CreationDateTime
                       };
            }
        }

        [HttpGet]
        [GET("api/faces/info")]
        public dynamic GetFacesInfo(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String sourcePhoto = null,
            [FromUri] Boolean? valid = null,
            [FromUri] Boolean? marked = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "sourcephoto",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                var faces = faceRepository.All();

                //Int64 total = faces.LongCount();

                if (!String.IsNullOrWhiteSpace(id))
                    faces = faces.Where(face => face.ID.Equals(id));

                if (!String.IsNullOrWhiteSpace(sourcePhoto))
                    faces = faces.Where(face => face.SourcePhoto.ID.Equals(sourcePhoto));

                if (valid != null)
                    faces = faces.Where(face => face.Valid == valid);

                if (marked != null)
                    faces = faces.Where(face => face.Marked == marked);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            faces = faces.OrderBy(face => face.ID);
                            break;

                        case "width":
                            faces = faces.OrderBy(face => face.Width);
                            break;

                        case "height":
                            faces = faces.OrderBy(face => face.Height);
                            break;

                        case "marked":
                            faces = faces.OrderBy(face => face.Marked);
                            break;

                        case "valid":
                            faces = faces.OrderBy(face => face.Valid);
                            break;

                        case "offsetx":
                            faces = faces.OrderBy(face => face.OffsetX);
                            break;

                        case "offsety":
                            faces = faces.OrderBy(face => face.OffsetY);
                            break;

                        case "sourcephoto":
                        default:
                            faces = faces.OrderBy(face => face.SourcePhoto);
                            break;
                    }
                }

                var result = faces.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }

                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(face => DateTimeUtil.Eval(creationDateTime, face.CreationDateTime));

                Int64 resultTotal = result.LongCount();

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                var info = new
                {
                    TotalItems = resultTotal,
                    TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                    MaxItemsPerPage = limit,
                    CurrentPage = page,
                    CurrentPageItems = result.ToList().LongCount()
                };

                return info;
            }
        }


        // api/faces/{id} get

        [HttpGet]
        [GET("api/faces/{id}")]
        public dynamic GetFace([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Face face = faceRepository.FindBy(id);

                if (face != null)
                    return new
                    {
                        ID = face.ID,
                        Width = face.Width,
                        Height = face.Height,
                        OffsetX = face.OffsetX,
                        OffsetY = face.OffsetY,
                        SourcePhoto = face.SourcePhoto.ID,
                        Valid = face.Valid,
                        Marked = face.Marked,
                        CreationDateTime = face.CreationDateTime
                    };
                else
                    throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // api/faces/{id} delete
        [HttpDelete]
        [DELETE("api/faces/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void DeleteFace([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Face face = faceRepository.FindBy(id);

                if (Authentication.CurrentRole == UserRole.User)
                {
                    if (!Authentication.CurrentUser.ID.Equals(face.SourcePhoto.Owner.ID))
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }

                if (face != null)
                {
                    faceRepository.Delete(face);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        // api/faces/{id}/image
        [HttpGet]
        [GET("api/faces/{id}/image")]
        public HttpResponseMessage GetImage([FromUri] String id, [FromUri] Int32 width = 0, [FromUri] Int32 height = 0)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Face face = faceRepository.FindBy(id);

                Image image;

                if (face != null)
                {
                    if (face.Image != null)
                    {
                        if (width > 0 && height > 0)
                            image = ImageUtil.GetImage(face.Image, width, height);
                        else
                            image = ImageUtil.GetImage(face.Image);

                        if (Authentication.CurrentRole == UserRole.Anonymous)
                        {
                            ImageUtil.PixellateBlur(image);
                        }
                    }
                    else
                    {
                        image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                    }
                }
                else
                {
                    image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                }

                response.Content = new StreamContent(new MemoryStream(ImageUtil.GetBytes(image, ImageFormat.Png)));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                return response;
            }
        }

    }
}