﻿using AForge.Imaging.Filters;
using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using OrigineIT.SaveAChildToday.Web.Imaging;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class ChildrenController : ApiController
    {
        [HttpGet]
        [GET("api/children")]
        public dynamic GetChildren(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,
            [FromUri] String gender = null,
            [FromUri] String birthday = null,
            [FromUri] String status = null,
            [FromUri] String missingCountry = null,
            [FromUri] String missingDateTime = null,
            [FromUri] String foundCountry = null,
            [FromUri] String foundDateTime = null,
            [FromUri] String creationDateTime = null,
            [FromUri] String lastUpdateDateTime = null,
            [FromUri] String owner = null,
            [FromUri] Boolean? hasPicture = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                var children = childRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    children = children.Where(child => child.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    children = children.Where(child => child.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(gender))
                    children = children.Where(child => child.Gender.ToString().Equals(gender));

                if (!String.IsNullOrWhiteSpace(status))
                    children = children.Where(child => child.Status.ToString().Equals(status));

                if (!String.IsNullOrWhiteSpace(missingCountry))
                    children = children.Where(child => child.MissingCountry.Equals(missingCountry));

                if (!String.IsNullOrWhiteSpace(foundCountry))
                    children = children.Where(child => child.FoundCountry.Equals(foundCountry));

                if (!String.IsNullOrWhiteSpace(owner))
                    children = children.Where(child => child.Owner.ID.Equals(owner));

                if (hasPicture != null)
                    children = children.Where(child => (child.ProfilePicture != null) == hasPicture);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            children = children.OrderBy(child => child.ID);
                            break;

                        case "gender":
                            children = children.OrderBy(child => child.Gender);
                            break;

                        case "owner":
                            children = children.OrderBy(child => child.Owner);
                            break;

                        case "birthday":
                            children = children.OrderBy(child => child.Birthday);
                            break;

                        case "status":
                            children = children.OrderBy(child => child.Status);
                            break;

                        case "comments":
                            children = children.OrderBy(child => child.Comments.Count);
                            break;

                        case "lastupdatedatetime":
                            children = children.OrderBy(child => child.LastUpdateDateTime);
                            break;

                        case "creationdatetime":
                            children = children.OrderBy(child => child.CreationDateTime);
                            break;

                        case "founddatetime":
                            children = children.OrderBy(child => child.FoundDateTime);
                            break;

                        case "foundcountry":
                            children = children.OrderBy(child => child.FoundCountry);
                            break;

                        case "missingdatetime":
                            children = children.OrderBy(child => child.MissingDateTime);
                            break;

                        case "missingcountry":
                            children = children.OrderBy(child => child.MissingCountry);
                            break;

                        case "name":
                        default:
                            children = children.OrderBy(child => child.Name);
                            break;
                    }
                }

                var result = children.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(birthday))
                    result = result.Where(child => DateTimeUtil.Eval(birthday, child.Birthday));

                if (!String.IsNullOrWhiteSpace(missingDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(missingDateTime, child.MissingDateTime));

                if (!String.IsNullOrWhiteSpace(foundDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(foundDateTime, child.FoundDateTime));

                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(creationDateTime, child.CreationDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(lastUpdateDateTime, child.LastUpdateDateTime));


                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return from child in result.ToList()
                       select new
                       {
                           ID = child.ID,
                           Name = child.Name,
                           Gender = child.Gender.ToString(),
                           Birthday = child.Birthday,
                           Age = child.GetAgeByDate(DateTime.UtcNow),
                           Description = child.Description,
                           ProfilePicture = (String)((child.ProfilePicture != null) ? child.ProfilePicture.ID : null),
                           Status = child.Status.ToString(),
                           MissingDescription = child.MissingDescription,
                           MissingCountry = child.MissingCountry,
                           MissingDateTime = child.MissingDateTime,
                           MissingAge = child.GetAgeByDate(child.MissingDateTime),
                           FoundDescription = child.FoundDescription,
                           FoundCountry = child.FoundCountry,
                           FoundDateTime = child.FoundDateTime,
                           FoundAge = child.GetAgeByDate(child.FoundDateTime),
                           CreationDateTime = child.CreationDateTime,
                           LastUpdateDateTime = child.LastUpdateDateTime,
                           Owner = (String)((child.Owner != null) ? child.Owner.ID : null)
                       };
            }
        }


        [HttpGet]
        [GET("api/children/info")]
        public dynamic GetChildrenInfo(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,
            [FromUri] String gender = null,
            [FromUri] String birthday = null,
            [FromUri] String status = null,
            [FromUri] String missingCountry = null,
            [FromUri] String missingDateTime = null,
            [FromUri] String foundCountry = null,
            [FromUri] String foundDateTime = null,
            [FromUri] String creationDateTime = null,
            [FromUri] String lastUpdateDateTime = null,
            [FromUri] String owner = null,
            [FromUri] Boolean? hasPicture = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                var children = childRepository.All();

                // Int64 total = children.LongCount();

                if (!String.IsNullOrWhiteSpace(id))
                    children = children.Where(child => child.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    children = children.Where(child => child.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(gender))
                    children = children.Where(child => child.Gender.ToString().Equals(gender));

                if (!String.IsNullOrWhiteSpace(status))
                    children = children.Where(child => child.Status.ToString().Equals(status));

                if (!String.IsNullOrWhiteSpace(missingCountry))
                    children = children.Where(child => child.MissingCountry.Equals(missingCountry));

                if (!String.IsNullOrWhiteSpace(foundCountry))
                    children = children.Where(child => child.FoundCountry.Equals(foundCountry));

                if (!String.IsNullOrWhiteSpace(owner))
                    children = children.Where(child => child.Owner.ID.Equals(owner));

                if (hasPicture != null)
                    children = children.Where(child => (child.ProfilePicture != null) == hasPicture);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            children = children.OrderBy(child => child.ID);
                            break;

                        case "gender":
                            children = children.OrderBy(child => child.Gender);
                            break;

                        case "owner":
                            children = children.OrderBy(child => child.Owner);
                            break;

                        case "birthday":
                            children = children.OrderBy(child => child.Birthday);
                            break;

                        case "status":
                            children = children.OrderBy(child => child.Status);
                            break;

                        case "comments":
                            children = children.OrderBy(child => child.Comments.Count);
                            break;

                        case "lastupdatedatetime":
                            children = children.OrderBy(child => child.LastUpdateDateTime);
                            break;

                        case "creationdatetime":
                            children = children.OrderBy(child => child.CreationDateTime);
                            break;

                        case "founddatetime":
                            children = children.OrderBy(child => child.FoundDateTime);
                            break;

                        case "foundcountry":
                            children = children.OrderBy(child => child.FoundCountry);
                            break;

                        case "missingdatetime":
                            children = children.OrderBy(child => child.MissingDateTime);
                            break;

                        case "missingcountry":
                            children = children.OrderBy(child => child.MissingCountry);
                            break;

                        case "name":
                        default:
                            children = children.OrderBy(child => child.Name);
                            break;
                    }
                }

                var result = children.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(birthday))
                    result = result.Where(child => DateTimeUtil.Eval(birthday, child.Birthday));

                if (!String.IsNullOrWhiteSpace(missingDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(missingDateTime, child.MissingDateTime));

                if (!String.IsNullOrWhiteSpace(foundDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(foundDateTime, child.FoundDateTime));

                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(creationDateTime, child.CreationDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(child => DateTimeUtil.Eval(lastUpdateDateTime, child.LastUpdateDateTime));

                Int64 resultTotal = result.LongCount();

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                

                var info = new
                {
                    TotalItems = resultTotal,
                    TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                    MaxItemsPerPage = limit,
                    CurrentPage = page,
                    CurrentPageItems = result.ToList().LongCount()
                };

                return info;
            }
        }


        [HttpGet]
        [GET("api/children/{id}")]
        public dynamic GetChild([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                Child child = childRepository.FindBy(id);

                if (child != null)
                {
                    Country missingCountry = null;
                    Country foundCountry = null;

                    if (!String.IsNullOrWhiteSpace(child.MissingCountry))
                        missingCountry = countryRepository.FindBy(child.MissingCountry);

                    if (!String.IsNullOrWhiteSpace(child.FoundCountry))
                        foundCountry = countryRepository.FindBy(child.FoundCountry);

                    return new
                    {
                        ID = child.ID,
                        Name = child.Name,
                        Gender = child.Gender.ToString(),
                        Birthday = child.Birthday,
                        Age = child.GetAgeByDate(DateTime.UtcNow),
                        Description = child.Description,
                        ProfilePicture = /*(String)*/((child.ProfilePicture != null) ?
                            //child.ProfilePicture.ID 
                            new
                            {
                                ID = child.ProfilePicture.ID,
                                Width = child.ProfilePicture.Width,
                                Height = child.ProfilePicture.Height,
                                Owner = (String)((child.ProfilePicture.Owner != null) ? child.ProfilePicture.Owner.ID : null),
                                MarkedFace = (String)((child.ProfilePicture.MarkedFace != null) ? child.ProfilePicture.MarkedFace.ID : null),
                                MarkedChild = (String)((child.ProfilePicture.MarkedChild != null) ? child.ProfilePicture.MarkedChild.ID : null),
                                Caption = child.ProfilePicture.Caption,
                                Description = child.ProfilePicture.Description,
                                Longitude = child.ProfilePicture.Longitude,
                                Latitude = child.ProfilePicture.Latitude,
                                Country = child.ProfilePicture.Country,
                                CaptureDateTime = child.ProfilePicture.CaptureDateTime,
                                UploadDateTime = child.ProfilePicture.UploadDateTime,
                                LastUpdateTime = child.ProfilePicture.LastUpdateDateTime
                            }
                            : null),
                        Status = child.Status.ToString(),
                        MissingDescription = child.MissingDescription,
                        MissingCountry = //child.MissingCountry,
                            missingCountry,
                        MissingDateTime = child.MissingDateTime,
                        MissingAge = child.GetAgeByDate(child.MissingDateTime),
                        FoundDescription = child.FoundDescription,
                        FoundCountry = //child.FoundCountry,
                            foundCountry,
                        FoundDateTime = child.FoundDateTime,
                        FoundAge = child.GetAgeByDate(child.FoundDateTime),
                        CreationDateTime = child.CreationDateTime,
                        LastUpdateDateTime = child.LastUpdateDateTime,
                        Owner = /*(String)*/ ((child.Owner != null) ?
                            //child.Owner.ID  
                            new
                            {
                                ID = child.Owner.ID,
                                Name = child.Owner.Name,
                                ContactNumber = child.Owner.ContactNumber,
                                Website = (String)child.Owner.Website,
                                Country = child.Owner.Country,
                                Email = child.Owner.Email,
                                Verified = child.Owner.Verified,
                                CreationDateTime = child.Owner.CreationDateTime,
                                VerificationDateTime = child.Owner.VerificationDateTime,
                                LastLoginDateTime = child.Owner.LastLoginDateTime,
                                LastUpdateDateTime = child.Owner.LastUpdateDateTime,
                                Role = child.Owner.Role.ToString()
                            }
                            : null)
                    };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpPost]
        [POST("api/children")]
        [Authorize(Roles = "User, Administrator, System")]
        public dynamic AddChild([FromBody] dynamic childInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, DataAccess.Entities.User>(unitOfWork);
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                try
                {
                    User user = userRepository.FindBy(Authentication.CurrentUser.ID);

                    Child child = new Child()
                    {
                        Name = childInfo.Name,
                        Gender = EnumUtil.ParseEnum<Gender>((String)childInfo.Gender),
                        Birthday = childInfo.Birthday,
                        Description = childInfo.Description,
                        MissingDateTime = childInfo.MissingDateTime,
                        MissingDescription = childInfo.MissingDescription,
                        MissingCountry = childInfo.MissingCountry,
                        Owner = user
                    };

                    user.Children.Add(child);

                    var id = childRepository.Add(child);

                    unitOfWork.Commit();

                    return id;
                }
                catch (Exception)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }
  


        [HttpPost]
        [POST("api/children/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void UpdateChild([FromUri] String id, [FromBody] dynamic childInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Child child = childRepository.FindBy(id);

                if (child != null)
                {
                    if (Authentication.CurrentRole == UserRole.User)
                    {
                        if (!Authentication.CurrentUser.ID.Equals(child.Owner.ID))
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    var name = (String)childInfo.Name;
                    var gender = (String)childInfo.Gender;
                    var birthday = (DateTime?)childInfo.Birthday;
                    var description = (String)childInfo.Description;
                    var profilePicture = (String)childInfo.ProfilePicture;
                    var status = (String)childInfo.Status;
                    var missingDescription = (String)childInfo.MissingDescription;
                    var missingCountry = (String)childInfo.MissingCountry;
                    var missingDateTime = (DateTime?)childInfo.MissingDateTime;
                    var foundDescription = (String)childInfo.FoundDescription;
                    var foundCountry = (String)childInfo.FoundCountry;
                    var foundDateTime = (DateTime?)childInfo.FoundDateTime;

                    if (!String.IsNullOrWhiteSpace(name))
                        child.Name = name;

                    if (!String.IsNullOrWhiteSpace(gender))
                        child.Gender = EnumUtil.ParseEnum<Gender>(gender);

                    if (birthday != null)
                        child.Birthday = (DateTime)birthday;

                    if (!String.IsNullOrWhiteSpace(description))
                        child.Description = description;

                    if (!String.IsNullOrWhiteSpace(profilePicture))
                    {
                        Photo photo = photoRepository.FindBy(profilePicture);

                        if (photo != null)
                        {
                            if (photo.MarkedChild.ID.Equals(child.ID) && photo.MarkedFace != null)
                                child.ProfilePicture = photo;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(status))
                        child.Status = EnumUtil.ParseEnum<ChildStatus>(status);

                    if (!String.IsNullOrWhiteSpace(missingDescription))
                        child.MissingDescription = missingDescription;

                    if (!String.IsNullOrWhiteSpace(missingCountry))
                        child.MissingCountry = missingCountry;

                    if (missingDateTime != null)
                        child.MissingDateTime = (DateTime)missingDateTime;

                    if (!String.IsNullOrWhiteSpace(foundDescription))
                        child.FoundDescription = foundDescription;

                    if (!String.IsNullOrWhiteSpace(foundCountry))
                        child.FoundCountry = foundCountry;

                    if (foundDateTime != null)
                        child.FoundDateTime = (DateTime)foundDateTime;

                    try
                    {
                        child.LastUpdateDateTime = DateTime.UtcNow;
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpDelete]
        [DELETE("api/children/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void DeleteChild([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                Child child = childRepository.FindBy(id);

                if (Authentication.CurrentRole == UserRole.User)
                {
                    if (!Authentication.CurrentUser.ID.Equals(child.Owner.ID))
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }

                if (child != null)
                {
                    childRepository.Delete(child);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpGet]
        [GET("api/children/{id}/photos")]
        public dynamic GetLinkedPhotos([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                
                Child child = childRepository.FindBy(id);

                if (child != null)
                {
                    var photos = from photo in photoRepository.All()
                                 where photo.MarkedChild.ID == child.ID
                                 select new
                                 {
                                     ID = photo.ID,
                                     Width = photo.Width,
                                     Height = photo.Height,
                                     Owner = (String)((photo.Owner != null) ? photo.Owner.ID : null),
                                     MarkedFace = (String)((photo.MarkedFace != null) ? photo.MarkedFace.ID : null),
                                     MarkedChild = (String)((photo.MarkedChild != null) ? photo.MarkedChild.ID : null),
                                     Caption = photo.Caption,
                                     Description = photo.Description,
                                     Longitude = photo.Longitude,
                                     Latitude = photo.Latitude,
                                     Country = photo.Country,
                                     CaptureDateTime = photo.CaptureDateTime,
                                     UploadDateTime = photo.UploadDateTime,
                                     LastUpdateTime = photo.LastUpdateDateTime
                                 };

                    return photos.ToList();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        [HttpGet]
        [GET("api/children/{id}/picture")]
        public HttpResponseMessage GetProfilePicture([FromUri] String id, [FromUri] Int32 width = 0, [FromUri] Int32 height = 0, [FromUri] Boolean face = true)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                Child child = childRepository.FindBy(id);

                Image image;

                if (child != null)
                {
                    if (child.ProfilePicture != null)
                    {
                        if (width > 0 && height > 0)
                            image = ImageUtil.GetImage(face ? child.ProfilePicture.MarkedFace.Image : child.ProfilePicture.Image, width, height);
                        else
                            image = ImageUtil.GetImage(face ? child.ProfilePicture.MarkedFace.Image : child.ProfilePicture.Image);

                        if (Authentication.CurrentRole == UserRole.Anonymous)
                        {
                            ImageUtil.PixellateBlur(image);
                        }
                    }
                    else
                    {
                        image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                    }
                }
                else
                {
                    image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                }

                response.Content = new StreamContent(new MemoryStream(ImageUtil.GetBytes(image, ImageFormat.Png)));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                return response;
            }
        }


        [HttpPost]
        [POST("api/children/{id}/comments")]
        [Authorize(Roles = "User, Administrator, System")]
        public dynamic AddComment([FromUri] String id, [FromBody] dynamic commentInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Comment> commentRepository = new NHRepository<String, Comment>(unitOfWork);

                Child child = childRepository.FindBy(id);

                if (child != null)
                {
                    try
                    {
                        User owner = userRepository.FindBy(Authentication.CurrentUser.ID);

                        Comment comment = new Comment()
                        {
                            Message = commentInfo.Message,
                            CreationDateTime = DateTime.UtcNow,
                            Owner = owner
                        };

                        child.Comments.Add(comment);
                        owner.Comments.Add(comment);

                        var commentID = commentRepository.Add(comment);

                        unitOfWork.Commit();

                        return commentID;
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpGet]
        [GET("api/children/{childID}/comments")]
        public dynamic GetComments(
            // Resource
            [FromUri] String childID,

            // Filter criterias
            [FromUri] String owner = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "creationdatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                Child child = childRepository.FindBy(childID);

                if (child != null)
                {
                    var comments = child.Comments.AsQueryable();

                    if (!String.IsNullOrWhiteSpace(owner))
                        comments = comments.Where(comment => comment.Owner.ID.Equals(owner));


                    if (!String.IsNullOrEmpty(orderBy))
                    {
                        switch (orderBy.Trim().ToLower())
                        {
                            case "owner":
                                comments = comments.OrderBy(comment => comment.Owner);
                                break;

                            case "creationdatetime":
                            default:
                                comments = comments.OrderBy(comment => comment.CreationDateTime);
                                break;
                        }
                    }

                    var result = comments.AsEnumerable();

                    switch (order.Trim().ToLower())
                    {
                        case "desc":
                            result = result.Reverse();
                            break;
                    }

                    if (!String.IsNullOrWhiteSpace(creationDateTime))
                        result = result.Where(comment => DateTimeUtil.Eval(creationDateTime, comment.CreationDateTime));

                    if (page > 0 && limit > 0)
                        result = result.Skip((page - 1) * limit).Take(limit);

                    return from comment in result.ToList()
                           select new
                           {
                               ID = comment.ID,
                               Message = comment.Message,
                               CreationDateTime = comment.CreationDateTime,
                               //Owner = comment.Owner.ID,
                               Owner = ((comment.Owner != null) ?
                            new
                            {
                                ID = comment.Owner.ID,
                                Name = comment.Owner.Name,
                                ContactNumber = comment.Owner.ContactNumber,
                                Website = (String)comment.Owner.Website,
                                Country = comment.Owner.Country,
                                Email = comment.Owner.Email,
                                Verified = comment.Owner.Verified,
                                CreationDateTime = comment.Owner.CreationDateTime,
                                VerificationDateTime = comment.Owner.VerificationDateTime,
                                LastLoginDateTime = comment.Owner.LastLoginDateTime,
                                LastUpdateDateTime = comment.Owner.LastUpdateDateTime,
                                Role = comment.Owner.Role.ToString()
                            }
                            : null)
                           };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpGet]
        [GET("api/children/{childID}/comments/info")]
        public dynamic GetCommentsInfo(
            // Resource
            [FromUri] String childID,

            // Filter criterias
            [FromUri] String owner = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "creationdatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                Child child = childRepository.FindBy(childID);

                if (child != null)
                {
                    var comments = child.Comments.AsQueryable();

                    // Int64 total = comments.LongCount();

                    if (!String.IsNullOrWhiteSpace(owner))
                        comments = comments.Where(comment => comment.Owner.ID.Equals(owner));


                    if (!String.IsNullOrEmpty(orderBy))
                    {
                        switch (orderBy.Trim().ToLower())
                        {
                            case "owner":
                                comments = comments.OrderBy(comment => comment.Owner);
                                break;

                            case "creationdatetime":
                            default:
                                comments = comments.OrderBy(comment => comment.CreationDateTime);
                                break;
                        }
                    }

                    var result = comments.AsEnumerable();

                    switch (order.Trim().ToLower())
                    {
                        case "desc":
                            result = result.Reverse();
                            break;
                    }

                    if (!String.IsNullOrWhiteSpace(creationDateTime))
                        result = result.Where(comment => DateTimeUtil.Eval(creationDateTime, comment.CreationDateTime));

                    Int64 resultTotal = result.LongCount();

                    if (page > 0 && limit > 0)
                        result = result.Skip((page - 1) * limit).Take(limit);

                    var info = new
                    {
                        TotalItems = resultTotal,
                        TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                        MaxItemsPerPage = limit,
                        CurrentPage = page,
                        CurrentPageItems = result.ToList().LongCount()
                    };

                    return info;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

    }
}