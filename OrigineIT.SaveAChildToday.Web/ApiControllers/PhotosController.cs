﻿using AForge.Imaging.Filters;
using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using OrigineIT.SaveAChildToday.Web.Imaging;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Windows.Media.Imaging;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class PhotosController : ApiController
    {

        // api/photos
        [HttpGet]
        [GET("api/photos")]
        public dynamic GetPhotos(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String owner = null,
            [FromUri] String markedChild = null,
            [FromUri] String country = null,
            [FromUri] String captureDateTime = null,
            [FromUri] String uploadDateTime = null,
            [FromUri] String lastUpdateDateTime = null,
            [FromUri] Boolean? linked = null,
            [FromUri] Boolean? hasFaces = null,
            [FromUri] Boolean? faceMarked = null,
            [FromUri] Boolean? profiled = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "uploaddatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                var photos = photoRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    photos = photos.Where(photo => photo.ID.Equals(id));

                if (!String.IsNullOrWhiteSpace(owner))
                    photos = photos.Where(photo => photo.Owner.ID.Equals(owner));

                if (!String.IsNullOrWhiteSpace(markedChild))
                    photos = photos.Where(photo => photo.MarkedChild.ID.Equals(markedChild));

                if (!String.IsNullOrWhiteSpace(country))
                    photos = photos.Where(photo => photo.Country.Equals(country));

                if (linked != null)
                    photos = photos.Where(photo => (photo.MarkedChild != null) == linked);

                if (hasFaces != null)
                    photos = photos.Where(photo => (photo.Faces.Count > 0) == hasFaces);

                if (faceMarked != null)
                    photos = photos.Where(photo => (photo.MarkedFace != null) == faceMarked);

                if (profiled != null)
                    photos = photos.Where(photo => ((photo.MarkedChild != null) && (photo == photo.MarkedChild.ProfilePicture)) == profiled);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            photos = photos.OrderBy(photo => photo.ID);
                            break;

                        case "owner":
                            photos = photos.OrderBy(photo => photo.Owner);
                            break;

                        case "markedchild":
                            photos = photos.OrderBy(photo => photo.MarkedChild);
                            break;

                        case "country":
                            photos = photos.OrderBy(photo => photo.Country);
                            break;

                        case "capturedatetime":
                            photos = photos.OrderBy(photo => photo.CaptureDateTime);
                            break;

                        case "lastupdatedatetime":
                            photos = photos.OrderBy(photo => photo.LastUpdateDateTime);
                            break;

                        case "comments":
                            photos = photos.OrderBy(photo => photo.Comments.Count);
                            break;

                        case "faces":
                            photos = photos.OrderBy(photo => photo.Faces.Count);
                            break;

                        case "uploaddatetime":
                        default:
                            photos = photos.OrderBy(photo => photo.UploadDateTime);
                            break;
                    }
                }

                var result = photos.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(captureDateTime))
                    result = result.Where(photo => DateTimeUtil.Eval(captureDateTime, photo.CaptureDateTime));

                if (!String.IsNullOrWhiteSpace(uploadDateTime))
                    result = result.Where(photo => DateTimeUtil.Eval(uploadDateTime, photo.UploadDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastUpdateDateTime, user.LastUpdateDateTime));


                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return from photo in result.ToList()
                       select new
                       {
                           ID = photo.ID,
                           Width = photo.Width,
                           Height = photo.Height,
                           Owner = (String)((photo.Owner != null) ? photo.Owner.ID : null),
                           MarkedFace = (String)((photo.MarkedFace != null) ? photo.MarkedFace.ID : null),
                           MarkedChild = (String)((photo.MarkedChild != null) ? photo.MarkedChild.ID : null),
                           Caption = photo.Caption,
                           Description = photo.Description,
                           Longitude = photo.Longitude,
                           Latitude = photo.Latitude,
                           Country = photo.Country,
                           CaptureDateTime = photo.CaptureDateTime,
                           UploadDateTime = photo.UploadDateTime,
                           LastUpdateTime = photo.LastUpdateDateTime,
                           PicturedChild = (photo.MarkedChild != null ? (photo.Equals(photo.MarkedChild.ProfilePicture) ? photo.MarkedChild.ID : null) : null)
                       };
            }
        }


        [HttpGet]
        [GET("api/photos/info")]
        public dynamic GetPhotosInfo(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String owner = null,
            [FromUri] String markedChild = null,
            [FromUri] String country = null,
            [FromUri] String captureDateTime = null,
            [FromUri] String uploadDateTime = null,
            [FromUri] String lastUpdateDateTime = null,
            [FromUri] Boolean? linked = null,
            [FromUri] Boolean? hasFaces = null,
            [FromUri] Boolean? faceMarked = null,
            [FromUri] Boolean? profiled = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "uploaddatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                var photos = photoRepository.All();

                // Int64 total = photos.LongCount();

                if (!String.IsNullOrWhiteSpace(id))
                    photos = photos.Where(photo => photo.ID.Equals(id));

                if (!String.IsNullOrWhiteSpace(owner))
                    photos = photos.Where(photo => photo.Owner.ID.Equals(owner));

                if (!String.IsNullOrWhiteSpace(markedChild))
                    photos = photos.Where(photo => photo.MarkedChild.ID.Equals(markedChild));

                if (!String.IsNullOrWhiteSpace(country))
                    photos = photos.Where(photo => photo.Country.Equals(country));

                if (linked != null)
                    photos = photos.Where(photo => (photo.MarkedChild != null) == linked);

                if (hasFaces != null)
                    photos = photos.Where(photo => (photo.Faces.Count > 0) == hasFaces);

                if (faceMarked != null)
                    photos = photos.Where(photo => (photo.MarkedFace != null) == faceMarked);

                if (profiled != null)
                    photos = photos.Where(photo => ((photo.MarkedChild != null) && (photo == photo.MarkedChild.ProfilePicture)) == profiled);

                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            photos = photos.OrderBy(photo => photo.ID);
                            break;

                        case "owner":
                            photos = photos.OrderBy(photo => photo.Owner);
                            break;

                        case "markedchild":
                            photos = photos.OrderBy(photo => photo.MarkedChild);
                            break;

                        case "country":
                            photos = photos.OrderBy(photo => photo.Country);
                            break;

                        case "capturedatetime":
                            photos = photos.OrderBy(photo => photo.CaptureDateTime);
                            break;

                        case "lastupdatedatetime":
                            photos = photos.OrderBy(photo => photo.LastUpdateDateTime);
                            break;

                        case "comments":
                            photos = photos.OrderBy(photo => photo.Comments.Count);
                            break;

                        case "faces":
                            photos = photos.OrderBy(photo => photo.Faces.Count);
                            break;

                        case "uploaddatetime":
                        default:
                            photos = photos.OrderBy(photo => photo.UploadDateTime);
                            break;
                    }
                }

                var result = photos.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(captureDateTime))
                    result = result.Where(photo => DateTimeUtil.Eval(captureDateTime, photo.CaptureDateTime));

                if (!String.IsNullOrWhiteSpace(uploadDateTime))
                    result = result.Where(photo => DateTimeUtil.Eval(uploadDateTime, photo.UploadDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastUpdateDateTime, user.LastUpdateDateTime));

                Int64 resultTotal = result.LongCount();

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                var info = new
                {
                    TotalItems = resultTotal,
                    TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                    MaxItemsPerPage = limit,
                    CurrentPage = page,
                    CurrentPageItems = result.ToList().LongCount()
                };

                return info;
            }
        }

        //get
        [HttpGet]
        [GET("api/photos/{id}")]
        public dynamic GetPhoto([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {

                    Country country = null;

                    if (!String.IsNullOrWhiteSpace(photo.Country))
                        country = countryRepository.FindBy(photo.Country);

                    return new
                    {
                        ID = photo.ID,
                        Width = photo.Width,
                        Height = photo.Height,
                        //Owner = (String)((photo.Owner != null) ? photo.Owner.ID : null),
                        Owner = ((photo.Owner != null) ?
                            new
                            {
                                ID = photo.Owner.ID,
                                Name = photo.Owner.Name,
                                ContactNumber = photo.Owner.ContactNumber,
                                Website = (String)photo.Owner.Website,
                                Country = photo.Owner.Country,
                                Email = photo.Owner.Email,
                                Verified = photo.Owner.Verified,
                                CreationDateTime = photo.Owner.CreationDateTime,
                                VerificationDateTime = photo.Owner.VerificationDateTime,
                                LastLoginDateTime = photo.Owner.LastLoginDateTime,
                                LastUpdateDateTime = photo.Owner.LastUpdateDateTime,
                                Role = photo.Owner.Role.ToString()
                            }
                            : null),
                        //MarkedFace = (String)((photo.MarkedFace != null) ? photo.MarkedFace.ID : null),
                        MarkedFace = ((photo.MarkedFace != null) ?
                            new
                            {
                                ID = photo.MarkedFace.ID,
                                Width = photo.MarkedFace.Width,
                                Height = photo.MarkedFace.Height,
                                OffsetX = photo.MarkedFace.OffsetX,
                                OffsetY = photo.MarkedFace.OffsetY,
                                SourcePhoto = photo.MarkedFace.SourcePhoto.ID,
                                Valid = photo.MarkedFace.Valid,
                                Marked = photo.MarkedFace.Marked,
                                CreationDateTime = photo.MarkedFace.CreationDateTime
                            }
                            : null),
                        //MarkedChild = (String)((photo.MarkedChild != null) ? photo.MarkedChild.ID : null),
                        MarkedChild = ((photo.MarkedChild != null) ?
                            new {
                                ID = photo.MarkedChild.ID,
                                Name = photo.MarkedChild.Name,
                                Gender = photo.MarkedChild.Gender.ToString(),
                                Birthday = photo.MarkedChild.Birthday,
                                Age = photo.MarkedChild.GetAgeByDate(DateTime.UtcNow),
                                Description = photo.MarkedChild.Description,
                                ProfilePicture = (String)((photo.MarkedChild.ProfilePicture != null) ? photo.MarkedChild.ProfilePicture.ID : null),
                                Status = photo.MarkedChild.Status.ToString(),
                                MissingDescription = photo.MarkedChild.MissingDescription,
                                MissingCountry = photo.MarkedChild.MissingCountry,
                                MissingDateTime = photo.MarkedChild.MissingDateTime,
                                MissingAge = photo.MarkedChild.GetAgeByDate(photo.MarkedChild.MissingDateTime),
                                FoundDescription = photo.MarkedChild.FoundDescription,
                                FoundCountry = photo.MarkedChild.FoundCountry,
                                FoundDateTime = photo.MarkedChild.FoundDateTime,
                                FoundAge = photo.MarkedChild.GetAgeByDate(photo.MarkedChild.FoundDateTime),
                                CreationDateTime = photo.MarkedChild.CreationDateTime,
                                LastUpdateDateTime = photo.MarkedChild.LastUpdateDateTime,
                                Owner = (String)((photo.MarkedChild.Owner != null) ? photo.MarkedChild.Owner.ID : null)
                            }
                            : null),
                        Caption = photo.Caption,
                        Description = photo.Description,
                        Longitude = photo.Longitude,
                        Latitude = photo.Latitude,
                        //Country = photo.Country,
                        Country = country,
                        CaptureDateTime = photo.CaptureDateTime,
                        UploadDateTime = photo.UploadDateTime,
                        LastUpdateTime = photo.LastUpdateDateTime,
                        PicturedChild = (photo.MarkedChild != null ? (photo.Equals(photo.MarkedChild.ProfilePicture) ? photo.MarkedChild.ID : null) : null)
                    };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        // api/photos/{id} = c r u d

        [HttpPost]
        [POST("api/photos")]
        [Authorize(Roles = "User, Administrator, System")]
        public async Task<HttpResponseMessage> AddPhoto()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var dataStreamProvider = new MultipartFormDataStreamProvider(Global.TempFolder);

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                try
                {
                    await Request.Content.ReadAsMultipartAsync(dataStreamProvider);

                    User user = userRepository.FindBy(Authentication.CurrentUser.ID);

                    Photo photo = new Photo() { Owner = user, UploadDateTime = DateTime.UtcNow, CaptureDateTime = DateTime.UtcNow };

                    foreach (var key in dataStreamProvider.FormData.AllKeys)
                    {
                        foreach (var val in dataStreamProvider.FormData.GetValues(key))
                        {

                            switch (key.ToLower())
                            {
                                case "caption":
                                    photo.Caption = val;
                                    break;

                                case "description":
                                    photo.Description = val;
                                    break;

                                case "capturedatetime":
                                    photo.CaptureDateTime = DateTime.Parse(val);
                                    break;

                                case "longitude":
                                    photo.Longitude = Double.Parse(val);
                                    break;

                                case "latitude":
                                    photo.Latitude = Double.Parse(val);
                                    break;

                                case "country":
                                    photo.Country = val;
                                    break;
                            }

                        }
                    }

                    foreach (var file in dataStreamProvider.FileData)
                    {
                        FileInfo fileInfo = new FileInfo(file.LocalFileName);
                        String tempFile = fileInfo.Directory + "\\" + fileInfo.Name;

                        Byte[] imageData = File.ReadAllBytes(tempFile);

                        Boolean validImage = ImageUtil.ValidateBytes(imageData);

                        if (!validImage)
                        {
                            File.Delete(tempFile);
                            return new HttpResponseMessage()
                            {
                                StatusCode = HttpStatusCode.BadRequest
                            };
                        }

                        Image image = ImageUtil.GetImage(imageData);

                        photo.Height = image.Height;
                        photo.Width = image.Width;
                        photo.Image = imageData;

                        photo.Faces.AddAll(FaceRecognizer.DetectFaces(photo));

                        File.Delete(tempFile);

                        break;
                    }

                    if (photo.Faces.Count > 0)
                    {
                        String id = photoRepository.Add(photo);

                        unitOfWork.Commit();

                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.Created,
                            Content = new StringContent(id)
                        };
                    }
                    else
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.OK,
                            Content = new StringContent("null")
                        };
                    }
                }
                catch (Exception e)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
            }
        }


        // api/photos/{id}  + update (*)
        [HttpPost]
        [POST("api/photos/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void UpdatePhoto([FromUri] String id, [FromBody] dynamic photoInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {
                    if (Authentication.CurrentRole == UserRole.User)
                    {
                        if (!Authentication.CurrentUser.ID.Equals(photo.Owner.ID))
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    var markedFace = (String)photoInfo.MarkedFace;
                    var markedChild = (String)photoInfo.MarkedChild;
                    var caption = (String)photoInfo.Caption;
                    var description = (String)photoInfo.Description;
                    var longitude = (Double)photoInfo.Longitude;
                    var latitude = (Double)photoInfo.Latitude;
                    var country = (String)photoInfo.Country;
                    var captureDatetime = (DateTime)photoInfo.CaptureDateTime;

                    if (!String.IsNullOrWhiteSpace(caption))
                        photo.Caption = caption;

                    if (!String.IsNullOrWhiteSpace(description))
                        photo.Description = description;

                    if (longitude != 0)
                        photo.Longitude = longitude;

                    if (latitude != 0)
                        photo.Latitude = latitude;

                    if (!String.IsNullOrWhiteSpace(description))
                        photo.Description = description;

                    if (!String.IsNullOrWhiteSpace(markedFace))
                    {
                        Face face = faceRepository.FindBy(markedFace);

                        if (face != null)
                        {
                            if (face.SourcePhoto.ID.Equals(photo.ID))
                                photo.MarkedFace = face;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(markedChild))
                    {
                        Child child = childRepository.FindBy(markedChild);

                        if (child != null)
                        {
                            photo.MarkedChild = child;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(country))
                        photo.Country = photo.Country;

                    if (captureDatetime != null)
                        photo.CaptureDateTime = captureDatetime;

                    try
                    {
                        photo.LastUpdateDateTime = DateTime.UtcNow;
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        // api/photos/{id}  + delete (progress)
        [HttpDelete]
        [DELETE("api/photos/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void DeletePhoto([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (Authentication.CurrentRole == UserRole.User)
                {
                    if (!Authentication.CurrentUser.ID.Equals(photo.Owner.ID))
                        throw new HttpResponseException(HttpStatusCode.Unauthorized);
                }

                if (photo != null)
                {
                    photoRepository.Delete(photo);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        // api/photos/{id}/image
        [HttpGet]
        [GET("api/photos/{id}/image")]
        public HttpResponseMessage GetImage([FromUri] String id, [FromUri] Int32 width = 0, [FromUri] Int32 height = 0)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                Image image;

                if (photo != null)
                {
                    if (photo.Image != null)
                    {
                        if (width > 0 && height > 0)
                            image = ImageUtil.GetImage(photo.Image, width, height);
                        else
                            image = ImageUtil.GetImage(photo.Image);

                        if (Authentication.CurrentRole == UserRole.Anonymous)
                        {
                            ImageUtil.PixellateBlur(image);
                        }
                    }
                    else
                    {
                        image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                    }
                }
                else
                {
                    image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                }

                response.Content = new StreamContent(new MemoryStream(ImageUtil.GetBytes(image, ImageFormat.Png)));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                return response;
            }
        }
        


        [HttpGet]
        [GET("api/photos/{id}/markedface")]
        public dynamic GetMarkedFace([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null && photo.MarkedFace != null)
                {
                    Face face = photo.MarkedFace;

                    return new
                    {
                        ID = face.ID,
                        Width = face.Width,
                        Height = face.Height,
                        OffsetX = face.OffsetX,
                        OffsetY = face.OffsetY,
                        SourcePhoto = face.SourcePhoto.ID,
                        Valid = face.Valid,
                        Marked = face.Marked,
                        CreationDateTime = face.CreationDateTime
                    };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        // api/photos/{id}/markedface/image
        [HttpGet]
        [GET("api/photos/{id}/markedface/image")]
        public HttpResponseMessage GetMarkedFaceImage([FromUri] String id, [FromUri] Int32 width = 0, [FromUri] Int32 height = 0)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                Image image;

                if (photo != null)
                {
                    if (photo.MarkedFace != null)
                    {
                        if (width > 0 && height > 0)
                            image = ImageUtil.GetImage(photo.MarkedFace.Image, width, height);
                        else
                            image = ImageUtil.GetImage(photo.MarkedFace.Image);

                        if (Authentication.CurrentRole == UserRole.Anonymous)
                        {
                            ImageUtil.PixellateBlur(image);
                        }
                    }
                    else
                    {
                        image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                    }
                }
                else
                {
                    image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                }

                response.Content = new StreamContent(new MemoryStream(ImageUtil.GetBytes(image, ImageFormat.Png)));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                return response;
            }
        }


        #region Pending...
        [HttpPost]
        [POST("api/photos/{id}/faces")] // ADD
        public dynamic AddFace([FromUri] String id, [FromBody] dynamic faceInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {
                    try
                    {
                        Face face = new Face() 
                        {
                            OffsetX = faceInfo.OffsetX,
                            OffsetY = faceInfo.OffsetY,
                            Height = faceInfo.Height,
                            Width = faceInfo.Width,
                            SourcePhoto = photo,
                            CreationDateTime = DateTime.UtcNow 
                        };

                        Image photoImage = ImageUtil.GetImage(photo.Image);
                        Image faceImage = ImageUtil.Crop(photoImage, new Rectangle(face.OffsetX, face.OffsetY, face.Width, face.Height));

                        face.Image = ImageUtil.GetBytes(faceImage);

                        photo.Faces.Add(face);
                        faceRepository.Add(face);

                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }

            return "please do it";
        }
        #endregion


        // api/photos/{id}/faces
        [HttpGet]
        [GET("api/photos/{id}/faces")]
        public dynamic GetFaces([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {
                    return from face in photo.Faces.ToList()
                           select new
                           {
                                ID = face.ID,
                                Width = face.Width,
                                Height = face.Height,
                                OffsetX = face.OffsetX,
                                OffsetY = face.OffsetY,
                                SourcePhoto = face.SourcePhoto.ID,
                                Valid = face.Valid,
                                Marked = face.Marked,
                                CreationDateTime = face.CreationDateTime
                            };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        // api/photos/{id}/comments + add, edit, delete
        [HttpGet]
        [GET("api/photos/{photoID}/comments")] // need update!!!!!!!!!!
        public dynamic GetComments(
            // Resource
            [FromUri] String photoID,

            // Filter criterias
            [FromUri] String owner = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "creationdatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(photoID);

                if (photo != null)
                {
                    var comments = photo.Comments.AsQueryable();

                    if (!String.IsNullOrWhiteSpace(owner))
                        comments = comments.Where(comment => comment.Owner.ID.Equals(owner));


                    if (!String.IsNullOrEmpty(orderBy))
                    {
                        switch (orderBy.Trim().ToLower())
                        {
                            case "owner":
                                comments = comments.OrderBy(comment => comment.Owner);
                                break;

                            case "creationdatetime":
                            default:
                                comments = comments.OrderBy(comment => comment.CreationDateTime);
                                break;
                        }
                    }

                    var result = comments.AsEnumerable();

                    switch (order.Trim().ToLower())
                    {
                        case "desc":
                            result = result.Reverse();
                            break;
                    }

                    if (!String.IsNullOrWhiteSpace(creationDateTime))
                        result = result.Where(comment => DateTimeUtil.Eval(creationDateTime, comment.CreationDateTime));

                    if (page > 0 && limit > 0)
                        result = result.Skip((page - 1) * limit).Take(limit);

                    return from comment in result.ToList()
                           select new
                           {
                               ID = comment.ID,
                               Message = comment.Message,
                               CreationDateTime = comment.CreationDateTime,
                               Owner = comment.Owner.ID
                           };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpGet]
        [GET("api/photos/{photoID}/comments/info")] // need update!!!!!!!!!!
        public dynamic GetCommentsInfo(
            // Resource
            [FromUri] String photoID,

            // Filter criterias
            [FromUri] String owner = null,
            [FromUri] String creationDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "creationdatetime",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(photoID);

                if (photo != null)
                {
                    var comments = photo.Comments.AsQueryable();

                    // Int64 total = comments.LongCount();

                    if (!String.IsNullOrWhiteSpace(owner))
                        comments = comments.Where(comment => comment.Owner.ID.Equals(owner));


                    if (!String.IsNullOrEmpty(orderBy))
                    {
                        switch (orderBy.Trim().ToLower())
                        {
                            case "owner":
                                comments = comments.OrderBy(comment => comment.Owner);
                                break;

                            case "creationdatetime":
                            default:
                                comments = comments.OrderBy(comment => comment.CreationDateTime);
                                break;
                        }
                    }

                    var result = comments.AsEnumerable();

                    switch (order.Trim().ToLower())
                    {
                        case "desc":
                            result = result.Reverse();
                            break;
                    }

                    if (!String.IsNullOrWhiteSpace(creationDateTime))
                        result = result.Where(comment => DateTimeUtil.Eval(creationDateTime, comment.CreationDateTime));

                    Int64 resultTotal = result.LongCount();

                    if (page > 0 && limit > 0)
                        result = result.Skip((page - 1) * limit).Take(limit);

                    var info = new
                    {
                        TotalItems = resultTotal,
                        TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                        MaxItemsPerPage = limit,
                        CurrentPage = page,
                        CurrentPageItems = result.ToList().LongCount()
                    };

                    return info;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpPost]
        [POST("api/photos/{id}/comments")]
        [Authorize(Roles = "User, Administrator, System")]
        public dynamic AddComment([FromUri] String id, [FromBody] dynamic commentInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Comment> commentRepository = new NHRepository<String, Comment>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {
                    try
                    {
                        User owner = userRepository.FindBy(Authentication.CurrentUser.ID);

                        Comment comment = new Comment()
                        {
                            Message = commentInfo.Message,
                            CreationDateTime = DateTime.UtcNow,
                            Owner = owner
                        };

                        photo.Comments.Add(comment);
                        owner.Comments.Add(comment);

                        var commentID = commentRepository.Add(comment);

                        unitOfWork.Commit();

                        return commentID;
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        // api/photos/{id}/link?child={child}

        [HttpPost]
        [POST("api/photos/{id}/link")]
        [Authorize(Roles = "User, Administrator, System")]
        public void LinkChild([FromUri] String id, [FromUri] String child)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);
                Child targetChild = childRepository.FindBy(child);

                if (photo != null && targetChild != null)
                {
                    photo.MarkedChild = targetChild;
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        // api/photos/{id}/unlink
        [HttpPost]
        [POST("api/photos/{id}/unlink")]
        [Authorize(Roles = "User, Administrator, System")]
        public void UnlinkChild([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null && photo.MarkedChild != null)
                {
                    photo.MarkedChild = null;
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        [HttpGet]
        [GET("api/photos/{id}/alikechild")]
        public dynamic FindLookAlikeChild([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                // NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);
                List<Photo> photos = photoRepository.All().ToList();

                if ((photo != null) && (photos.Count > 0))
                {
                    // BETA, don't trust me...
                    return FaceRecognizer.FindLookAlikeChild(photo, photos);
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            
            }
        }

        // api/photos/{id}/mark?face={face}
        [HttpPost]
        [POST("api/photos/{id}/mark")] // change to post
        [Authorize(Roles = "User, Administrator, System")]
        public void MarkFace([FromUri] String id, [FromUri] String face)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Face> faceRepository = new NHRepository<String, Face>(unitOfWork);

                Photo photo = photoRepository.FindBy(id);

                if (photo != null)
                {
                    Face targetFace = faceRepository.FindBy(face);

                    if (targetFace != null && targetFace.SourcePhoto.ID.Equals(photo.ID))
                    {
                        if (photo.MarkedFace != null)
                        {
                            photo.MarkedFace.Marked = false;
                        }

                        targetFace.Marked = true;
                        photo.MarkedFace = targetFace;
                        photo.LastUpdateDateTime = DateTime.UtcNow;
                        unitOfWork.Commit();
                    }
                    else
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

    }
}