﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Net;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class CommentController : ApiController
    {
        [HttpGet]
        [GET("api/comments/{id}")]
        public dynamic GetComment([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Comment> commentRepository = new NHRepository<String, Comment>(unitOfWork);

                Comment comment = commentRepository.FindBy(id);

                if (comment != null)
                {
                    return new
                    {
                        ID = comment.ID,
                        Message = comment.Message,
                        CreationDateTime = comment.CreationDateTime,
                        Owner = comment.Owner.ID
                    };
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpPost]
        [POST("api/comments/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void UpdateComment([FromUri] String id, [FromBody] dynamic commentInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Comment> commentRepository = new NHRepository<String, Comment>(unitOfWork);

                Comment comment = commentRepository.FindBy(id);

                if (comment != null)
                {
                    var message = (String)commentInfo.Message;

                    if (Authentication.CurrentRole == UserRole.User)
                    {
                        if (!Authentication.CurrentUser.ID.Equals(comment.Owner.ID))
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    if (!String.IsNullOrWhiteSpace(message))
                        comment.Message = message;

                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpDelete]
        [DELETE("api/comments/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void DeleteComment([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Comment> commentRepository = new NHRepository<String, Comment>(unitOfWork);

                Comment comment = commentRepository.FindBy(id);

                if (comment != null)
                {
                    if (Authentication.CurrentRole == UserRole.User)
                    {
                        if (!Authentication.CurrentUser.ID.Equals(comment.Owner.ID))
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    commentRepository.Delete(comment);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }
    }
}