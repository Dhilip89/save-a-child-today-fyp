﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Security;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class AuthController : ApiController
    {
        [HttpPost]
        [POST("api/auth/signin")]
        public void SignIn([FromBody] dynamic credential)
        {
            Boolean authSuccess = Authentication.Authenticate((String)credential.Email, (String)credential.Password, (Boolean)credential.Persistent);

            if (!authSuccess)
                throw new HttpResponseException(HttpStatusCode.Forbidden);
        }

        [HttpPost]
        [POST("api/auth/signout")]
        public void SignOut()
        {
            Authentication.Logout();
        }

        [HttpGet]
        [GET("api/auth/info")]
        [Authorize(Roles = "User, Administrator, System")]
        public dynamic GetAuthInfo()
        {
            OrigineIT.SaveAChildToday.DataAccess.Entities.User user = Authentication.CurrentUser;
            FormsAuthenticationTicket ticket = Authentication.Ticket;

            return new
            {
                ID = user.ID,
                Email = user.Email,
                Name = user.Name,
                Verified = user.Verified,
                LastSignedIn = user.LastLoginDateTime,
                Ticket = new { IssueDate = ticket.IssueDate, Expiration = ticket.Expiration, CookieName = FormsAuthentication.FormsCookieName, Persistent = ticket.IsPersistent},
            };
        }

    }
}