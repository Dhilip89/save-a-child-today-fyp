﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    class TimeMapData
    {
        private IList<MapPoint> _polyline;

        public String title { get; set; }
        public DateTime start { get; set; }
        public DateTime? end { get; set; }
        public MapPoint point { get; set; }

        public virtual IList<MapPoint> polyline
        {
            get { return _polyline ?? (_polyline = new List<MapPoint>()); }
            protected set { _polyline = value; }
        }

        public virtual dynamic options { get; set; }

        public TimeMapData()
        {
            this.start = DateTimeUtil.GetSimpleDateTime(DateTime.UtcNow);
            this.end = this.start;
        }
    }

    class MapPoint
    {
        public Double lon { get; set; }
        public Double lat { get; set; }
    }

    public class TimeMapController : ApiController
    {
        [HttpGet]
        [GET("api/timemap/children")]
        public dynamic GetChildrenData()
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                List<dynamic> payload = new List<dynamic>();

                var children = childRepository.All();
                                //.Where(c => c.ProfilePicture != null);

                foreach (Child child in children)
                {
                    Photo photo = photoRepository.All()
                                    .Where(p => p.MarkedChild.ID.Equals(child.ID))
                                    .OrderBy(p => p.CaptureDateTime)
                                    .Select(p => p)
                                    .FirstOrDefault();

                    TimeMapData data = new TimeMapData()
                    {
                        title = child.Name,
                        start = child.MissingDateTime,
                        end = null,
                        options = new
                        {
                            theme = child.Status == ChildStatus.Found ? "green" : "red",
                            infoHtml =
                                "<img class='pull-left thumbnail' style='width:64px; height:64px; margin-right:5px;' src='./api/children/" + child.ID + "/picture?width=64&height=64'>" +
                                "<table class='pull-right'>" +
                                "<tr><th>" + "<a href='./ChildProfile.aspx?id=" + child.ID + "'>"  + child.Name + "</a>" + "</th></tr>" +
                                "<tr><td>Gender: " + child.Gender.ToString() + "</td></tr>" +
                                "<tr><td>Birthday: " + child.Birthday.ToString("dd MMMM yyyy") + "</td></tr>" +
                                "<tr><td>Status: " + child.Status.ToString() + "</td></tr>" +
                                "</table>"
                        }
                    };

                    if ((photo != null) && (photo.Longitude != 0) && (photo.Latitude != 0))
                    {
                        data.point = new MapPoint() { lon = photo.Longitude, lat = photo.Latitude };
                    }
                    else
                    {
                        Country country = countryRepository.FindBy(child.MissingCountry);

                        data.point = new MapPoint() { lon = country.Longitude, lat = country.Latitude };
                    }

                    payload.Add(data);
                }

                return payload;
            }


        }

        [HttpGet]
        [GET("api/timemap/children/{id}")]
        public dynamic GetChildData([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Child> childRepository = new NHRepository<String, Child>(unitOfWork);
                NHRepository<String, Photo> photoRepository = new NHRepository<String, Photo>(unitOfWork);
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                Child child = childRepository.FindBy(id);

                if (child != null)
                {
                    List<dynamic> payload = new List<dynamic>();


                    var photos = photoRepository.All()
                                    .Where(p => p.MarkedChild.ID.Equals(child.ID))
                                    .OrderBy(p => p.CaptureDateTime)
                                    .ToList();

                    IEnumerator<Photo> enumerator = photos.GetEnumerator();
                    bool hasNext = enumerator.MoveNext();

                    bool first = true;

                    while (hasNext)
                    {
                        Photo current = enumerator.Current;

                        TimeMapData data = new TimeMapData()
                        {
                            title = current.Caption,
                            start = current.CaptureDateTime,
                            point = new MapPoint() { lat = current.Latitude, lon = current.Longitude },
                        };

                        data.options = new 
                        { 
                            theme = first ? "orange" : "blue",
                            lineColor = "red",
                            lineWeight = 3,
                            lineOpacity = 0.8,
                            infoHtml =
                                "<img class='pull-left thumbnail' style='width:64px; height:64px; margin-right:5px;' src='./api/photos/" + current.ID + "/markedface/image?width=64&height=64'>" +
                                "<table class='pull-right'>" +
                                "<tr><th>" + current.CaptureDateTime.ToString("dd MMMM yyyy - hh:mm tt") + "</th></tr>" +
                                "<tr><th>" + current.Caption + "</th></tr>" +
                                "<tr><td><em>" + current.Description.Replace("\n", "<br />") + "</em></td></tr>" +
                                "</table>"
                        };

                        hasNext = enumerator.MoveNext();

                        first = false;

                        if (hasNext)
                        {
                            Photo next = enumerator.Current;

                            data.end = next.CaptureDateTime;
                            data.polyline.Add(new MapPoint() { lat = current.Latitude, lon = current.Longitude });
                            data.polyline.Add(new MapPoint() { lat = next.Latitude, lon = next.Longitude });
                        }

                        payload.Add(data);
                    }

                    enumerator.Dispose();

                    return payload;
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }


        [HttpGet]
        [GET("api/timemap/debug")]
        public dynamic Debug()
        {
            List<dynamic> payload = new List<dynamic>();

            payload.Add(new
            {
                title = "This is a test",
                start = DateTimeUtil.GetSimpleDateTime(DateTime.UtcNow),
                //end = DateTime.Now.AddDays(300),
                point = new { lon = 9.140625, lat = 45.49094569262732 },
                polyline = new dynamic[] { 
                                    new {lat= 48.86471476180277, lon= 2.28515625},
                                    new {lat= 45.82879925192134, lon= 4.74609375},
                                    new {lat= 45.49094569262732, lon= 9.140625},
                                    new {lat= 41.934976500546604, lon= 12.392578125}
                    },
                options = new
                {
                    theme = "blue",
                    infoHtml =
                        "<img class='pull-left thumbnail' style='width:64px; height:64px; margin-right:5px;' src='./api/children/" + "0xDEADBEEF" + "/picture'>" +
                        "<table class='pull-right'>" +
                        "<tr><th>" + "DEBUG" + "</th></tr>" +
                        "<tr><td>Status: " + "Debug" + "</td></tr>" +
                        "<tr><td><a class='btn btn-primary btn-xs' href='./ChildProfile.aspx?id=" + "0xDEADBEEF" + "'>Profile</a></td></tr>" +
                        "</table>"
                }
            });


            return payload;
        }

    }
}