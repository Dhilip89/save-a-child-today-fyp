﻿using AForge.Imaging.Filters;
using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using OrigineIT.SaveAChildToday.DataAccess.Utils;
using OrigineIT.SaveAChildToday.Web.Imaging;
using OrigineIT.SaveAChildToday.Web.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class UsersController : ApiController
    {
        [HttpGet]
        [GET("api/users")]
        public dynamic GetUsers(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,
            [FromUri] String contactNumber = null,
            [FromUri] String website = null,
            [FromUri] String country = null,
            [FromUri] String email = null,
            [FromUri] Boolean? verified = null,
            [FromUri] String role = null,
            [FromUri] String creationDateTime = null,
            [FromUri] String verificationDateTime = null,
            [FromUri] String lastLoginDateTime = null,
            [FromUri] String lastUpdateDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                var users = userRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    users = users.Where(user => user.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    users = users.Where(user => user.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(contactNumber))
                    users = users.Where(user => user.ContactNumber.Contains(contactNumber));

                if (!String.IsNullOrWhiteSpace(website))
                    users = users.Where(user => user.Website.Contains(website));

                if (!String.IsNullOrWhiteSpace(country))
                    users = users.Where(user => user.Country.Equals(country));

                if (!String.IsNullOrWhiteSpace(email))
                    users = users.Where(user => user.Email.Contains(email));

                if (!String.IsNullOrWhiteSpace(role))
                    users = users.Where(user => user.Role.ToString().Equals(role));
                
                if (verified != null)
                    users = users.Where(user => user.Verified == verified);


                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            users = users.OrderBy(user => user.ID);
                            break;

                        case "contactnumber":
                            users = users.OrderBy(user => user.ContactNumber);
                            break;

                        case "email":
                            users = users.OrderBy(user => user.Email);
                            break;

                        case "website":
                            users = users.OrderBy(user => user.Website);
                            break;

                        case "role":
                            users = users.OrderBy(user => user.Role);
                            break;

                        case "comments":
                            users = users.OrderBy(user => user.Comments.Count);
                            break;

                        case "children":
                            users = users.OrderBy(user => user.Children.Count);
                            break;

                        case "status":
                            users = users.OrderBy(user => user.Verified);
                            break;

                        case "country":
                            users = users.OrderBy(user => user.Country);
                            break;

                        case "photos":
                            users = users.OrderBy(user => user.Photos.Count);
                            break;

                        case "creationdatetime":
                            users = users.OrderBy(user => user.CreationDateTime);
                            break;

                        case "lastlogindatetime":
                            users = users.OrderBy(user => user.LastLoginDateTime);
                            break;

                        case "lastupdatedatetime":
                            users = users.OrderBy(user => user.LastUpdateDateTime);
                            break;

                        case "verificationdatetime":
                            users = users.OrderBy(user => user.VerificationDateTime);
                            break;

                        case "name":
                        default:
                            users = users.OrderBy(user => user.Name);
                            break;
                    }
                }

                var result = users.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(creationDateTime, user.CreationDateTime));

                if (!String.IsNullOrWhiteSpace(verificationDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(verificationDateTime, user.VerificationDateTime));

                if (!String.IsNullOrWhiteSpace(lastLoginDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastLoginDateTime, user.LastLoginDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastUpdateDateTime, user.LastUpdateDateTime));


                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return from user in result.ToList()
                       select new
                       {
                           ID = user.ID,
                           Name = user.Name,
                           ContactNumber = user.ContactNumber,
                           Website = user.Website,
                           Country = user.Country,
                           Email = user.Email,
                           Verified = user.Verified,
                           CreationDateTime = user.CreationDateTime,
                           VerificationDateTime = user.VerificationDateTime,
                           LastLoginDateTime = user.LastLoginDateTime,
                           LastUpdateDateTime = user.LastUpdateDateTime,
                           Role = user.Role.ToString(),
                           Comments = user.Comments.Count,
                           Children = user.Children.Count,
                           Photos = user.Photos.Count
                       };
            }
        }


        [HttpGet]
        [GET("api/users/info")]
        public dynamic GetUsersInfo(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,
            [FromUri] String contactNumber = null,
            [FromUri] String website = null,
            [FromUri] String country = null,
            [FromUri] String email = null,
            [FromUri] Boolean? verified = null,
            [FromUri] String role = null,
            [FromUri] String creationDateTime = null,
            [FromUri] String verificationDateTime = null,
            [FromUri] String lastLoginDateTime = null,
            [FromUri] String lastUpdateDateTime = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                var users = userRepository.All();

                // Int64 total = users.LongCount();

                if (!String.IsNullOrWhiteSpace(id))
                    users = users.Where(user => user.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    users = users.Where(user => user.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(contactNumber))
                    users = users.Where(user => user.ContactNumber.Contains(contactNumber));

                if (!String.IsNullOrWhiteSpace(website))
                    users = users.Where(user => user.Website.Contains(website));

                if (!String.IsNullOrWhiteSpace(country))
                    users = users.Where(user => user.Country.Equals(country));

                if (!String.IsNullOrWhiteSpace(email))
                    users = users.Where(user => user.Email.Contains(email));

                if (!String.IsNullOrWhiteSpace(role))
                    users = users.Where(user => user.Role.ToString().Equals(role));

                if (verified != null)
                    users = users.Where(user => user.Verified == verified);


                if (!String.IsNullOrEmpty(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            users = users.OrderBy(user => user.ID);
                            break;

                        case "contactnumber":
                            users = users.OrderBy(user => user.ContactNumber);
                            break;

                        case "email":
                            users = users.OrderBy(user => user.Email);
                            break;

                        case "website":
                            users = users.OrderBy(user => user.Website);
                            break;

                        case "role":
                            users = users.OrderBy(user => user.Role);
                            break;

                        case "comments":
                            users = users.OrderBy(user => user.Comments.Count);
                            break;

                        case "children":
                            users = users.OrderBy(user => user.Children.Count);
                            break;

                        case "status":
                            users = users.OrderBy(user => user.Verified);
                            break;

                        case "country":
                            users = users.OrderBy(user => user.Country);
                            break;

                        case "photos":
                            users = users.OrderBy(user => user.Photos.Count);
                            break;

                        case "creationdatetime":
                            users = users.OrderBy(user => user.CreationDateTime);
                            break;

                        case "lastlogindatetime":
                            users = users.OrderBy(user => user.LastLoginDateTime);
                            break;

                        case "lastupdatedatetime":
                            users = users.OrderBy(user => user.LastUpdateDateTime);
                            break;

                        case "verificationdatetime":
                            users = users.OrderBy(user => user.VerificationDateTime);
                            break;

                        case "name":
                        default:
                            users = users.OrderBy(user => user.Name);
                            break;
                    }
                }

                var result = users.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }


                if (!String.IsNullOrWhiteSpace(creationDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(creationDateTime, user.CreationDateTime));

                if (!String.IsNullOrWhiteSpace(verificationDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(verificationDateTime, user.VerificationDateTime));

                if (!String.IsNullOrWhiteSpace(lastLoginDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastLoginDateTime, user.LastLoginDateTime));

                if (!String.IsNullOrWhiteSpace(lastUpdateDateTime))
                    result = result.Where(user => DateTimeUtil.Eval(lastUpdateDateTime, user.LastUpdateDateTime));

                Int64 resultTotal = result.LongCount();

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                var info = new
                {
                    TotalItems = resultTotal,
                    TotalPages = (limit > 0) ? (Int64) Math.Ceiling((Double) resultTotal / limit) : 1,
                    MaxItemsPerPage = limit,
                    CurrentPage = page,
                    CurrentPageItems = result.ToList().LongCount()
                };

                return info;
            }
        }


        [HttpGet]
        [GET("api/users/{id}")]
        public dynamic GetUser([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                User user = userRepository.FindBy(id);

                if (user != null)
                    return new
                    {
                        ID = user.ID,
                        Name = user.Name,
                        ContactNumber = user.ContactNumber,
                        Website = user.Website,
                        Country = user.Country,
                        Email = user.Email,
                        Verified = user.Verified,
                        CreationDateTime = user.CreationDateTime,
                        VerificationDateTime = user.VerificationDateTime,
                        LastLoginDateTime = user.LastLoginDateTime,
                        LastUpdateDateTime = user.LastUpdateDateTime,
                        Role = user.Role.ToString(),
                        Comments = user.Comments.Count,
                        Children = user.Children.Count,
                        Photos = user.Photos.Count
                    };
                else
                    throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpPost]
        [POST("api/users")]
        public dynamic AddUser([FromBody] dynamic userInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                try
                {
                    User user = new User()
                        {
                            Name = userInfo.Name,
                            Role = UserRole.User,
                            Email = userInfo.Email,
                            ContactNumber = userInfo.ContactNumber,
                            HashedPassword = Password.CreateHash((String)userInfo.Password),
                            Verified = false,
                            Website = userInfo.Website,
                            Country = userInfo.CountryID,
                        };

                    if (Authentication.CurrentRole == UserRole.Administrator || Authentication.CurrentRole == UserRole.System)
                    {
                        user.Verified = userInfo.Verified;
                        user.Role = EnumUtil.ParseEnum<UserRole>((String)userInfo.Role);
                    }

                    var id = userRepository.Add(user);

                    unitOfWork.Commit();

                    return id;
                }
                catch (Exception)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }


        [HttpPost]
        [POST("api/users/{id}")]
        [Authorize(Roles = "User, Administrator, System")]
        public void UpdateUser([FromUri] String id, [FromBody] dynamic userInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                User user = userRepository.FindBy(id);

                if (user != null)
                {
                    var name = (String) userInfo.Name;
                    var contactNumber = (String) userInfo.ContactNumber;
                    var website = (String) userInfo.Website;
                    var country = (String) userInfo.Country;
                    var password = (String) userInfo.Password;

                    if (Authentication.CurrentRole == UserRole.User)
                    {
                        if (!Authentication.CurrentUser.ID.Equals(user.ID))
                            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                    }

                    if (!String.IsNullOrWhiteSpace(name))
                        user.Name = name;

                    if (!String.IsNullOrWhiteSpace(contactNumber))
                        user.ContactNumber = contactNumber;

                    if (!String.IsNullOrWhiteSpace(website))
                        user.Website = website;

                    if (!String.IsNullOrWhiteSpace(country))
                        user.Country = country;

                    if (!String.IsNullOrWhiteSpace(password))
                        user.HashedPassword = Password.CreateHash((String)userInfo.Password);

                    try
                    {
                        if (Authentication.CurrentRole == UserRole.Administrator || Authentication.CurrentRole == UserRole.System)
                        {
                            var role = (String) userInfo.Role;
                            var verified = userInfo.Verified;
                            
                            if (!String.IsNullOrWhiteSpace(role))
                                user.Role = EnumUtil.ParseEnum<UserRole>(role);

                            if (verified != null)
                                user.Verified = verified;
                        }

                        user.LastUpdateDateTime = DateTime.UtcNow;
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        [HttpDelete]
        [DELETE("api/users/{id}")]
        [Authorize(Roles = "Administrator, System")]
        public void DeleteUser([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                User user = userRepository.FindBy(id);

                if (user != null)
                {

                    userRepository.Delete(user);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        [HttpGet]
        [GET("api/users/{id}/picture")]
        public HttpResponseMessage GetProfilePicture([FromUri] String id, [FromUri] Int32 width = 0, [FromUri] Int32 height = 0)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                User user = userRepository.FindBy(id);

                Image image;

                if (user != null)
                {
                    if (user.ProfilePicture != null)
                    {
                        if (width > 0 && height > 0)
                            image = ImageUtil.GetImage(user.ProfilePicture, width, height);
                        else
                            image = ImageUtil.GetImage(user.ProfilePicture);

                        if (Authentication.CurrentRole == UserRole.Anonymous)
                        {
                            ImageUtil.PixellateBlur(image);
                        }
                    }
                    else
                    {
                        image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                    }
                }
                else
                {
                    image = ImageUtil.GetImage(File.ReadAllBytes(Global.DefaultProfilePicture));
                }

                response.Content = new StreamContent(new MemoryStream(ImageUtil.GetBytes(image, ImageFormat.Png)));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

                return response;
            }
        }


        [HttpPost]
        [POST("api/users/{id}/verify")]
        public void ActivateUser([FromUri] String id, [FromUri] String token)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                User user = userRepository.FindBy(id);

                if (user != null)
                {
                    try
                    {
                        if (user.VerificationToken.Equals(token, StringComparison.CurrentCultureIgnoreCase))
                        {
                            user.Verified = true;
                            user.VerificationDateTime = DateTime.UtcNow;
                            unitOfWork.Commit();
                        }
                        else
                        {
                            throw new HttpResponseException(HttpStatusCode.BadRequest);
                        }
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

    }
}