﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class CountriesController : ApiController
    {
        [HttpGet]
        [GET("api/countries")]
        public dynamic GetCountries(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",
            
            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                var countries = countryRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    countries = countries.Where(country => country.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    countries = countries.Where(country => country.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            countries = countries.OrderBy(country => country.ID);
                            break;

                        case "name":
                        default:
                            countries = countries.OrderBy(country => country.Name);
                            break;
                    }
                }

                var result = countries.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return result.ToList();
            }
        }

        [HttpGet]
        [GET("api/countries/info")]
        public dynamic GetCountriesInfo(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "name",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                var countries = countryRepository.All();

                //Int64 total = countries.LongCount();

                if (!String.IsNullOrWhiteSpace(id))
                    countries = countries.Where(country => country.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    countries = countries.Where(country => country.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "id":
                            countries = countries.OrderBy(country => country.ID);
                            break;

                        case "name":
                        default:
                            countries = countries.OrderBy(country => country.Name);
                            break;
                    }
                }

                var result = countries.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }

                Int64 resultTotal = result.LongCount();

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                var info = new
                {
                    TotalItems = resultTotal,
                    TotalPages = (limit > 0) ? (Int64)Math.Ceiling((Double)resultTotal / limit) : 1,
                    MaxItemsPerPage = limit,
                    CurrentPage = page,
                    CurrentPageItems = result.ToList().LongCount()
                };

                return info;
            }
        }

        [HttpGet]
        [GET("api/countries/{id}")]
        public dynamic GetCountry([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                var country = countryRepository.FindBy(id);

                if (country != null)
                    return country;
                else
                    throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        [HttpPost]
        [POST("api/countries")]
        [Authorize(Roles = "Administrator, System")]
        public dynamic AddCountry([FromBody] dynamic country)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);
                try
                {
                    var id = countryRepository.Add(new Country() 
                        { 
                          ID = country.ID,
                          Name = country.Name, 
                          Longitude = country.Longitude, 
                          Latitude = country.Latitude 
                        });

                    unitOfWork.Commit();

                    return id;
                }
                catch (Exception)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
        }


        [HttpPost]
        [POST("api/countries/{id}")]
        [Authorize(Roles = "Administrator, System")]
        public void UpdateCountry([FromUri] String id, [FromBody] dynamic countryInfo)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                Country country = countryRepository.FindBy(id);

                if (country != null)
                {
                    var name = (String)countryInfo.Description;
                    var longitude = (Double)countryInfo.Longitude;
                    var latitude = (Double)countryInfo.Latitude;

                    if (!String.IsNullOrWhiteSpace(name))
                        country.Name = name;

                    if (longitude != 0)
                        country.Longitude = longitude;

                    if (latitude != 0)
                        country.Latitude = latitude;

                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        throw new HttpResponseException(HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }

        [HttpDelete]
        [DELETE("api/countries/{id}")]
        [Authorize(Roles = "Administrator, System")]
        public void DeleteCountry([FromUri] String id)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                var country = countryRepository.FindBy(id);

                if (country != null)
                {

                    countryRepository.Delete(country);
                    unitOfWork.Commit();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
        }
    }
}