﻿using AttributeRouting.Web.Http;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace OrigineIT.SaveAChildToday.Web.ApiControllers
{
    public class LogsController : ApiController
    {
        // api/logs
        [HttpGet]
        [GET("api/logs")]
        [Authorize(Roles = "Administrator, System")]
        public dynamic GetLogs(
            // Filter criterias
            [FromUri] String id = null,
            [FromUri] String name = null,

            // Result ordering
            [FromUri] String order = "asc",
            [FromUri] String orderBy = "id",

            // Result paging
            [FromUri] Int32 page = 1,
            [FromUri] Int32 limit = 0)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, Country> countryRepository = new NHRepository<String, Country>(unitOfWork);

                var countries = countryRepository.All();

                if (!String.IsNullOrWhiteSpace(id))
                    countries = countries.Where(country => country.ID.Contains(id));

                if (!String.IsNullOrWhiteSpace(name))
                    countries = countries.Where(country => country.Name.Contains(name));

                if (!String.IsNullOrWhiteSpace(orderBy))
                {
                    switch (orderBy.Trim().ToLower())
                    {
                        case "name":
                            countries = countries.OrderBy(country => country.Name);
                            break;

                        case "id":
                        default:
                            countries = countries.OrderBy(country => country.ID);
                            break;
                    }
                }

                var result = countries.AsEnumerable();

                switch (order.Trim().ToLower())
                {
                    case "desc":
                        result = result.Reverse();
                        break;
                }

                if (page > 0 && limit > 0)
                    result = result.Skip((page - 1) * limit).Take(limit);

                return result.ToList();
            }
        }


        // api/logs add
        [HttpPost]
        [POST("api/logs/{id}")]
        [Authorize(Roles = "System")]
        public void AddLog(Int64 id)
        {

        }


        // api/logs/{id} get
        [HttpGet]
        [GET("api/logs/{id}")]
        [Authorize(Roles = "Administrator, System")]
        public void GetLog(Int64 id)
        {

        }


        // api/logs/{id} delete
        [HttpDelete]
        [DELETE("api/logs/{id}")]
        [Authorize(Roles = "Administrator, System")]
        public void DeleteLog(Int64 id)
        {

        }

    }
}