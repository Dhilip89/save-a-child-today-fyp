﻿using Emgu.CV;
using Emgu.CV.Structure;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace OrigineIT.SaveAChildToday.Web.Imaging
{
    public class FaceRecognizer
    {
        public static List<Face> DetectFaces(Photo photo)
        {
            List<Face> faces = new List<Face>();

            HaarCascade frontalFaceHaarCascade = new HaarCascade(Global.FrontalFaceHaarCascade);

            Image<Bgr, Byte> originalImage = new Image<Bgr, Byte>(new Bitmap(ImageUtil.GetImage(photo.Image)));
            Image<Gray, Byte> grayscaleImage = originalImage.Convert<Gray, Byte>();

            MCvAvgComp[][] facesDetected = grayscaleImage.DetectHaarCascade(frontalFaceHaarCascade, 1.068, 4, 0, new Size(32, 32));

            foreach (MCvAvgComp m in facesDetected[0])
            {
                faces.Add(new Face()
                {
                    Image = ImageUtil.GetBytes(originalImage.Copy(m.rect).ToBitmap(), ImageFormat.Png),
                    Width = m.rect.Width,
                    Height = m.rect.Height,
                    OffsetX = m.rect.X,
                    OffsetY = m.rect.Y,
                    CreationDateTime = DateTime.UtcNow,
                    SourcePhoto = photo
                });
            }
            frontalFaceHaarCascade.Dispose();

            return faces;
        }

        public static String FindLookAlikeChild(Photo targetPhoto, List<Photo> photos)
        {
            if (targetPhoto.MarkedFace != null)
            {
                photos = photos.Where(photo => !photo.ID.Equals(targetPhoto.ID) && photo.MarkedFace != null && photo.MarkedChild != null).ToList();

                if (photos.Count > 0)
                {
                    MCvTermCriteria termCrit = new MCvTermCriteria(photos.Count, 0.025);

                    // Child[] children = (from p in photos select p.MarkedChild).ToArray();
                    String[] childIDs = (from p in photos select p.MarkedChild.ID).ToArray();

                    Image<Gray, Byte>[] faces = (from p in photos
                                                 select 
                                                 EqualizeHist(new Image<Bgr, Byte>(new Bitmap(ImageUtil.GetImage(p.MarkedFace.Image)))
                                                 .Convert<Gray, Byte>()
                                                 .Resize(128, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC))
                                                 ).ToArray();

                    /*
                    int i = 0;
                    foreach (Image<Gray, Byte> fa in faces)
                    {
                        fa.Save(@"p:\dbg\" + i + "_" + childIDs[i] + ".bmp");
                        i++;
                    }*/


                    EigenObjectRecognizer recognizer = new EigenObjectRecognizer(faces, childIDs, 1250, ref termCrit);

                    Image<Gray, Byte> targetFace = new Image<Bgr, Byte>(new Bitmap(ImageUtil.GetImage(targetPhoto.MarkedFace.Image)))
                        .Convert<Gray, Byte>();
                        //.Resize(128, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    targetFace = targetFace.Resize(128, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

                    targetFace = EqualizeHist(targetFace);

                    //targetFace.Save(@"p:\dbg\" + "target" + ".bmp");

                    String detectedChildID = recognizer.Recognize(targetFace);

                    return detectedChildID;

                    //if (!String.IsNullOrWhiteSpace(detectedChildID))
                    //    return children.Where(child => child.ID.Equals(detectedChildID)).FirstOrDefault();

                }

            }

            return null;
        }


        public static Image<Gray, Byte> EqualizeHist(Image<Gray, Byte> input)
        {
            Image<Gray, Byte> output = new Image<Gray, Byte>(input.Width, input.Height);
            CvInvoke.cvEqualizeHist(input.Ptr, output.Ptr);
            return output;
        }

    }
}