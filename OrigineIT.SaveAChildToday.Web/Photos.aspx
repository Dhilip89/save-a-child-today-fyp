﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Photos.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.Photos" %>

<!doctype html>
<html>
<head>
    <title>Save A Child Today - Find Missing Children Worldwide</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./StyleSheets/bootstrap.min.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-custom.css">
    <link rel="stylesheet" href="./StyleSheets/font-awesome.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="./StyleSheets/internal-common.css">

    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false'></script>
    <script type="text/javascript" src="./Scripts/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootbox.min.js"></script>
    <script type="text/javascript" src="./Scripts/date.js"></script>
    <script type="text/javascript" src="./Scripts/internal-common.js"></script>

    <script type="text/javascript">
        // Shared variables

        var photosPage = 1;
        var pagerAnimateDirection = "up";
        var geocoder = new google.maps.Geocoder();

        var selectChildPage = 1;
        var selectChildPagerAnimateDirection = "up";

        // Map
        var photoUploadLongitude = 0.0;
        var photoUploadLatitude = 0.0;
        var photoUploadMap, photoUploadMapMarker;
    </script>

</head>

<body>

    <!-- Navigation bar -->
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Save A Child Today</a>
            </div>
            <ul class="nav navbar-nav navbar-right user-item hide">
                <li>
                    <a href="./MyProfile.aspx">My Profile</a>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./Index.aspx">Home</a>
                    </li>
                    <li class="active">
                        <a href="#">Photos</a>
                    </li>
                    <li>
                        <a href="./People.aspx">People</a>
                    </li>
                    <li>
                        <a href="./About.aspx">About</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <a id="btn-navbar-signin" class="btn btn-primary guest-item hide" href="#modal-signin" data-toggle="modal"><i class="fa fa-sign-in"></i>&nbsp;Sign in</a>
                    <a id="btn-navbar-signout" class="btn btn-danger user-item hide" href="#"><i class="fa fa-sign-out"></i>&nbsp;Sign out</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Menu -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left btn-group hidden">
                    <a href="./TimelineMap.aspx" class="btn btn-default"><i class="fa fa-user"></i>&nbsp;Face</a><a href="#" class="btn btn-success"><i class="fa fa-instagram"></i> Photo</a>
                </div>
                <span class="btn"><i class="fa fa-unlink"></i>&nbsp;<span id="status-photos-unlinked-count">0</span>&nbsp;Unlinked&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-link"></i>&nbsp;<span id="status-photos-linked-count">0</span>&nbsp;Linked&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user"></i>&nbsp;<span id="status-photos-inuse-count">0</span>&nbsp;In Use</span>
                <div class="btn-toolbar pull-right">
                    <div class="btn-group">
                        <a href="#modal-upload-photo" data-toggle="modal" class="btn btn-default user-item hide"><i class="fa fa-upload"></i>&nbsp;Upload Photo</a>
                    </div>
                    <div class="btn-group hidden">
                        <a id="btn-filter" class="btn btn-default" href="#modal-filter" data-toggle="modal"><i class="fa fa-filter"></i>&nbsp;Filter</a><a id="btn-filter-menu" class="btn dropdown-toggle btn-default" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a id="btn-clearfilter" href="#"><i class="fa fa-times"></i>&nbsp;Clear Filter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Separator line -->
    <hr>

    <!-- Photos container -->
    <div class="container">
        <ul id="container-gallery" class="row">
            <!-- Items -->
        </ul>
    </div>

    <!-- Pager -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul id="pager-gallery" class="pagination pull-left">
                    <!-- Items -->
                </ul>
            </div>
        </div>
    </div>







    <!-- ----------------------------- Page specific modals ----------------------------- -->

    <div id="modal-photo-info" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-photo-info-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-picture-o"></i>&nbsp;About This Photo</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group well well-sm" style="width: 100%; max-height: 325px;">

                            <div class="" id="modal-photo-info-face-highlight-box" style="position: absolute; background: rgba(76, 255, 0, 0.20); border: 2px solid #4cff00; width: 0px; height: 0px"></div>

                            <img class="thumbnail center-block" id="modal-photo-info-image" style="max-width: 100%; max-height: 305px;" src="" />
                        </div>

                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading"></h4>
                                <!-- Photo info -->
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a id="modal-photo-info-btn-profile" class="btn btn-primary user-item" href="#">Link</a>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="modal-upload-photo" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-upload-photo-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-upload"></i>&nbsp;Upload Photo</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <label class="control-label" for="file">Image File:&nbsp;</label>
                                <div class="input-group">
                                    <div class="form-control uneditable-input" data-trigger="fileinput"><i class="glyphicon glyphicon-picture fileinput-exists"></i>&nbsp;<span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input class="file-input" type="file" name="file" accept="image/*"></span>
                                    <a href="#" id="btn-file-reset" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="caption">Caption:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                <input class="form-control" placeholder="Enter photo caption" name="caption" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="description">Description:&nbsp;</label>
                            <div class="form-group">
                                <textarea name="description" class="form-control" placeholder="Tell us more about this photo..." rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="takendatetime">Taken Date/Time:&nbsp;</label>
                            <div id="modal-upload-photo-form-takendatetime" class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-format="yyyy-mm-dd">
                                <input name="takendatetime" class="form-control" size="16" type="text" placeholder="Select a date and time" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <input type="hidden" id="latitude" value="0" />
                        <input type="hidden" id="longitude" value="0" />
                        <input type="hidden" id="radius" value="0.1" />


                        <div class="form-group">
                            <label class="control-label">Taken Location:&nbsp;</label>
                            <div id="map" class="map-canvas" style="width: 100%; height: 280px; background-color: #000000;"></div>
                            <input style="display: none" type="text" id="country" name="country" value="" />
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->

    <!-- select face modal -->

    <div id="modal-select-face" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-select-face-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-user"></i>&nbsp;Select Face</h3>
                    </div>
                    <div class="modal-body">

                        <!-- face select ui -->
                        <div class="form-group well well-sm" style="width: 100%; max-height: 325px;">
                            <div class="" id="face-selector" style="position: absolute; background: rgba(76, 255, 0, 0.20); border: 2px solid #4cff00; width: 0px; height: 0px"></div>
                            <div class="" id="face-highligher" style="position: absolute; background: rgba(255, 216, 0, 0.20); border: 2px solid #ffd800; width: 0px; height: 0px"></div>

                            <img class="thumbnail center-block" id="modal-select-face-form-image" style="max-width: 100%; max-height: 305px;" src="" />
                            <input style="display: none" type="text" id="photo-input" name="photo" value="" />
                        </div>

                        <div class="form-group">
                            <ul id="modal-select-face-form-faces" class="form-group row"></ul>
                            <input style="display: none" type="text" id="face-input" name="face" value="" />
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="btn-delete-face-photo" type="button" class="btn btn-danger pull-left">Discard</button>
                        <button id="btn-skip-face" type="button" class="btn btn-default">Skip</button>
                        <button id="btn-confirm-face" type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <!-- select child modal -->

    <div id="modal-select-child" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 800px">
            <form role="form" id="modal-select-child-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-user"></i>&nbsp;Select Child</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <ul id="modal-select-child-form-children" class="form-group row"></ul>
                            <input style="display: none" type="text" id="child-input" name="child" value="" />
                        </div>

                        <div class="form-group">

                            <ul id="modal-select-child-form-pager" class="pagination pull-left">
                                <!-- pages -->
                            </ul>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="btn-cancel-child" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->




    <!-- link child modal -->

    <div id="modal-link-child" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-link-child-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-link"></i>&nbsp;Link Child</h3>
                    </div>
                    <div class="modal-body">

                        <div class="media">
                            <a class="pull-left" href="#">
                                <img style="width: 128px; height: 128px" class="media-object thumbnail" src="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"></h4>
                                <!-- child info -->
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!--button type="submit" class="btn btn-primary">Profile</!--button-->
                        <a id="modal-link-child-btn-link" class="btn btn-primary" href="#">Link</a>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


        <!-- ----------------------------- Common modals ----------------------------- -->

    <!-- Sign in modal -->
    <div id="modal-signin" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signin-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i>&nbsp;Sign In</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-left" href="#modal-signup" data-toggle="modal">Sign Up</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Sign up modal -->
    <div id="modal-signup" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signup-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-ticket"></i>&nbsp;Sign Up</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="firstname">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter your name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- ############################# Common modals ############################# -->



    <!-- core script -->
    <script type="text/javascript">


        function renderPhotosGridView() {
            $.ajax({
                url: "./api/photos?page=" + photosPage + "&limit=24&facemarked=true&orderby=creationdatetime",
                dataType: "json",
                beforeSend: function () {
                },
                complete: function () {
                    updatePhotosStatus();
                },
                success: function (data) {
                    $("#container-gallery").hide();
                    $("#container-gallery").empty();           
                    $.each(data, function (key, val) {
                        var newElem = $('<li class="col-lg-2 col-md-2 col-sm-3 col-xs-4" style=""><a href="#">' + ((val.MarkedChild != null) ? ((val.PicturedChild != null) ? '<span style="position:absolute; border: 1px solid peru" class="label label-warning">In Use</span>' : '<span style="position:absolute; border: 1px solid green" class="label label-success">Linked</span>') : '') + '<img src="./api/photos/' + val.ID + '/image?width=128&height=128" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        $("#container-gallery").append(newElem);
                    });
                    $("#container-gallery").show("slide", { direction: pagerAnimateDirection }, 275).effect("bounce", { distance: 8, times: 4 }, { duration: 200 });
                },
            });
        };

        function updatePhotosStatus() {

            $.getJSON("./api/photos?page=1&limit=0&facemarked=true&orderby=creationdatetime", {})
                .done(function (data) {
                })

                .success(function (data) {
                    var unlinked = 0;
                    var linked = 0;
                    var inUse = 0;

                    $.each(data, function (key, val) {
                        if (val.MarkedChild != null)
                            linked++;
                        else
                            unlinked++;

                        if (val.PicturedChild != null)
                            inUse++;

                    });

                    $("#status-photos-unlinked-count").text(unlinked);
                    $("#status-photos-linked-count").text(linked);
                    $("#status-photos-inuse-count").text(inUse);
                });
        }

        function renderPhotosPager() {
            $.getJSON("./api/photos/info?page=" + photosPage + "&limit=24&facemarked=true&orderby=creationdatetime", {})
                .done(function (data) {
                })

                .success(function (data) {
                    var pager = $("#pager-gallery");
                    pager.empty();

                    if (data.TotalPages <= 1)
                        return;

                    if (photosPage == 1) {
                        pager.append('<li class="disabled"><a>Prev</a></li>');
                    } else {
                        pager.append('<li class="pager-prev"><a href="#">Prev</a></li>');
                    }

                    for (var i = 1; i <= data.TotalPages; i++) {

                        pager.append('<li data-page="' + i + '" class="pager-page ' + (i == photosPage ? 'active' : '') + '"><a href="#">' + i + '</a></li>');
                    }

                    if (photosPage == data.TotalPages) {
                        pager.append('<li class="disabled"><a>Next</a></li>');
                    } else {
                        pager.append('<li class="pager-next"><a href="#">Next</a></li>');
                    }
                });
        }

        // -- new funcs

        function uploadPhoto() {
            var form = $("#modal-upload-photo-form");

            var image = form.find("[name=file]")[0].files[0];
            var caption = form.find("[name=caption]").val();
            var takenDateTime = form.find("#modal-upload-photo-form-takendatetime").data("datetimepicker").getDate();
            var description = form.find("[name=description]").val();

            var longitude = form.find("#longitude").val();
            var latitude = form.find("#latitude").val();
            var country = form.find("#country").val();

            var photoData = new FormData();

            photoData.append("Caption", caption);
            photoData.append("Description", description);
            photoData.append("CaptureDateTime", new Date(takenDateTime).toISOString());
            photoData.append("Longitude", longitude);
            photoData.append("Latitude", latitude);
            photoData.append("Country", country);
            photoData.append("PhotoImage", image);


            var dialog;

            $.ajax({
                type: "POST",
                url: "./api/photos",
                cache: false,
                contentType: false,
                async: true,
                processData: false,
                data: photoData,

                beforeSend: function () {
                    dialog = bootbox.dialog({ title: "Uploading, please wait...", message: '<div class="progress progress-striped active"><div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>', closeButton: false });
                },

                complete: function () {
                    dialog.modal("hide");
                },

                success: function (id) {

                    if (id === "null") {
                        bootbox.alert({ message: "No faces were recognized in the photo provided, please upload another photo.", title: '<i class="fa fa-warning"></i>&nbsp;Photo Rejected' });
                        return;
                    }

                    var photoInfo;
                    $.getJSON("./api/photos/" + id, {})
                        .done(function (data) {
                        })

                        .success(function (data) {
                            photoInfo = data;

                            // display uploaded image
                            $("#modal-select-face-form-image").attr("src", "./api/photos/" + id + "/image");
                            $("#photo-input").val(id);

                            // get faces data
                            $.getJSON("./api/photos/" + id + "/faces", {})
                                .done(function (data) {
                                    $("#modal-select-face-form-faces").empty();
                                })

                                .success(function (data) {
                                    $.each(data, function (key, val) {
                                        var newElem = $(
                                            '<li class="col-md-2 col-lg-2 col-sm-2 col-xs-2">' +
                                            '<img class="btn-warning thumbnail" style="width: 48px; height: 48px; cursor: pointer;" ' +
                                            'data-id="' + val.ID + '" ' +
                                            'data-x="' + val.OffsetX + '" ' +
                                            'data-y="' + val.OffsetY + '" ' +
                                            'data-width="' + val.Width + '" ' +
                                            'data-height="' + val.Height + '" ' +
                                            'data-photo-height="' + photoInfo.Height + '" ' +
                                            'data-photo-width="' + photoInfo.Width + '" ' +
                                            'src="./api/faces/' + val.ID + '/image?width=48&height=48">' +
                                            '</li>');
                                        $("#modal-select-face-form-faces").append(newElem);
                                    });
                                    $("#modal-select-face").modal("show");
                                });
                        });


                },

                error: function (jqXHR, textStatus, errorThrown) {
                    switch (jqXHR.status) {
                        case 400:
                            bootbox.alert({ message: "Photo uploaded is either corrupted or in incorrect format, please upload a valid photo.", title: '<i class="fa fa-warning"></i>&nbsp;Invalid Photo' });
                            break;
                        default:
                            bootbox.alert({ message: "Unknown error occured, please try again.", title: '<i class="fa fa-warning"></i>&nbsp;Error Occured' });
                            break;
                    }
                }
            });
        }


        function markFace() {

            var form = $("#modal-select-face-form");

            var photo_id = form.find("#photo-input").val();
            var face_id = form.find("#face-input").val();

            var dialog;

            $.ajax({
                type: "POST",
                url: "./api/photos/" + photo_id + "/mark?face=" + face_id,
                contentType: "application/json",
                async: true,
                data: "",

                beforeSend: function () {
                    dialog = bootbox.dialog({ title: "Processing, please wait...", message: '<div class="progress progress-striped active"><div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>', closeButton: false });
                },

                complete: function () {
                    dialog.modal("hide");

                },

                success: function (data) {
                    bootbox.confirm({
                        message: "To help finding and tracking missing children more efficient, photos should be linked to an exact or look-alike child.<br><br>Would you like to link this photo now?", title: '<i class="fa fa-link"></i>&nbsp;Link Photo', callback: function (result) {
                            if (result == true) {

                                $("#modal-select-child").modal("show");
                                listSelectChildChildren();
                                renderSelectChildPager();

                            } else {
                                location.reload();

                            }
                        }
                    });



                    //alert("not bad");
                    //window.location = "./ChildProfile.aspx?id=" + data;
                },

                error: function (xmlHttpRequest, status, exception) {
                    message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });


            //alert(form.find("#face-input").val());

        }



        function listSelectChildChildren() {
            $.getJSON("./api/children?page=" + selectChildPage + "&limit=18&haspicture=&orderby=creationdatetime&status=Missing", {})
                .done(function (data) {

                })

                .success(function (data) {

                    $("#modal-select-child-form-children").hide();
                    $("#modal-select-child-form-children").empty();




                    $.each(data, function (key, val) {
                        var newElem = $('<li class="col-md-2 col-sm-2 col-lg-2 col-xs-2"><a href="#">' + ((val.Status === "Found") ? '<span style="position:absolute; border: 1px solid green" class="label label-success">Linked</span>' : '') + '<img style="width: 96px; height: 96px;" src="./api/photos/' + val.ProfilePicture + '/image?width=64&height=64" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        $("#modal-select-child-form-children").append(newElem);
                        //newElem.show("drop", { direction: selectChildPagerAnimateDirection }, 350);
                    });
                    $("#modal-select-child-form-children").show("slide", { direction: selectChildPagerAnimateDirection }, 275).effect("bounce", { distance: 8, times: 4 }, { duration: 200 });



                });
        }


        function renderSelectChildPager() {
            $.getJSON("./api/children/info?page=" + selectChildPage + "&limit=18&haspicture=&orderby=creationdatetime&status=Missing", {})
                .done(function (data) {
                })

                .success(function (data) {
                    var pager = $("#modal-select-child-form-pager");
                    pager.empty();

                    if (data.TotalPages <= 1)
                        return;

                    if (selectChildPage == 1) {
                        pager.append('<li class="disabled"><a>Prev</a></li>');
                    } else {
                        pager.append('<li class="pager-prev"><a href="#">Prev</a></li>');
                    }

                    for (var i = 1; i <= data.TotalPages; i++) {

                        pager.append('<li data-page="' + i + '" class="pager-page ' + (i == selectChildPage ? 'active' : '') + '"><a href="#">' + i + '</a></li>');
                    }

                    if (selectChildPage == data.TotalPages) {
                        pager.append('<li class="disabled"><a>Next</a></li>');
                    } else {
                        pager.append('<li class="pager-next"><a href="#">Next</a></li>');
                    }
                });
        }




        $("#modal-upload-photo-form").validate({
            ignore: [],
            rules: {
                file: {
                    required: true
                },
                caption: {
                    required: true
                },
                description: {
                    required: true,
                    maxlength: 1000
                },
                takendatetime: {
                    required: true
                },
                country: {
                    required: true
                }
            },
            messages: {
                country: "Please select a valid location."
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.is(".file-input")) {
                    error.insertAfter(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });


        // initialize on load
        $(document).ready(function (event) {


            renderPhotosGridView();
            renderPhotosPager();

            $("#modal-add-child-form").on("submit", function (e) {
                e.preventDefault();
                if ($("#modal-add-child-form").valid()) {
                    addChild();
                }
            });




            $("#pager-gallery").on("click", ".pager-page", function (e) {
                e.preventDefault();

                if ($(this).hasClass("active"))
                    return;

                var newPage = $(this).data("page");
                if (newPage > photosPage)
                    pagerAnimateDirection = "right";
                else
                    pagerAnimateDirection = "left";
                photosPage = newPage;
                renderPhotosGridView();
                renderPhotosPager();
            });

            $("#pager-gallery").on("click", ".pager-prev", function (e) {
                e.preventDefault();
                photosPage = photosPage - 1;
                pagerAnimateDirection = "left";
                renderPhotosGridView();
                renderPhotosPager();
            });

            $("#pager-gallery").on("click", ".pager-next", function (e) {
                e.preventDefault();
                photosPage = photosPage + 1;
                pagerAnimateDirection = "right";
                renderPhotosGridView();
                renderPhotosPager();
            });

            $("#modal-photo-info-btn-profile").on("click", function (e) {
                e.preventDefault();
                var mode = $(this).data("mode");
                var p_id = $(this).data("photo");

                if (mode === "link") {

                    $("#photo-input").val(p_id);
                    $("#modal-select-child").modal("show");
                    listSelectChildChildren();
                    renderSelectChildPager();

                } else {

                    bootbox.confirm({
                        message: "This photo will be unlinked from currently linked child.<br><br>Are you sure you want to unlink now?", title: '<i class="fa fa-unlink"></i>&nbsp;Unlink Photo', callback: function (result) {
                            if (result == true) {
                                $.ajax({
                                    type: "POST",
                                    url: "./api/photos/" + p_id + "/unlink",
                                    contentType: "application/json",
                                    async: true,
                                    data: "",

                                    beforeSend: function () {
                                        $("#modal-wait").modal("show");
                                    },

                                    complete: function () {
                                        $("#modal-wait").modal("hide");

                                    },

                                    success: function (data) {
                                        $("#modal-wait").modal("hide");
                                        bootbox.alert(
                                            {
                                                message: "Photo has been unlinked successfuly.",
                                                title: '<i class="fa fa-info-circle"></i>&nbsp;Photo Unlinked',
                                                callback: function () { location.reload(); }
                                            });
                                    },

                                    error: function (xmlHttpRequest, status, exception) {
                                        $("#modal-wait").modal("hide");

                                        bootbox.alert({ message: "Error occured while unlinking photo, please try again.", title: '<i class="fa fa-warning"></i>&nbsp;Unlink Error' });

                                    }
                                });
                            }
                        }
                    });


                }
            });

            $("#container-gallery").on("click", ".thumbnail", function (e) {
                e.preventDefault();
                var childId = $(this).data("id");

                $.getJSON("./api/photos/" + childId, {})
                    .success(function (data) {

                        $("#modal-photo-info-btn-profile").data("photo", data.ID);

                        // $("#modal-photo-info-btn-profile").prop('disabled', false);
                        $("#modal-photo-info-btn-profile").show();

                        if (data.MarkedChild != null) {
                            $("#modal-photo-info-btn-profile").removeClass("btn-primary");
                            $("#modal-photo-info-btn-profile").addClass("btn-danger");
                            $("#modal-photo-info-btn-profile").text("Unlink");
                            $("#modal-photo-info-btn-profile").data("mode", "unlink");
                            $("#modal-photo-info-btn-profile").data("child", data.MarkedChild.ID);
                            // console.log(data.PicturedChild);
                            if (data.PicturedChild != null) {
                                $("#modal-photo-info-btn-profile").hide();
                            }

                        } else {
                            $("#modal-photo-info-btn-profile").removeClass("btn-danger");
                            $("#modal-photo-info-btn-profile").addClass("btn-primary");
                            $("#modal-photo-info-btn-profile").text("Link");
                            $("#modal-photo-info-btn-profile").data("mode", "link");

                        }


                        //$("#modal-photo-info-btn-profile").attr("href", "./ChildProfile.aspx?id=" + data.ID);

                       // $("#modal-photo-info").find(".thumbnail").attr("src", "./api/photos/" + data.ID + "/image?width=128&height=128");

                        $("#modal-photo-info").find("#modal-photo-info-image").attr("src", "./api/photos/" + data.ID + "/image");

                        $("#modal-photo-info").find(".media-body").children().remove("table");

                        $("#modal-photo-info").find(".media-heading").html(data.Caption);

                        $("#modal-photo-info").find(".media-body")
                            .append(
                            "<table>" +
                            "<tbody>" +

                            "<tr>" + "<th>Uploaded By :&nbsp;</th>" + "<td>" + "<a href='./UserProfile.aspx?id=" + data.Owner.ID + "'>" + data.Owner.Name + "</a>" + "</td>" + "</tr>" +
                            "<tr>" + "<th>Upload Date/Time :&nbsp;</th>" + "<td>" + new Date(data.UploadDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                            "<tr>" + "<th>Captured Date/Time :&nbsp;</th>" + "<td>" + new Date(data.CaptureDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                            "<tr>" + "<th>Captured Location:&nbsp;</th>" + "<td>" + "<a target='_blank' href='https://maps.google.com/maps?z=6&q=" + data.Latitude + "," + data.Longitude + "'> " + data.Country.Name + "</td>" + "</tr>" +
                            "<tr>" + "<th>Linked Child :&nbsp;</th>" + "<td>" + (data.MarkedChild != null ? "<a href='./ChildProfile.aspx?id=" + data.MarkedChild.ID + "'>" + data.MarkedChild.Name + "</a>" : "(none)") + "</td>" + "</tr>" +
                            "<tr>" + "<th valign='top'>Description:</th>" + "<td>" + (data.Description.replace(/\n/g, '<br />')) + "</td>" + "</tr>" +
                            "</tbody>" +
                            "</table>");

                        $("#modal-photo-info").modal("show");
                    })
                    .fail(function () {
                        // will this fail?
                    });
            });


            $("#btn-clearfilter-modal").on("click", function (e) {
                //
                e.preventDefault();
                resetFilter();
            });

            $("#btn-clearfilter").on("click", function (e) {
                //
                e.preventDefault();
                resetFilter();
            });

            $("#modal-filter-form").on("submit", function (e) {
                //
                e.preventDefault();
                applyFilter();
            });

            // - new features

            $("#modal-upload-photo-form").on("submit", function (e) {
                e.preventDefault();
                if ($("#modal-upload-photo-form").valid()) {
                    //$("#modal-wait").show();
                    uploadPhoto();
                }
            });

            $("#btn-file-reset").on("click", function (e) {
                e.preventDefault();
            });

            $('#modal-upload-photo-form').find(":input.file-input").change(function (e) {
                // workaround for file input dismiss bug
                $('#modal-upload-photo-form').find(":input.file-input").attr('name', 'file');
            });

            $("#modal-select-face-form-faces").on("mouseover", ".thumbnail", function (e) {

                var face_x = $(this).data("x");
                var face_y = $(this).data("y");
                var face_width = $(this).data("width");
                var face_height = $(this).data("height");
                var photo_width = $(this).data("photo-width");
                var photo_height = $(this).data("photo-height");

                var selector_parent_x = $("#modal-select-face-form-image").offset().left;
                var selector_parent_y = $("#modal-select-face-form-image").offset().top;

                var photo_view_width = $("#modal-select-face-form-image").width();
                var photo_view_height = $("#modal-select-face-form-image").height();

                var photo_width_ratio = (photo_view_width / photo_width);
                var photo_height_ratio = (photo_view_height / photo_height);

                var selector_width = face_width * photo_width_ratio;;
                var selector_height = face_height * photo_height_ratio;;
                var selector_x = (face_x * photo_width_ratio) + selector_parent_x + 5 - 2;
                var selector_y = (face_y * photo_height_ratio) + selector_parent_y + 5 - 2;

                $("#face-highligher").show();
                $("#face-highligher").offset({ left: selector_x, top: selector_y })
                $("#face-highligher").width(selector_width);
                $("#face-highligher").height(selector_height);

            });

            $("#modal-select-face-form-faces").on("click", ".thumbnail", function (e) {

                // copied from another function
                var face_x = $(this).data("x");
                var face_y = $(this).data("y");
                var face_width = $(this).data("width");
                var face_height = $(this).data("height");
                var photo_width = $(this).data("photo-width");
                var photo_height = $(this).data("photo-height");

                var selector_parent_x = $("#modal-select-face-form-image").offset().left;
                var selector_parent_y = $("#modal-select-face-form-image").offset().top;

                var photo_view_width = $("#modal-select-face-form-image").width();
                var photo_view_height = $("#modal-select-face-form-image").height();

                var photo_width_ratio = (photo_view_width / photo_width);
                var photo_height_ratio = (photo_view_height / photo_height);

                var selector_width = face_width * photo_width_ratio;;
                var selector_height = face_height * photo_height_ratio;;
                var selector_x = (face_x * photo_width_ratio) + selector_parent_x + 5 - 2;
                var selector_y = (face_y * photo_height_ratio) + selector_parent_y + 5 - 2;

                // special
                var input = $("#face-input");
                var thumbs = $("#modal-select-face-form-faces").find(".thumbnail");

                //console.log(thumbs);

                $("#face-highligher").hide();
                thumbs.css("background-color", "");

                $("#face-selector").show();
                $("#face-selector").offset({ left: selector_x, top: selector_y })
                $("#face-selector").width(selector_width);
                $("#face-selector").height(selector_height);


                input.val($(this).data("id"));

                $(this).css("background-color", "#04B324");

                $("#btn-confirm-face").prop('disabled', false);

            });

            $("#modal-select-face-form-faces").on("mouseleave", ".thumbnail", function (e) {
                $("#face-highligher").hide();
            });

            $('#modal-select-face').on('shown.bs.modal', function () {
                //$("#txtname").focus();
                if ($("#face-input").val().length <= 0) {
                    $("#btn-confirm-face").prop('disabled', true);
                    $("#face-selector").hide();
                }

                $("#face-highligher").hide();

            });

            $("#modal-select-face-form").on("submit", function (e) {
                e.preventDefault();
                markFace();

            });

            $("#btn-skip-face").on("click", function (e) {
                e.preventDefault();
                bootbox.confirm({
                    message: "Photos with no face selected will not get published, however you may do so any time in your profile.<br><br>Are you sure you want to skip this step?", title: '<i class="fa fa-share-square-o"></i>&nbsp;Skip', callback: function (result) {
                        if (result == true) {
                            location.reload();
                        }
                    }
                });
            });




            $("#modal-select-child-form-pager").on("click", ".pager-page", function (e) {
                e.preventDefault();
                var newPage = $(this).data("page");
                if (newPage > selectChildPage)
                    selectChildPagerAnimateDirection = "right";
                else
                    selectChildPagerAnimateDirection = "left";
                selectChildPage = newPage;
                listSelectChildChildren();
                renderSelectChildPager();
            });

            $("#modal-select-child-form-pager").on("click", ".pager-prev", function (e) {
                e.preventDefault();
                selectChildPage = selectChildPage - 1;
                selectChildPagerAnimateDirection = "left";
                listSelectChildChildren();
                renderSelectChildPager();
            });

            $("#modal-select-child-form-pager").on("click", ".pager-next", function (e) {
                e.preventDefault();
                selectChildPage = selectChildPage + 1;
                selectChildPagerAnimateDirection = "right";
                listSelectChildChildren();
                renderSelectChildPager();
            });


            $("#modal-link-child-btn-link").on("click", function (e) {

                var c_id = $(this).data("id");
                var p_id = $("#photo-input").val();

                var dialog;

                $.ajax({
                    type: "POST",
                    url: "./api/photos/" + p_id + "/link?child=" + c_id,
                    contentType: "application/json",
                    async: true,
                    data: "",

                    beforeSend: function () {
                        dialog = bootbox.dialog({ title: "Processing, please wait...", message: '<div class="progress progress-striped active"><div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>', closeButton: false });
                    },

                    complete: function () {
                        dialog.modal("hide");
                    },

                    success: function (data) {
                        $("#modal-link-child").modal("hide");

                        bootbox.alert(
                            {
                                message: "Photo has been linked successfuly.",
                                title: '<i class="fa fa-info-circle"></i>&nbsp;Photo Linked',
                                callback: function () { location.reload(); }
                            });
                    },

                    error: function (xmlHttpRequest, status, exception) {
                        bootbox.alert({ message: "Error occured while linking photo, please try again.", title: '<i class="fa fa-warning"></i>&nbsp;Link Error' });
                    }
                });
            });


            $("#modal-select-child-form-children").on("click", ".thumbnail", function (e) {
                e.preventDefault();

                var thumbId = $(this).data("id");

                $.getJSON("./api/children/" + thumbId, {})
                    .success(function (data) {
                        $("#modal-link-child").find(".thumbnail").attr("src", "./api/children/" + data.ID + "/picture?width=128&height=128");
                        //$("#modal-link-child-btn-link").attr("href", "./ChildProfile.aspx?id=" + data.ID);

                        $("#modal-link-child-btn-link").data("id", data.ID); //.attr("href", "./ChildProfile.aspx?id=" + data.ID);

                        $("#modal-link-child").find(".media-body").children().remove("table");

                        $("#modal-link-child").find(".media-heading").html(data.Name);

                        $("#modal-link-child").find(".media-body")
                            .append(
                            "<table>" +
                            "<tbody>" +
                            "<tr>" + "<th>Gender:&nbsp;</th>" + "<td>" + data.Gender + "</td>" + "</tr>" +
                            "<tr>" + "<th>Birthday:&nbsp;</th>" + "<td>" + new Date(data.Birthday).toString("d MMMM yyyy (") + "<abbr title='" + data.Age + " years old as today.'>" + data.Age + " years old</abbr>)</td>" + "</tr>" +
                            "<tr>" + "<th>Added On:&nbsp;</th>" + "<td>" + new Date(data.CreationDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                            "<tr>" + "<th>Missing On:&nbsp;</th>" + "<td>" + new Date(data.MissingDateTime).toString("d MMMM yyyy - h:mm tt (") + "<abbr title='" + data.MissingAge + " years old as missing time.'>" + data.MissingAge + " years old</abbr>)</td>" + "</tr>" +
                            "<tr>" + "<th>Missing Location:&nbsp;</th>" + "<td>" + "<a target='_blank' href='https://maps.google.com/maps?z=6&q=" + data.MissingCountry.Latitude + "," + data.MissingCountry.Longitude + "'> " + data.MissingCountry.Name + "</td>" + "</tr>" +
                            "<tr>" + "<th>Owner:&nbsp;</th>" + "<td>" + "<a href='./UserProfile.aspx?id=" + data.Owner.ID + "'>" + data.Owner.Name + "</a>" + "</td>" + "</tr>" +
                            "<tr>" + "<th>Status:&nbsp;</th>" + "<td>" + data.Status + "</td>" + "</tr>" +
                            "</tbody>" +
                            "</table>");

                        $("#modal-link-child").modal("show");
                    })
                    .fail(function () {
                        // will this fail?
                    });
            });



            $("#btn-delete-face-photo").on("click", function (e) {
                e.preventDefault();

                bootbox.confirm({
                    message: "This photo will be discarded.<br><br>Are you sure you want to discard this photo?",
                    title: '<i class="fa fa-trash-o"></i>&nbsp;Discard Photo',
                    callback: function (result) {
                        if (result == true) {
                            var photo_id = $("#photo-input").val();
                            //alert(photo_id);

                            $.ajax({
                                type: "DELETE",
                                url: "./api/photos/" + photo_id,
                                contentType: "application/json",
                                async: true,
                                data: "",

                                beforeSend: function () {
                                    $("#modal-wait").modal("show");
                                },

                                complete: function () {
                                    $("#modal-wait").modal("hide");

                                },

                                success: function (data) {
                                    $("#modal-wait").modal("hide");
                                    location.reload();
                                },

                                error: function (xmlHttpRequest, status, exception) {
                                    $("#modal-wait").modal("hide");
                                    message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                                }
                            });
                        } else {
                            return;
                        }
                    }
                });
            });

            // -- end --
        });

    </script>


    <!-- Google Map for photo upload -->
    <script type="text/javascript">

        function geolocationSuccess(pos) {
            var crd = pos.coords;

            photoUploadLongitude = crd.longitude;
            photoUploadLatitude = crd.latitude;
        }

        function geolocationError(err) {
            photoUploadLongitude = 0.0;
            photoUploadLatitude = 0.0;
        }

        $(document).ready(function () {
            var circle = null;
            var radius = $("#radius").val();

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
            }

            var StartPosition = new google.maps.LatLng(photoUploadLatitude, photoUploadLongitude);

            function DrawCircle(photoUploadMap, center, radius) {

                if (circle != null) {
                    circle.setMap(null);
                }

                if (radius > 0) {
                    radius *= 1609.344;
                    circle = new google.maps.Circle({
                        center: center,
                        radius: radius,
                        strokeColor: "#0000FF",
                        strokeOpacity: 0.35,
                        strokeWeight: 2,
                        fillColor: "#0000FF",
                        fillOpacity: 0.20,
                        map: photoUploadMap
                    });
                }
            }

            function SetPosition(Location, Viewport) {
                photoUploadMapMarker.setPosition(Location);
                if (Viewport) {
                    photoUploadMap.fitBounds(Viewport);
                    photoUploadMap.setZoom(map.getZoom() + 2);
                }
                else {
                    photoUploadMap.panTo(Location);
                }
                radius = $("#radius").val();
                DrawCircle(photoUploadMap, Location, radius);
                $("#latitude").val(Location.lat().toFixed(8));
                $("#longitude").val(Location.lng().toFixed(8));

                // Reverse geocode for country
                geocoder.geocode({ 'latLng': photoUploadMapMarker.position }, function (results, status) {
                    $("#country").val("");
                    if (status == google.maps.GeocoderStatus.OK) {
                        var newzip = results[0].address_components['postal_code'];
                        $.each(results, function () {
                            if (this.types[0] === "country") {
                                $("#country").val(this.address_components[0].short_name);
                                return;
                            }

                        });
                    }
                });
            }

            var MapOptions = {
                zoom: 12,
                center: StartPosition,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                disableDoubleClickZoom: true,
                streetViewControl: false
            };

            var MapView = $("#map");
            photoUploadMap = new google.maps.Map(MapView.get(0), MapOptions);

            photoUploadMapMarker = new google.maps.Marker({
                position: StartPosition,
                map: photoUploadMap,
                title: "Drag Me",
                draggable: true
            });

            google.maps.event.addListener(photoUploadMapMarker, "dragend", function (event) {
                SetPosition(photoUploadMapMarker.position);
            });

            $("#radius").keyup(function () {
                google.maps.event.trigger(photoUploadMapMarker, "dragend");
            });

            DrawCircle(photoUploadMap, StartPosition, radius);
            SetPosition(photoUploadMapMarker.position);

            $("#modal-upload-photo").on("shown.bs.modal", function () {
                // Workaround for map resize problem
                google.maps.event.trigger(photoUploadMap, 'resize');
                SetPosition(new google.maps.LatLng(photoUploadLatitude, photoUploadLongitude));
            });
        });
    </script>
</body>
</html>
