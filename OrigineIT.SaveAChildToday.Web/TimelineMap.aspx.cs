﻿using OrigineIT.SaveAChildToday.Web.ApiControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrigineIT.SaveAChildToday.Web
{
    public partial class TimelineMap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var countries = new CountriesController().GetCountries();
            Repeater1.DataSource = countries;
            //Repeater2.DataSource = countries;

            Repeater1.DataBind();
            //Repeater2.DataBind();
        }
    }
}