﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.About" %>

<!doctype html>
<html>
<head>
    <title>Save A Child Today - Find Missing Children Worldwide</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./StyleSheets/bootstrap.min.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-custom.css">
    <link rel="stylesheet" href="./StyleSheets/font-awesome.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="./StyleSheets/internal-common.css">

    <script type="text/javascript" src="./Scripts/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootbox.min.js"></script>
    <script type="text/javascript" src="./Scripts/date.js"></script>
    <script type="text/javascript" src="./Scripts/internal-common.js"></script>
</head>

<body>

    <!-- Navigation bar -->
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Save A Child Today</a>
            </div>
            <ul class="nav navbar-nav navbar-right user-item hide">
                <li>
                    <a href="./MyProfile.aspx">My Profile</a>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./Index.aspx">Home</a>
                    </li>
                    <li>
                        <a href="./Photos.aspx">Photos</a>
                    </li>
                    <li>
                        <a href="./People.aspx">People</a>
                    </li>
                    <li class="active">
                        <a href="#">About</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <a id="btn-navbar-signin" class="btn btn-primary guest-item hide" href="#modal-signin" data-toggle="modal"><i class="fa fa-sign-in"></i>&nbsp;Sign in</a>
                    <a id="btn-navbar-signout" class="btn btn-danger user-item hide" href="#"><i class="fa fa-sign-out"></i>&nbsp;Sign out</a>
                </form>
            </div>
        </div>
    </div>

    <div class="container">

        <div id="carousel-example-captions" class="carousel slide thumbnail" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-captions" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img alt="" src="./Images/REU-CHINA_3.jpg" />
                    <div class="carousel-caption">
                        <h3>宝贝，你在哪里？</h3>
                        <p>我们大家都好想念你，希望你能回家...</p>
                    </div>
                </div>
                <div class="item">
                    <img src="./Images/3501460JPG-6311822.jpg" />
                    <div class="carousel-caption">
                        <h3>Mum, where are you?</h3>
                        <p>I want to go back home...</p>
                    </div>
                </div>

            </div>
            <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>


        <h2>About SaveAChildToday.net</h2>
        <p><b>SaveAChildToday.net</b> is an online platform to find missing children worldwide. It empower the public to take pictures of the suspected victims, and upload to the same platform.</p>

        <p>A face-recognition engine will run tirelessly to match the pictures uploaded by the public to missing children profiles, and speed up the investigation process.
        <br>
        The big data we collected will be analyzed and presented onto a timeline map.
The result may help to track child trafficking activities around the region.</p>

        Our vision is simple,
        <ul>
            <li>Bring the lost kids back home, and unite the family, and</li>
            <li>Combat child trafficking</li>
        </ul>

        <div>
            <img src="./Images/screenx.png" style="width: 100%" class="thumbnail">
        </div>
        <h4>Privacy Consideration</h4>
        <ul>
            <li>Pictures uploaded by the public will not be shared on public. </li>
            <li>Only the parent who have reported missing children via this platform will be able to view all the pictures uploaded by the public.</li>
        </ul>


        <h4>Why are we doing this ?</h4>
        <ul>
            <li>Our new direction is to use technology to improve life, and make the world a happier place to live</li>
            <li>Human trafficking is one of the biggest issue in this century, we want to play our part at best, as the member of the society.</li>
        </ul>

        <br>
    </div>

            <!-- ----------------------------- Common modals ----------------------------- -->

    <!-- Sign in modal -->
    <div id="modal-signin" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signin-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i>&nbsp;Sign In</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-left" href="#modal-signup" data-toggle="modal">Sign Up</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Sign up modal -->
    <div id="modal-signup" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signup-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-ticket"></i>&nbsp;Sign Up</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="firstname">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter your name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- ############################# Common modals ############################# -->

</body>
</html>
