﻿using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Configurations;
using OrigineIT.SaveAChildToday.DataAccess.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess.Setups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrigineIT.SaveAChildToday.Web
{
    public class DataAccessConfig
    {
        public static void Execute(SchemaConfiguration config)
        {
            NHPersistentStore.Configure(DatabaseConfiguration.AppHarborMySqlFree, config);

            if (config == SchemaConfiguration.Rebuild)
            {
                NHPersistentStore.ExecuteSetup(new CountriesSetup(NHPersistentStore.SessionFactory));
            }
        }

        public static void End()
        {
            NHPersistentStore.Close();
        }
    }
}