﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChildProfile.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.ChildProfile" %>

<!doctype html>

<html>

<head>
    <title>Save A Child Today - Find Missing Children Worldwide</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./StyleSheets/bootstrap.min.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-custom.css">
    <link rel="stylesheet" href="./StyleSheets/font-awesome.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="./StyleSheets/internal-common.css">

    <script type="text/javascript" src="./Scripts/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="./Scripts/date.js"></script>


    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript" src="./Scripts/Mxn/mxn.js?(googlev3)"></script>
    <script type="text/javascript" src="./Scripts/TimeMap/timeline-1.2.js"></script>
    <script type="text/javascript" src="./Scripts/TimeMap/timemap.js"></script>
    <script type="text/javascript" src="./Scripts/TimeMap/json2.pack.js"></script>
    <script type="text/javascript" src="./Scripts/TimeMap/loaders/json.js"></script>
        <script type="text/javascript" src="./Scripts/internal-common.js"></script>




        <script type="text/javascript">
            // Shared variables

            var photosPage = 1;
            var pagerAnimateDirection = "up";
            var geocoder = new google.maps.Geocoder();

            var selectChildPage = 1;
            var selectChildPagerAnimateDirection = "up";

            // Map
            var photoUploadLongitude = 0.0;
            var photoUploadLatitude = 0.0;
            var photoUploadMap, photoUploadMapMarker;
    </script>


    <style type="text/css">
        div#help {
            font-size: 12px;
            width: 45em;
            padding: 1em;
        }

        div#timemap {
            padding: 0px;
            border: #CCC 1px solid;
        }

        div#timelinecontainer {
            width: 100%;
            height: 135px;
        }

        div#timeline {
            width: 100%;
            height: 100%;
            font-size: 12px;
            background: #CCCCCC;
        }

        div#mapcontainer {
            width: 100%;
            height: 400px;
            position: relative;
        }

        div#map {
            width: 100%;
            height: 100%;
            background: #EEEEEE;
        }

        div.infotitle {
            font-size: 14px;
            font-weight: bold;
        }

        div.infodescription {
            font-size: 14px;
            font-style: italic;
        }

        div.custominfostyle {
            font-family: Georgia, Garamond, serif;
            font-size: 1.5em;
            font-style: italic;
            width: 20em;
        }




        ul.row {
            padding: 0 0 0 0;
            margin: 0 0 0 0;
        }

            ul.row li {
                list-style: none;
                margin-bottom: 0px;
            }

                ul.row li img {
                    /*cursor: pointer;*/
                }

                    ul.row li img.thumbnail {
                        width: 128px;
                        height: 128px;
                    }
    </style>
</head>

<body>

        <!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
    <!-- select photo modal -->

    <div id="modal-select-photo" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 800px">
            <form role="form" id="modal-select-photo-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-picture-o"></i>&nbsp;Select Photo</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <ul id="modal-select-photo-form-photos" class="form-group row"></ul>
                            <input style="display: none" type="text" id="child-input" name="child" value="" />
                        </div>

                        <div class="form-group">

                            <ul id="modal-select-photo-form-pager" class="pagination pull-left">
                                <!-- pages -->
                            </ul>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="btn-cancel-select-photo" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->




        <!-- list photo modal -->

    <div id="modal-list-photo" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 800px">
            <form role="form" id="modal-list-photo-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-picture-o"></i>&nbsp;Linked Photos</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <ul id="modal-list-photo-form-photos" class="form-group row"></ul>
                            <input style="display: none" type="text" id="Text1" name="child" value="" />
                        </div>

                        <div class="form-group">

                            <ul id="modal-list-photo-form-pager" class="pagination pull-left">
                                <!-- pages -->
                            </ul>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="Button3" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    
        <!-- link photo modal -->

    <div id="modal-link-photo" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 800px">
            <form role="form" id="Form5">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-picture-o"></i>&nbsp;Linked Photos</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <ul id="Ul3" class="form-group row"></ul>
                            <input style="display: none" type="text" id="Text2" name="child" value="" />
                        </div>

                        <div class="form-group">

                            <ul id="Ul4" class="pagination pull-left">
                                <!-- pages -->
                            </ul>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="Button4" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->




        <!-- loading modal -->

    <div id="modal-wait" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <form role="form" id="Form2">
                <div class="modal-content">

                    <div class="modal-body">
                        <h4>Processing, please wait...</h4>
                        <div class="progress progress-striped active">
                            <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>

                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


            <!-- timeline modal -->

    <div id="Div1" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog"  style="width: 900px;">
            <form role="form" id="Form1">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-calendar"></i>&nbsp;Timeline</h3>
                    </div>
                    <div class="modal-body">
                            <!-- tm -->

                        <!--div class="container"-->
                        <div class="row">
                            <div class="col-md-12">
                                <div id="timemap">
                                    <div id="timelinecontainer">
                                        <div id="timeline"></div>
                                    </div>
                                    <div id="mapcontainer">
                                        <div id="map"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <!--/div-->
                        <!-- etm -->
                    </div>
                                        <div class="modal-footer">
                        <button id="Button1" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->

                <!-- comment modal -->


    <div id="Div2" class="modal fade" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog"  style="width: 500px; height: 600px">
            <div role="form" id="Form3">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-comment"></i>&nbsp;Comments</h3>
                    </div>
                    <div class="modal-body" style="padding-bottom: 0px !important">
                        <div id="comments-container" style="overflow-y: scroll; height: 500px">
                        </div>

                       

                        <div class="form-group user-item">

                            <form id="comment-form">
                                <input type="text" class="form-control" name="message" id="comment-input" placeholder="Enter comment here">
                                <!--button class="btn btn-primary pull-right" type="submit">Submit</!--button-->
                            </form>
                        </div>

                    </div>
                                        <div class="modal-footer">
                        <button id="Button2" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    <!-- change stat modal -->

            <div id="modal-change-status" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-change-status-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-check"></i>&nbsp;Report Found</h3>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <label class="control-label" for="mscountry">Found Location:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <select class="form-control" name="foundcountry">
                                    <option value="" selected>Select country</option>
                                    <asp:Repeater ID="Repeater2" runat="server">
                                        <ItemTemplate>
                                            <option value="<%# Eval("ID") %>" data-longitude="<%# Eval("Longitude") %>" data-latitude="<%# Eval("Latitude") %>"><%# Eval("Name") %></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="founddatetime">Found Date/Time:&nbsp;</label>
                            <div id="modal-change-status-form-founddatetime" class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-format="yyyy-mm-dd">
                                <input name="founddatetime" class="form-control" size="16" type="text" placeholder="Select a date and time" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="founddescription">Found Description:&nbsp;</label>

                            <textarea name="founddescription" class="form-control" placeholder="Describe how the child was found..." rows="3"></textarea>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    <!-- edit modal -->

        <div id="modal-edit-child" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-edit-child-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-edit"></i>&nbsp;Edit Profile</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="name">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter child&apos;s name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="gender">Gender:&nbsp;</label><br />
                            <div class="btn-group" data-toggle="buttons">

                                <label class="btn btn-default active">
                                    <input type="radio" name="gender" value="Male" checked="">
                                    <i class="fa fa-male"></i>&nbsp;Male
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="gender" value="Female">
                                    <i class="fa fa-female"></i>&nbsp;Female
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="birthday">Birthday:&nbsp;</label>
                            <!-- dd MM yyyy -->
                            <div id="modal-add-child-form-birthday" class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-format="yyyy-mm-dd">
                                <input name="birthday" class="form-control" size="16" type="text" placeholder="Select a date" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="description">Description:&nbsp;</label>

                            <textarea name="description" class="form-control" placeholder="Tell us more about this child..." rows="3"></textarea>

                        </div>


                        <div class="form-group">
                            <label class="control-label" for="mscountry">Missing Location:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <select class="form-control" name="mscountry">
                                    <option value="" selected>Select country</option>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <option value="<%# Eval("ID") %>" data-longitude="<%# Eval("Longitude") %>" data-latitude="<%# Eval("Latitude") %>"><%# Eval("Name") %></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="msdatetime">Missing Date/Time:&nbsp;</label>
                            <div id="modal-add-child-form-msdatetime" class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-format="yyyy-mm-dd">
                                <input name="msdatetime" class="form-control" size="16" type="text" placeholder="Select a date and time" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="msdescription">Missing Description:&nbsp;</label>

                            <textarea name="msdescription" class="form-control" placeholder="Describe how the child was missing..." rows="3"></textarea>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Save A Child Today</a>
            </div>
            <ul class="nav navbar-nav navbar-right user-item hide">
                <li>
                    <a href="./MyProfile.aspx">My Profile</a>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./Index.aspx">Home</a>
                    </li>
                    <li>
                        <a href="./Photos.aspx">Photos</a>
                    </li>
                    <li>
                        <a href="./People.aspx">People</a>
                    </li>
                    <li>
                        <a href="./About.aspx">About</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <a id="btn-navbar-signin" class="btn btn-primary guest-item hide" href="#modal-signin" data-toggle="modal"><i class="fa fa-sign-in"></i>&nbsp;Sign in</a>
                    <a id="btn-navbar-signout" class="btn btn-danger user-item hide" href="#"><i class="fa fa-sign-out"></i>&nbsp;Sign out</a>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left btn-group">
                    <a id="btn-timeline" href="#Div1" data-toggle="modal" class="btn btn-default"><i class="fa fa-calendar"></i>&nbsp;Timeline</a>
                    <a href="#Div2" data-toggle="modal" class="btn btn-default"><i class="fa fa-comment"></i>&nbsp;Comments</a>
                    <a id="btn-list-linked-photos" href="#modal-list-photo" data-toggle="modal" class="btn btn-default"><i class="fa fa-picture-o"></i>&nbsp;Photos</a>
                </div>
                <div class="btn-toolbar pull-right">
                    <div class="btn-group hide xuser-item">
                        <a href="#modal-link-photo" data-toggle="modal" class="btn btn-default"><i class="fa fa-chain"></i>&nbsp;Link Photo</a>
                        <!--a-- href="#modal-upload-photo" data-toggle="modal" class="btn btn-default"><i class="fa fa-upload"></i> Upload Photo</!--a-->
                    </div>
                    <div class="btn-group hide owner-item">
                        <a class="btn btn-default" href="#modal-edit-child" data-toggle="modal"><i class="fa fa-edit"></i>&nbsp;Edit Profile</a><a class="btn dropdown-toggle btn-default" data-toggle="dropdown" href="#">    <span class="caret"></span>  </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#modal-change-status" data-toggle="modal"><i class="fa fa-check"></i>&nbsp;Report Found</a>
                </li>
              </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>



    

    
    <div class="container">
        <h3><span id="prof-child-name"><!-- data --></span></h3>
        <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                <div class="thumbnail" style="width: 150px; min-width: 150px; height: auto;">
                    <img id="prof-child-picture" src="./api/children/null/picture?width=140&height=140" width="140" height="140" style="width: 140px; height: 140px">
                    <div class="caption owner-item hide">
                        <a id="btn-change-picture" class="btn btn-block btn-default btn-sm"><i class="fa fa-edit"></i>&nbsp;Change</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-5">
                <h4>Personal Details</h4>
                <table>
                    <tbody>
                        <tr>
                            <th>Gender:&nbsp;</th>
                            <td><span id="prof-child-gender"><!-- data --></span></td>
                        </tr>
                        <tr>
                            <th>Birthday:&nbsp;</th>
                            <td><span id="prof-child-birthday"><!-- data --></span></td>
                        </tr>

                        <tr>
                            <th>
                                Owner:&nbsp;</th>
                            <td>
                                <span id="prof-child-owner"><!-- data --></span>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                Status:&nbsp;</th>
                            <td>
                                <span id="prof-child-status"><!-- data --></span>
                            </td>
                        </tr>
                                                <tr>
                            <th>
                                Added On:&nbsp;</th>
                            <td>
                                <span id="prof-child-added-on"><!-- data --></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <th valign="top">
                                Description:&nbsp;</th>
                            <td>
                                <span id="prof-child-description"><!-- data --></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-5">
                <h4>Missing Information</h4>
                <table>
                    <tbody>
                        <tr>
                            <th>Date/Time:&nbsp;</th>
                            <td><span id="prof-child-missing-on"><!-- data --></span></td>
                        </tr>
                        <tr>
                            <th>Location:&nbsp;</th>
                            <td><span id="prof-child-missing-location"><!-- data --></span></td>
                        </tr>

                        <tr>
                            <th valign="top">
                                Description:&nbsp;</th>
                            <td>
                                <span id="prof-child-missing-description"><!-- data --></span>
                            </td>
                        </tr>
                    </tbody>
                </table>


                <div id="found-information" class="hide">
                    <br /><br />
                 <h4>Found Information</h4>
                <table>
                    <tbody>
                        <tr>
                            <th>Date/Time:&nbsp;</th>
                            <td><span id="prof-child-found-on"><!-- data --></span></td>
                        </tr>
                        <tr>
                            <th>Location:&nbsp;</th>
                            <td><span id="prof-child-found-location"><!-- data --></span></td>
                        </tr>

                        <tr>
                            <th valign="top">
                                Description:&nbsp;</th>
                            <td>
                                <span id="prof-child-found-description"><!-- data --></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>

            </div>
        </div>
    </div>
    <!--hr-->



            <!-- ----------------------------- Common modals ----------------------------- -->

    <!-- Sign in modal -->
    <div id="modal-signin" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signin-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i>&nbsp;Sign In</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-left" href="#modal-signup" data-toggle="modal">Sign Up</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Sign up modal -->
    <div id="modal-signup" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signup-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-ticket"></i>&nbsp;Sign Up</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="firstname">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter your name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- ############################# Common modals ############################# -->












        <div id="modal-upload-photo" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-upload-photo-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-upload"></i>&nbsp;Upload Photo</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <label class="control-label" for="file">Image File:&nbsp;</label>
                                <div class="input-group">
                                    <div class="form-control uneditable-input" data-trigger="fileinput"><i class="glyphicon glyphicon-picture fileinput-exists"></i>&nbsp;<span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input class="file-input" type="file" name="file" accept="image/*"></span>
                                    <a href="#" id="btn-file-reset" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="caption">Caption:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                <input class="form-control" placeholder="Enter photo caption" name="caption" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="description">Description:&nbsp;</label>
                            <div class="form-group">
                                <textarea name="description" class="form-control" placeholder="Tell us more about this photo..." rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="takendatetime">Taken Date/Time:&nbsp;</label>
                            <div id="modal-upload-photo-form-takendatetime" class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-format="yyyy-mm-dd">
                                <input name="takendatetime" class="form-control" size="16" type="text" placeholder="Select a date and time" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <input type="hidden" id="latitude" value="0" />
                        <input type="hidden" id="longitude" value="0" />
                        <input type="hidden" id="radius" value="0.1" />


                        <div class="form-group">
                            <label class="control-label">Taken Location:&nbsp;</label>
                            <div id="Div3" class="map-canvas" style="width: 100%; height: 280px; background-color: #000000;"></div>
                            <input style="display: none" type="text" id="country" name="country" value="" />
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->

    <!-- select face modal -->

    <div id="modal-select-face" class="modal fade" data-backdrop="static">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-select-face-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-user"></i>&nbsp;Select Face</h3>
                    </div>
                    <div class="modal-body">

                        <!-- face select ui -->
                        <div class="form-group well well-sm" style="width: 100%; max-height: 325px;">
                            <div class="" id="face-selector" style="position: absolute; background: rgba(76, 255, 0, 0.20); border: 2px solid #4cff00; width: 0px; height: 0px"></div>
                            <div class="" id="face-highligher" style="position: absolute; background: rgba(255, 216, 0, 0.20); border: 2px solid #ffd800; width: 0px; height: 0px"></div>

                            <img class="thumbnail center-block" id="modal-select-face-form-image" style="max-width: 100%; max-height: 305px;" src="" />
                            <input style="display: none" type="text" id="photo-input" name="photo" value="" />
                        </div>

                        <div class="form-group">
                            <ul id="modal-select-face-form-faces" class="form-group row"></ul>
                            <input style="display: none" type="text" id="face-input" name="face" value="" />
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="btn-delete-face-photo" type="button" class="btn btn-danger pull-left">Discard</button>
                        <button id="btn-skip-face" type="button" class="btn btn-default">Skip</button>
                        <button id="btn-confirm-face" type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->








    <!--- zcxczcxcxcxz!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->

        <div id="modal-photo-info" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-photo-info-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-picture-o"></i>&nbsp;About This Photo</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group well well-sm" style="width: 100%; max-height: 325px;">

                            <div class="" id="modal-photo-info-face-highlight-box" style="position: absolute; background: rgba(76, 255, 0, 0.20); border: 2px solid #4cff00; width: 0px; height: 0px"></div>

                            <img class="thumbnail center-block" id="modal-photo-info-image" style="max-width: 100%; max-height: 305px;" src="" />
                        </div>

                        <div class="media">
                            <div class="media-body">
                                <h4 class="media-heading"></h4>
                                <!-- Photo info -->
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a id="modal-photo-info-btn-profile" class="btn btn-primary x-item hide" href="#">Link</a>
                    </div>
                </div>
            </form>
        </div>
    </div>







    <script type="text/javascript">


        $("#comment-form").validate({
            rules: {
                message: {
                    required: true
                }
            },
            messages: {
                message: ""
            },
            highlight: function (element) {
                //$(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
               // $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

    </script>




    <script type="text/javascript">
        function queryStringValues() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
    </script>

    <script type="text/javascript">


        var child;

        function getChildInfo() {

            var id = queryStringValues()["id"];


            /*
            $.getJSON("./api/children/" + id, {})

                .done(function () {
                })

                .success(function (data) {

                    //console.log(data);
                    //authInfo = getAuthInfo();
                    child = data;
                    renderProfile();
                    renderTimeMap();
                    //renderTimeMap();

                    listComments();
                })

                .fail(function () {
                    //child = null;
                    window.location.replace("./Index.aspx");
                });*/


            $.ajax({

                type: "GET",
                url: "./api/children/" + id,
                contentType: "application/json",
                async: false,

                success: function (data) {
                    //console.log(data);
                    //authInfo = getAuthInfo();
                    child = data;
                    renderProfile();
                    renderTimeMap();
                    //renderTimeMap();
                },

                error: function () {
                    window.location.replace("./Index.aspx");
                }


            });
        }

        $(document).ready(function (ev) {
            // alert(id);
           

            getChildInfo();


        });


        // 

    </script>






    <script type="text/javascript">

        var timeMap;

        var selectPhotoPage = 1;
        var selectPhotoPagerAnimateDirection = "up";

        $(document).ready(function (e) {



            $("#Div1").on("shown.bs.modal", function (e) {
                e.preventDefault();
                renderTimeMap();
                //google.maps.event.trigger(timeMap.getNativeMap(), "resize");
                //$("#Div1").trigger("resize");
                //timeMap.timeline._initialize();
            });

            $("#Div2").on("shown.bs.modal", function (e) {
                e.preventDefault();
                listComments();
                //google.maps.event.trigger(timeMap.getNativeMap(), "resize");
                //$("#Div1").trigger("resize");
                //timeMap.timeline._initialize();
            });



            // events


            $("#modal-select-photo-form-photos").on("click", ".thumbnail", function (e) {
                e.preventDefault();

                var thumbId = $(this).data("id");

                var pObj = new Object();

                pObj.ProfilePicture = thumbId;

                $.ajax({
                    type: "POST",
                    url: "./api/children/" + child.ID,
                    contentType: "application/json",
                    async: true,
                    data: JSON.stringify(pObj),

                    beforeSend: function () {
                       // $("#modal-wait").modal("show");
                    },

                    complete: function () {
                       // $("#modal-wait").modal("hide");
                        //alert("wow la?");
                    },

                    success: function (data) {
                        //$("#modal-wait").modal("hide");
                        //location.reload();
                        renderProfile();
                        $("#modal-select-photo").modal("hide");
                        //alert("wow la");
                        //window.location = "./ChildProfile.aspx?id=" + data;
                    },

                    error: function (xmlHttpRequest, status, exception) {
                        //$("#modal-wait").modal("hide");
                        // message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                    }
                });

            });


            // ---------

            $("#comment-form").on("submit", function (e) {

                e.preventDefault();
                //alert($("#comment-input").val());
                if ($(this).valid()) {
                    postComment();
                }

            });

            $("#btn-change-picture").on("click", function (e) {
                e.preventDefault();

                // load json
                $("#modal-select-photo").modal("show");
                listSelectPhotoPhotos();
                renderSelectPhotoPager();

            });

            $("#modal-select-photo-form-pager").on("click", ".pager-page", function (e) {
                e.preventDefault();
                var newPage = $(this).data("page");
                if (newPage > selectPhotoPage)
                    selectPhotoPagerAnimateDirection = "right";
                else
                    selectPhotoPagerAnimateDirection = "left";
                selectPhotoPage = newPage;
                listSelectPhotoPhotos();
                renderSelectPhotoPager();
            });

            $("#modal-select-photo-form-pager").on("click", ".pager-prev", function (e) {
                e.preventDefault();
                selectPhotoPage = selectPhotoPage - 1;
                selectPhotoPagerAnimateDirection = "left";
                listSelectPhotoPhotos();
                renderSelectPhotoPager();
            });

            $("#modal-select-photo-form-pager").on("click", ".pager-next", function (e) {
                e.preventDefault();
                selectPhotoPage = selectPhotoPage + 1;
                selectPhotoPagerAnimateDirection = "right";
                listSelectPhotoPhotos();
                renderSelectPhotoPager();
            });

        });




        // ***************************************************************************************************************

        // validations
        $("#modal-update-child-form").validate({
            rules: {
                name: {
                    minlength: 3,
                    required: true
                },
                birthday: {
                    required: true
                },
                description: {
                    maxlength: 1000
                },
                mscountry: {
                    required: true
                },
                msdatetime: {
                    required: true
                },
                msdescription: {
                    maxlength: 1000
                }
            },
            messages: {

            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        // validations
        $("#modal-change-status-form").validate({
            rules: {
                founddescription: {
                    required: true,
                    maxlength: 1000
                },
                foundcountry: {
                    required: true
                },
                founddatetime: {
                    required: true
                }
            },
            messages: {

            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#modal-change-status-form").on("submit", function (e) {
            e.preventDefault();
            var form = $("#modal-change-status-form");
            if (form.valid()) {
                changeStatus();
            }

        });

        $("#modal-edit-child-form").on("submit", function (e) {
            e.preventDefault();
            var form = $("#modal-edit-child-form");
            if (form.valid()) {
                updateChild();
            }
        });

        $("#btn-delete-face-photo").on("click", function (e) {
            e.preventDefault();

            bootbox.confirm({
                message: "This photo will be discarded.<br><br>Are you sure you want to discard this photo?",
                title: '<i class="fa fa-trash-o"></i>&nbsp;Discard Photo',
                callback: function (result) {
                    if (result == true) {
                        var photo_id = $("#photo-input").val();
                        //alert(photo_id);

                        $.ajax({
                            type: "DELETE",
                            url: "./api/photos/" + photo_id,
                            contentType: "application/json",
                            async: true,
                            data: "",

                            beforeSend: function () {
                                $("#modal-wait").modal("show");
                            },

                            complete: function () {
                                $("#modal-wait").modal("hide");

                            },

                            success: function (data) {
                                $("#modal-wait").modal("hide");
                                location.reload();
                            },

                            error: function (xmlHttpRequest, status, exception) {
                                $("#modal-wait").modal("hide");
                                message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                            }
                        });
                    } else {
                        return;
                    }
                }
            });
        });


        function changeStatus() {

            var form = $("#modal-change-status-form");

            var foundCountry = form.find("[name=foundcountry]").val();
            var foundDateTime = form.find("#modal-change-status-form-founddatetime").data("datetimepicker").getDate();;
            var foundDescription = form.find("[name=founddescription]").val();;

            var statData = new Object();

            statData.FoundCountry = foundCountry;
            statData.FoundDateTime = foundDateTime;
            statData.FoundDescription = foundDescription;
            statData.Status = "Found";


            $.ajax({
                type: "POST",
                url: "./api/children/" + child.ID,
                contentType: "application/json",
                async: true,
                data: JSON.stringify(statData),

                beforeSend: function () {
                    //$("#modal-wait").modal("show");
                },

                complete: function () {
                    //$("#modal-wait").modal("hide");
                    getChildInfo();
                    $("#modal-change-status").modal("hide");
                },

                success: function (data) {
                    //$("#modal-wait").modal("hide");
                    //window.location = "./ChildProfile.aspx?id=" + data;


                },

                error: function (xmlHttpRequest, status, exception) {
                    //$("#modal-wait").modal("hide");
                    //message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });

        }


        function updateChild() {
            var form = $("#modal-edit-child-form");

            var name = form.find("[name=name]").val();
            var gender = form.find("[name=gender]:checked").val();
            var birthday = form.find("#modal-add-child-form-birthday").data("datetimepicker").getDate();
            var description = form.find("[name=description]").val();
            var missingCountry = form.find("[name=mscountry]").val();
            var missingDateTime = form.find("#modal-add-child-form-msdatetime").data("datetimepicker").getDate();;
            var missingDescription = form.find("[name=msdescription]").val();;

            var childData = new Object();

            childData.Name = name;
            childData.Gender = gender;
            childData.Birthday = birthday;
            childData.Description = description;
            childData.MissingCountry = missingCountry;
            childData.MissingDateTime = missingDateTime;
            childData.MissingDescription = missingDescription;


            $.ajax({
                type: "POST",
                url: "./api/children/" + child.ID,
                contentType: "application/json",
                async: true,
                data: JSON.stringify(childData),

                beforeSend: function () {
                    //$("#modal-wait").modal("show");
                },

                complete: function () {
                    //$("#modal-wait").modal("hide");
                    getChildInfo();
                    $("#modal-edit-child").modal("hide");
                },

                success: function (data) {
                    //$("#modal-wait").modal("hide");
                    //window.location = "./ChildProfile.aspx?id=" + data;

                    
                },

                error: function (xmlHttpRequest, status, exception) {
                    //$("#modal-wait").modal("hide");
                    //message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });
        }







        function renderTimeMap() {

            timeMap = TimeMap.init({
                mapId: "map",
                timelineId: "timeline",
                options: {
                    mapType: "normal",
                    eventIconPath: "./Scripts/TimeMap/Images/",
                    //centerMapOnItems: true,
                    //syncBands: true
                },
                datasets: [
                    {
                        type: "json_string",

                        options: {
                            url: "api/timemap/children/" + child.ID
                        }
                    }
                ],
                bandInfo: [
                    {
                        width: "80%",
                        intervalUnit: Timeline.DateTime.MONTH,
                        intervalPixels: 125,
                        showEventText: true,
                    },
                     {
                         width: "20%",
                         intervalUnit: Timeline.DateTime.YEAR,
                         intervalPixels: 125,
                         showEventText: false,
                         trackHeight: 0.2,
                         trackGap: 0.2,
                     }
                ]
            });
        }

    </script>


    <script type="text/javascript">


        function renderSelectPhotoPager() {
            $.getJSON("./api/photos/info?page=" + selectPhotoPage + "&limit=18&profiled=false&linked=true&facemarked=true&markedchild=" + child.ID, {})
                .done(function (data) {
                })

                .success(function (data) {
                    var pager = $("#modal-select-photo-form-pager");
                    //console.log(pager);
                    pager.empty();

                    if (data.TotalPages <= 1)
                        return;

                    if (selectPhotoPage == 1) {
                        pager.append('<li class="disabled"><a>Prev</a></li>');
                    } else {
                        pager.append('<li class="pager-prev"><a href="#">Prev</a></li>');
                    }

                    for (var i = 1; i <= data.TotalPages; i++) {

                        pager.append('<li data-page="' + i + '" class="pager-page ' + (i == selectPhotoPage ? 'active' : '') + '"><a href="#">' + i + '</a></li>');
                    }

                    if (selectPhotoPage == data.TotalPages) {
                        pager.append('<li class="disabled"><a>Next</a></li>');
                    } else {
                        pager.append('<li class="pager-next"><a href="#">Next</a></li>');
                    }
                });
        }

        function listSelectPhotoPhotos() {
            $.getJSON("./api/photos?page=" + selectPhotoPage + "&limit=18&profiled=false&linked=true&facemarked=true&markedchild=" + child.ID, {})
                .done(function (data) {

                })

                .success(function (data) {

                    $("#modal-select-photo-form-photos").hide();
                    $("#modal-select-photo-form-photos").empty();




                    $.each(data, function (key, val) {
                        var newElem = $('<li class="col-md-2 col-sm-2 col-lg-2 col-xs-2""><a href="#">' + '<img style="width: 96px; height: 96px;" src="./api/photos/' + val.ID + '/image?width=96&height=96" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        $("#modal-select-photo-form-photos").append(newElem);
                        //newElem.show("drop", { direction: selectPhotoPagerAnimateDirection }, 350);
                    });

                    $("#modal-select-photo-form-photos").show("slide", { direction: selectPhotoPagerAnimateDirection }, 275).effect("bounce", { distance: 8, times: 4 }, { duration: 200 });


                });
        }


        // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        $("#modal-list-photo-form-photos").on("click", ".thumbnail", function (e) {
            //alert($(this).data("id"));
            $.getJSON("./api/photos/" + $(this).data("id"), {})
                .success(function (data) {

                    $("#modal-photo-info-btn-profile").data("photo", data.ID);

                    // $("#modal-photo-info-btn-profile").prop('disabled', false);
                    //$("#modal-photo-info-btn-profile").show();

                    /*
                    if (data.MarkedChild != null) {
                        $("#modal-photo-info-btn-profile").removeClass("btn-primary");
                        $("#modal-photo-info-btn-profile").addClass("btn-danger");
                        $("#modal-photo-info-btn-profile").text("Unlink");
                        $("#modal-photo-info-btn-profile").data("mode", "unlink");
                        $("#modal-photo-info-btn-profile").data("child", data.MarkedChild.ID);
                        // console.log(data.PicturedChild);
                        if (data.PicturedChild != null) {
                            $("#modal-photo-info-btn-profile").hide();
                        }

                    } else {
                        $("#modal-photo-info-btn-profile").removeClass("btn-danger");
                        $("#modal-photo-info-btn-profile").addClass("btn-primary");
                        $("#modal-photo-info-btn-profile").text("Link");
                        $("#modal-photo-info-btn-profile").data("mode", "link");

                    }*/


                    //$("#modal-photo-info-btn-profile").attr("href", "./ChildProfile.aspx?id=" + data.ID);

                    // $("#modal-photo-info").find(".thumbnail").attr("src", "./api/photos/" + data.ID + "/image?width=128&height=128");

                    $("#modal-photo-info").find("#modal-photo-info-image").attr("src", "./api/photos/" + data.ID + "/image");

                    $("#modal-photo-info").find(".media-body").children().remove("table");

                    $("#modal-photo-info").find(".media-heading").html(data.Caption);

                    $("#modal-photo-info").find(".media-body")
                        .append(
                        "<table>" +
                        "<tbody>" +

                        "<tr>" + "<th>Uploaded By :&nbsp;</th>" + "<td>" + "<a href='./UserProfile.aspx?id=" + data.Owner.ID + "'>" + data.Owner.Name + "</a>" + "</td>" + "</tr>" +
                        "<tr>" + "<th>Upload Date/Time :&nbsp;</th>" + "<td>" + new Date(data.UploadDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                        "<tr>" + "<th>Captured Date/Time :&nbsp;</th>" + "<td>" + new Date(data.CaptureDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                        "<tr>" + "<th>Captured Location:&nbsp;</th>" + "<td>" + "<a target='_blank' href='https://maps.google.com/maps?z=6&q=" + data.Latitude + "," + data.Longitude + "'> " + data.Country.Name + "</td>" + "</tr>" +
                        "<tr>" + "<th>Linked Child :&nbsp;</th>" + "<td>" + (data.MarkedChild != null ? "<a href='./ChildProfile.aspx?id=" + data.MarkedChild.ID + "'>" + data.MarkedChild.Name + "</a>" : "(none)") + "</td>" + "</tr>" +
                        "<tr>" + "<th valign='top'>Description:</th>" + "<td>" + (data.Description.replace(/\n/g, '<br />')) + "</td>" + "</tr>" +
                        "</tbody>" +
                        "</table>");

                    $("#modal-photo-info").modal("show");
                })
                .fail(function () {
                    // will this fail?
                });
        });

        $("#btn-list-linked-photos").on("click", function (e) {
            $("#modal-list-photo-form-photos").hide();
            $("#modal-list-photo-form-photos").empty();
        });

        $("#modal-list-photo-form-pager").on("click", ".pager-page", function (e) {
            e.preventDefault();
            var newPage = $(this).data("page");
            if (newPage > listPhotoPage)
                listPhotoPagerAnimateDirection = "right";
            else
                listPhotoPagerAnimateDirection = "left";
            listPhotoPage = newPage;
            listListPhotoPhotos();
            renderListPhotoPager();
        });

        $("#modal-list-photo-form-pager").on("click", ".pager-prev", function (e) {
            e.preventDefault();
            listPhotoPage = listPhotoPage - 1;
            listPhotoPagerAnimateDirection = "left";
            listListPhotoPhotos();
            renderListPhotoPager();
        });

        $("#modal-list-photo-form-pager").on("click", ".pager-next", function (e) {
            e.preventDefault();
            listPhotoPage = listPhotoPage + 1;
            listPhotoPagerAnimateDirection = "right";
            listListPhotoPhotos();
            renderListPhotoPager();
        });

        $("#modal-list-photo").on("shown.bs.modal", function (e) {

            listListPhotoPhotos();
            renderListPhotoPager();
        });

        var listPhotoPage = 1;
        var listPhotoPagerAnimateDirection = "up";

        function renderListPhotoPager() {
            $.getJSON("./api/photos/info?page=" + listPhotoPage + "&limit=18&linked=true&facemarked=true&markedchild=" + child.ID, {})
                .done(function (data) {
                })

                .success(function (data) {
                    var pager = $("#modal-list-photo-form-pager");
                    //console.log(pager);
                    pager.empty();

                    if (data.TotalPages <= 1)
                        return;

                    if (listPhotoPage == 1) {
                        pager.append('<li class="disabled"><a>Prev</a></li>');
                    } else {
                        pager.append('<li class="pager-prev"><a href="#">Prev</a></li>');
                    }

                    for (var i = 1; i <= data.TotalPages; i++) {

                        pager.append('<li data-page="' + i + '" class="pager-page ' + (i == listPhotoPage ? 'active' : '') + '"><a href="#">' + i + '</a></li>');
                    }

                    if (listPhotoPage == data.TotalPages) {
                        pager.append('<li class="disabled"><a>Next</a></li>');
                    } else {
                        pager.append('<li class="pager-next"><a href="#">Next</a></li>');
                    }
                });
        }

        function listListPhotoPhotos() {
            $.getJSON("./api/photos?page=" + listPhotoPage + "&limit=18&linked=true&facemarked=true&markedchild=" + child.ID, {})
                .done(function (data) {

                })

                .success(function (data) {

                    $("#modal-list-photo-form-photos").hide();
                    $("#modal-list-photo-form-photos").empty();




                    $.each(data, function (key, val) {
                        var newElem = $('<li class="col-md-2 col-sm-2 col-lg-2 col-xs-2""><a href="#">' + '<img style="width: 96px; height: 96px;" src="./api/photos/' + val.ID + '/image?width=96&height=96" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        $("#modal-list-photo-form-photos").append(newElem);
                        //newElem.show("drop", { direction: selectPhotoPagerAnimateDirection }, 350);
                    });

                    $("#modal-list-photo-form-photos").show("slide", { direction: listPhotoPagerAnimateDirection }, 275).effect("bounce", { distance: 8, times: 4 }, { duration: 200 });


                });
        }



        // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

        function renderProfile() {
            $("#prof-child-name").text(child.Name);
            $("#prof-child-gender").text(child.Gender);
            $("#prof-child-birthday").html(new Date(child.Birthday).toString("d MMMM yyyy - h:mm tt") + " (<abbr title='" + child.Age + " years old as today.'>" + child.Age + " years old</abbr>)");
            $("#prof-child-added-on").text( new Date(child.CreationDateTime).toString("d MMMM yyyy - h:mm tt"));
            $("#prof-child-missing-on").html(new Date(child.MissingDateTime).toString("d MMMM yyyy - h:mm tt")  + " (<abbr title='" + child.MissingAge + " years old as missing time.'>" + child.MissingAge + " years old</abbr>)");
            $("#prof-child-status").text(child.Status);
            $("#prof-child-owner").text(child.Owner.Name);
            $("#prof-child-missing-location").text(child.MissingCountry.Name);
            $("#prof-child-missing-description").html(child.MissingDescription.replace(/\n/g, '<br />'));
            $("#prof-child-description").html(child.Description.replace(/\n/g, '<br />'));
            $("#prof-child-picture").attr("src", "./api/children/" + child.ID + "/picture?width=140&height=140");

            if (child.Status === "Found") {

                $("#prof-child-found-location").text(child.FoundCountry.Name);
                $("#prof-child-found-on").html(new Date(child.FoundDateTime).toString("d MMMM yyyy - h:mm tt") + " (<abbr title='" + child.FoundAge + " years old as found time.'>" + child.FoundAge + " years old</abbr>)");
               $("#prof-child-found-description").html(child.FoundDescription.replace(/\n/g, '<br />'));

                $("#found-information").removeClass("hide");
            } else {
                $("#found-information").addClass("hide");
            }

            var form = $("#modal-edit-child-form");

            var name = form.find("[name=name]");
            var gender = form.find("[name=gender]");
            var birthday = form.find("#modal-add-child-form-birthday");
            var description = form.find("[name=description]");
            var missingCountry = form.find("[name=mscountry]");
            var missingDateTime = form.find("#modal-add-child-form-msdatetime");
            var missingDescription = form.find("[name=msdescription]");

            name.val(child.Name);
            child.Gender === "Male" ? $(gender[0]).trigger("click") : $(gender[1]).trigger("click");
            $(birthday).data("datetimepicker").setDate(new Date(child.Birthday));
            description.val(child.Description);
            missingCountry.val(child.MissingCountry.ID);
            $(missingDateTime).data("datetimepicker").setDate(new Date(child.MissingDateTime));
            missingDescription.val(child.MissingDescription);
            // birthday
            // desc
            // msc
            // msdate
            // msdesc
        }

        function postComment() {


            var commentData = new Object();

            commentData.Message = $("#comment-input").val();

            //console.log(commentData);

            $.ajax({
                type: "POST",
                url: "./api/children/" + child.ID + "/comments",
                contentType: "application/json",
                async: true,
                data: JSON.stringify(commentData),

                beforeSend: function () {
                    //$("#modal-wait").modal("show");
                },

                complete: function () {
                    //$("#modal-wait").modal("hide");
                },

                success: function (data) {
                    //$("#modal-wait").modal("hide");
                    listComments();
                    $("#comment-form").trigger("reset");
                    ///window.location = "./ChildProfile.aspx?id=" + data;
                },

                error: function (xmlHttpRequest, status, exception) {
                    //$("#modal-wait").modal("hide");
                    //message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });
        }

        function listComments() {

            $("#comments-container").empty();

            $.ajax({
                url: "./api/children/" + child.ID + "/comments",
                dataType: "json",
                beforeSend: function () {
                    //$("#modal-wait").modal("show");
                },

                complete: function () {
                    //$("#modal-wait").modal("hide");
                    $("#comments-container").scrollTop($("#comments-container")[0].scrollHeight);
                },

                success: function (data) {
                    //$("#modal-wait").modal("hide");
                    $("#comments-container").empty();
                    $.each(data, function (key, val) {
                        //var newElem = $('<li class="col-lg-2 col-md-2 col-sm-3 col-xs-4" style="display: none;"><a href="#">' + ((val.MarkedChild != null) ? '<code style="position:absolute" class="btn-success"><i class="fa fa-link"></i>&nbsp;Linked</code>' : '') + '<img src="./api/photos/' + val.ID + '/image?width=128&height=128" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        //$("#container-gallery").append(newElem);
                        //newElem.show("drop", { direction: pagerAnimateDirection }, 375);
                        //$("#comments-container").append("<p>" + val.Message + "</p>");

                        $("#comments-container").append(
                            '<div class="media" style="padding: 5px">' +
                            '<a class="pull-left" href="./UserProfile.aspx?id=' + val.Owner.ID + '">' +
                            '<img style="width: 48px; height: 48px" class="media-object img-rounded" src="./api/users/' + val.Owner.ID + '/picture?width=48&height=48">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<a href="./UserProfile.aspx?id=' + val.Owner.ID + '">' + '<strong class="media-heading">' + val.Owner.Name + '</strong></a>' + '&nbsp;&nbsp;&nbsp;<br><p class="label label-info">' + new Date(val.CreationDateTime).toString("d MMMM yyyy - h:mm tt") + '</p>' +
                            '<table>' +
                            '<tbody>' + 
                            '<tr>' +
                            '<td style="padding-top: 8px">' + val.Message + '</td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>' +
                            '</div>' +
                            //'<a class="btn pull-right btn-danger btn-xs"><i class="fa fa-trash-o"></i>&nbsp;Delete</a>' +
                            '<hr style="margin:0px; padding:0px" />' +
                            '</div>' +
                            ''
                            );


                    });

                    
                    //updateStatistic();
                },
            });


        }

    </script>


        <!-- Google Map for photo upload -->
    <script type="text/javascript">

        function geolocationSuccess(pos) {
            var crd = pos.coords;

            photoUploadLongitude = crd.longitude;
            photoUploadLatitude = crd.latitude;
        }

        function geolocationError(err) {
            photoUploadLongitude = 0.0;
            photoUploadLatitude = 0.0;
        }

        $(document).ready(function () {
            var circle = null;
            var radius = $("#radius").val();

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
            }

            var StartPosition = new google.maps.LatLng(photoUploadLatitude, photoUploadLongitude);

            function DrawCircle(photoUploadMap, center, radius) {

                if (circle != null) {
                    circle.setMap(null);
                }

                if (radius > 0) {
                    radius *= 1609.344;
                    circle = new google.maps.Circle({
                        center: center,
                        radius: radius,
                        strokeColor: "#0000FF",
                        strokeOpacity: 0.35,
                        strokeWeight: 2,
                        fillColor: "#0000FF",
                        fillOpacity: 0.20,
                        map: photoUploadMap
                    });
                }
            }

            function SetPosition(Location, Viewport) {
                photoUploadMapMarker.setPosition(Location);
                if (Viewport) {
                    photoUploadMap.fitBounds(Viewport);
                    photoUploadMap.setZoom(map.getZoom() + 2);
                }
                else {
                    photoUploadMap.panTo(Location);
                }
                radius = $("#radius").val();
                DrawCircle(photoUploadMap, Location, radius);
                $("#latitude").val(Location.lat().toFixed(8));
                $("#longitude").val(Location.lng().toFixed(8));

                // Reverse geocode for country
                geocoder.geocode({ 'latLng': photoUploadMapMarker.position }, function (results, status) {
                    $("#country").val("");
                    if (status == google.maps.GeocoderStatus.OK) {
                        var newzip = results[0].address_components['postal_code'];
                        $.each(results, function () {
                            if (this.types[0] === "country") {
                                $("#country").val(this.address_components[0].short_name);
                                return;
                            }

                        });
                    }
                });
            }

            var MapOptions = {
                zoom: 12,
                center: StartPosition,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                disableDoubleClickZoom: true,
                streetViewControl: false
            };

            var MapView = $("#map");
            photoUploadMap = new google.maps.Map(MapView.get(0), MapOptions);

            photoUploadMapMarker = new google.maps.Marker({
                position: StartPosition,
                map: photoUploadMap,
                title: "Drag Me",
                draggable: true
            });

            google.maps.event.addListener(photoUploadMapMarker, "dragend", function (event) {
                SetPosition(photoUploadMapMarker.position);
            });

            $("#radius").keyup(function () {
                google.maps.event.trigger(photoUploadMapMarker, "dragend");
            });

            DrawCircle(photoUploadMap, StartPosition, radius);
            SetPosition(photoUploadMapMarker.position);

            $("#modal-upload-photo").on("shown.bs.modal", function () {
                // Workaround for map resize problem
                google.maps.event.trigger(photoUploadMap, 'resize');
                SetPosition(new google.maps.LatLng(photoUploadLatitude, photoUploadLongitude));
            });
        });
    </script>


    <script type="text/javascript">

        $(document).ready(function () {


            
            if (authInfo != null && authInfo.ID === child.Owner.ID) {
                $(".owner-item").removeClass('hide');
            }
        });

    </script>




</body>

</html>
