﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.MyProfile" %>

<!doctype html>

<html>
  
  <head>
    <title>New Page</title>
    <meta name="viewport" content="width=device-width">


      <script type="text/javascript">

          var id = "";



          window.location.replace("./Index.aspx");

      </script>

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
    <style type="text/css">
      ul.row {
        padding:0 0 0 0;
        margin:0 0 0 0;
      }
      ul.row li {
        list-style:none;
        margin-bottom:0px;
      }
      ul.row li img {
        /*cursor: pointer;*/
      }
      ul.row li img.thumbnail {
        width: 128px;
        height: 128px;
      }
    </style>
  </head>
  
  <body>
    <div class="navbar navbar-static-top navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
          </button>
          <a href="#" class="navbar-brand">Save A Child Today</a>
        </div>
        <ul class="nav navbar-nav navbar-right"></ul>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li>
              <a href="index.html">Home</a>
            </li>
            <li>
              <a href="photos.html">Photos</a>
            </li>
            <li>
              <a href="#">People</a>
            </li>
            <li>
              <a href="#">About</a>
            </li>
          </ul>
          <ul class="nav navbar-nav pull-right">
            <li class="active">
              <a href="#">My Profile</a>
            </li>
          </ul>
          <form class="navbar-form navbar-right">
            <div class="form-group"></div>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-sign-in"></i> Sign in</button>
          </form>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="btn-toolbar pull-right">
            <div class="btn-group">
              <a class="btn btn-default"><i class="fa fa-chain"></i></i> Link Photo</a><a class="btn btn-default"><i class="fa fa-upload"></i> Upload Photo</a>
            </div>
            <div class="btn-group">
              <a class="btn btn-default" data-toggle="dropdown" href="#"><i class="fa fa-edit"></i> Edit Profile</a><a class="btn dropdown-toggle btn-default" data-toggle="dropdown" href="#">    <span class="caret"></span>  </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#"><i class="fa fa-gear"></i> Change Password</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="container">
      <h3><i class="fa fa-user"></i>&nbsp;DK Dhilip</h3>
      <div class="row">
        <div class="col-md-2">
          <div class="thumbnail" style="width: 150px; height: auto;">
            <img src="https://app.divshot.com/img/placeholder-100x100.gif" width="150px" height="150px">
            <div class="caption">
              <a class="btn btn-block btn-default btn-sm"><i class="fa fa-edit"></i> Change</a>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <h4>Personal Details</h4>
          <table>
            <tbody>
              <tr>
                <th>
                  <i class="fa fa-map-marker"></i> Location:&nbsp;</th>
                <td>Malaysia</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-phone"></i> Contact:</th>
                <td>016-7592062</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-envelope"></i> Email:</th>
                <td>
                  <a href="mailto:dhilip89@hotmail.com">dhilip89@hotmail.com</a>
                </td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-globe"></i> Website:</th>
                <td>
                  <a href="https://www.facebook.com/Dhilip89">https://www.facebook.com/Dhilip89</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-5">
          <h4>Missing Information</h4>
        </div>
      </div>
    </div>
    <!--hr-->
    <hr>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- tab start -->
          <ul id="myTab" class="nav nav-tabs">
            <li class="active">
              <a href="#home" data-toggle="tab"><i class="fa fa-comment"></i> Comments</a>
            </li>
            <li>
              <a href="#profile" data-toggle="tab"><i class="fa fa-calendar"></i> Timeline</a>
            </li>
            <li>
              <a href="#profile" data-toggle="tab"><i class="fa fa-map-marker"></i> Maps</a>
            </li>
            <li>
              <a href="#profile" data-toggle="tab"><i class="fa fa-th"></i> Photos</a>
            </li>
          </ul><br>
          <!-- tab content -->
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="home">
              <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.&nbsp;</p>
            </div>
            <div class="tab-pane fade" id="profile">
              <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
            </div>
            <div class="tab-pane fade" id="dropdown1">
              <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
            </div>
            <div class="tab-pane fade" id="dropdown2">
              <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
            </div>
          </div>
          <!-- tab end -->
        </div>
      </div>
    </div>





  </body>

</html>