﻿using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Web.Security;

namespace OrigineIT.SaveAChildToday.Web.Security
{
    public class SimpleRoleProvider : RoleProvider
    {
        #region Not Implemented
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
        #endregion

        public override string[] GetRolesForUser(string username)
        {
            if (NHPersistentStore.IsConfigured)
            {
                using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
                {
                    NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                    User user = userRepository.FindBy(x => x.Email.Equals(username));

                    if (user != null)
                    {
                        return new String[] { user.Role.ToString() };
                    }
                }
            }

            return new String[] { };
        }
    }
}
