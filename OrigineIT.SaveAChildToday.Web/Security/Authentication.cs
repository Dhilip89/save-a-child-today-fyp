﻿using OrigineIT.SaveAChildToday.Common.Enumerables;
using OrigineIT.SaveAChildToday.DataAccess;
using OrigineIT.SaveAChildToday.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace OrigineIT.SaveAChildToday.Web.Security
{
    public class Authentication
    {
        public static User CurrentUser
        {
            get
            {
                if (Authentication.IsAuthenticated)
                {
                    using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
                    {
                        NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);

                        return userRepository.FindBy(Ticket.UserData);
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public static FormsAuthenticationTicket Ticket
        {
            get
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                return FormsAuthentication.Decrypt(authCookie.Value);
            }
        }

        public static Boolean IsAuthenticated
        {
            get { return System.Web.HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        public static UserRole CurrentRole
        {
            get
            {
                User user = Authentication.CurrentUser;

                if (user != null)
                    return user.Role;
                else
                    return UserRole.Anonymous;
            }
        }

        public static Boolean Authenticate(String email, String password, Boolean persistent = false)
        {
            using (NHUnitOfWork unitOfWork = NHPersistentStore.NewUnitOfWork())
            {
                NHRepository<String, User> userRepository = new NHRepository<String, User>(unitOfWork);
                
                User user = userRepository.FindBy(x => x.Email.Equals(email));

                if ((user != null) && (Password.Validate(password, user.HashedPassword)))
                {
                    FormsAuthentication.SignOut();

                    FormsAuthenticationTicket authTicket
                        = new FormsAuthenticationTicket(
                            1,
                            user.Email,
                            DateTime.UtcNow,
                            DateTime.UtcNow.AddMonths(1),
                            persistent,
                            user.ID);

                    String encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                    HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));

                    user.LastLoginDateTime = DateTime.UtcNow;

                    unitOfWork.Commit();

                    return true;
                }
            }

            return false;
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}