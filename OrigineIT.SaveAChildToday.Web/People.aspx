﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="People.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.People" %>

<!doctype html>
<html>
<head>
    <title>Save A Child Today - Find Missing Children Worldwide</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./StyleSheets/bootstrap.min.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-custom.css">
    <link rel="stylesheet" href="./StyleSheets/font-awesome.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="./StyleSheets/internal-common.css">

    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false'></script>
    <script type="text/javascript" src="./Scripts/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootbox.min.js"></script>
    <script type="text/javascript" src="./Scripts/date.js"></script>
    <script type="text/javascript" src="./Scripts/internal-common.js"></script>

    <script type="text/javascript">
        // Shared variables

        var photosPage = 1;
        var pagerAnimateDirection = "up";
        var geocoder = new google.maps.Geocoder();

        var selectChildPage = 1;
        var selectChildPagerAnimateDirection = "left";

        // Map
        var photoUploadLongitude = 0.0;
        var photoUploadLatitude = 0.0;
        var photoUploadMap, photoUploadMapMarker;
    </script>

</head>

<body>

    <!-- Navigation bar -->
    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Save A Child Today</a>
            </div>
            <ul class="nav navbar-nav navbar-right user-item hide">
                <li>
                    <a href="./MyProfile.aspx">My Profile</a>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./Index.aspx">Home</a>
                    </li>
                    <li>
                        <a href="./Photos.aspx">Photos</a>
                    </li>
                    <li class="active">
                        <a href="#">People</a>
                    </li>
                    <li>
                        <a href="./About.aspx">About</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <a id="btn-navbar-signin" class="btn btn-primary guest-item hide" href="#modal-signin" data-toggle="modal"><i class="fa fa-sign-in"></i>&nbsp;Sign in</a>
                    <a id="btn-navbar-signout" class="btn btn-danger user-item hide" href="#"><i class="fa fa-sign-out"></i>&nbsp;Sign out</a>
                </form>
            </div>
        </div>
    </div>


    <!-- Photos container -->
    <div class="container">
        <table class="table table-striped">
        <thead>
          <tr>
            <th>People</th>
            <th>Children Added</th>
            <th>Photos Uploaded</th>
          </tr>
        </thead>
        <tbody id="container-people">
            <!-- Items -->
        </tbody>
      </table>
    </div>

</body>
</html>


        <!-- ----------------------------- Common modals ----------------------------- -->

    <!-- Sign in modal -->
    <div id="modal-signin" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signin-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i>&nbsp;Sign In</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-left" href="#modal-signup" data-toggle="modal">Sign Up</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Sign up modal -->
    <div id="modal-signup" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signup-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-ticket"></i>&nbsp;Sign Up</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="firstname">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter your name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- ############################# Common modals ############################# -->


<script type="text/javascript">

    

    function renderPeopleListView() {

        

       


        $.ajax({
            url: "./api/users",
            dataType: "json",
            beforeSend: function () {
            },
            complete: function () {
                updatePhotosStatus();
            },
            success: function (data) {
                $("#container-people").empty();        
                $.each(data, function (key, val) {
                    $("#container-people").append(
                        "<tr>" +
                        "<td>" + "<img class='' style='width:24px; height:24px' src='./api/users/" + val.ID +"/picture?width=24&height=24'></img>&nbsp;&nbsp;" + "<a href='./UserProfile.aspx?id=" + val.ID + "'>" + val.Name + "</a></td>" +
                        "<td>" + val.Children + "</td>" +
                        "<td>" + val.Photos + "</td>" +
                        "</tr>"
                        );
                });

            },
        });
    }

</script>


<script type="text/javascript">


    $(document).ready(function (evt) {

        renderPeopleListView();

    });

</script>