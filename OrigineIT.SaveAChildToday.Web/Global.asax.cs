﻿using AForge.Imaging.Filters;
using OrigineIT.SaveAChildToday.DataAccess.Enumerables;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;

namespace OrigineIT.SaveAChildToday.Web
{
    public class Global : System.Web.HttpApplication
    {
        public static readonly String DefaultProfilePicture = HttpContext.Current.Server.MapPath("~/App_Data/Defaults/ProfilePicture.png");
        public static readonly String FrontalFaceHaarCascade = HttpContext.Current.Server.MapPath("~/App_Data/OpenCV/HaarCascades/haarcascade_frontalface_alt2.xml");
        public static readonly String TempFolder = HttpContext.Current.Server.MapPath("~/App_Data/Temp");

        // This is only meant for debugging, please remove it
        public static void _ResetDatabase()
        {
            // Close data access
            DataAccessConfig.End();

            // Rebuild database
            DataAccessConfig.Execute(SchemaConfiguration.Rebuild);
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // Append OpenCV dependencies to %PATH% environment variable
            String _path = String.Concat(System.Environment.GetEnvironmentVariable("PATH"), ";", Server.MapPath(ConfigurationManager.AppSettings["OpenCV_NativeDLLs"]));
            System.Environment.SetEnvironmentVariable("PATH", _path, EnvironmentVariableTarget.Process);

            // Turn off XML Serializer for REST API (XML serializer does not support dynamic type)
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            // Execute data access configuration
            DataAccessConfig.Execute(SchemaConfiguration.NoChange);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            // Close data access
            DataAccessConfig.End();
        }
    }
}