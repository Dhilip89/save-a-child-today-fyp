﻿// Global variables
var authInfo;

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

message = function () { }
message.show = function (target, message, type, delay) {
    var alertBox = $('<div class="alert fade in ' + type + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
    $(target).prepend(alertBox);
    alertBox.delay(delay).fadeOut(1000);
}

$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Please check your input."
);

function getAuthInfo() {

    var v;

    $.ajax({

        type: "GET",
        url: "./api/auth/info",
        contentType: "application/json",
        async: false,

        success: function (data) {
            $(".user-item").removeClass('hide');
            $(".guest-item").addClass('hide');
            v = data;
        },

        error: function () {
            $(".user-item").addClass('hide');
            $(".guest-item").removeClass('hide');
            v = null;
        }


    });

    return v;

    /*
    $.getJSON(
        "./api/auth/info",
        {})

    .done(function () {
    })

    .success(function (data) {
        $(".user-item").removeClass('hide');
        $(".guest-item").addClass('hide');
        //console.log(data);
        authInfo = data;
        return data;
    })

    .fail(function () {
        $(".user-item").addClass('hide');
        $(".guest-item").removeClass('hide');
        authInfo = null;
        return null;
    });*/

}

function signIn() {
    var form = $("#modal-signin-form");

    var email = form.find("[name=email]").val();
    var password = form.find("[name=password]").val();

    var signInData = new Object();

    signInData.Email = email;
    signInData.Password = password;
    signInData.Persistent = true;

    $.ajax({
        type: "POST",
        url: "./api/auth/signin",
        contentType: "application/json",
        async: true,
        data: JSON.stringify(signInData),

        beforeSend: function () {
            $("#modal-wait").modal("show");
        },

        complete: function () {
            $("#modal-wait").modal("hide");
        },

        success: function () {
            getAuthInfo();
            location.reload();
        },

        error: function (xmlHttpRequest, status, exception) {
            $("#modal-wait").modal("hide");
            $("#modal-signin-form").effect("shake");
            message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Incorrect email address or password provided.', "alert-danger", 3500);
        }
    });
}

function signUp() {
    var form = $("#modal-signup-form");

    var name = form.find("[name=name]").val();
    var email = form.find("[name=email]").val();
    var password = form.find("[name=password]").val();

    var signUpData = new Object();

    signUpData.Name = name;
    signUpData.Email = email;
    signUpData.Password = password;

    $.ajax({
        type: "POST",
        url: "./api/users",
        contentType: "application/json",
        async: true,
        data: JSON.stringify(signUpData),

        beforeSend: function () {
            $("#modal-wait").modal("show");
        },

        complete: function () {
            $("#modal-wait").modal("hide");
        },

        success: function () {
            $("#modal-signup").modal("hide");
        },

        error: function (xmlHttpRequest, status, exception) {
            $("#modal-wait").modal("hide");
            message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
        }
    });
}

function signOut() {
    $.post("./api/auth/signout").done(function () {
        location.reload();
    });
}

$("#modal-signup-form").validate({
    rules: {
        name: {
            minlength: 3,
            required: true
        },
        password: {
            minlength: 6,
            required: true
        },
        email: {
            required: true,
            email: true,

            remote: {
                url: "./api/validations/email",
                type: "post",
                data: {
                    email: function () {
                        return $("#modal-signup-form [name=email]").val();
                    }
                }
            }
        }
    },
    messages: {
        email: {
            remote: "This email address is already in use."
        }
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$("#modal-signin-form").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true
        }
    },
    messages: {
    },
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});



$(document).ready(function (e) {

    // Init
    authInfo = getAuthInfo();
    //getAuthInfo();

    //console.log(authInfo);

    // Datetime picker
    $('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate: -Infinity,
        endDate: new Date()
    });

    $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        startDate: -Infinity,
        endDate: new Date()
    });

    // Validations
    $("#modal-signup-form").validate({
        rules: {
            name: {
                minlength: 3,
                required: true
            },
            password: {
                minlength: 6,
                required: true
            },
            email: {
                required: true,
                email: true,

                remote: {
                    url: "./api/validations/email",
                    type: "post",
                    data: {
                        email: function () {
                            return $("#modal-signup-form [name=email]").val();
                        }
                    }
                }
            }
        },
        messages: {
            email: {
                remote: "This email address is already in use."
            }
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#modal-signin-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        messages: {
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    // Events
    $("#btn-navbar-signout").on("click", function (e) {
        e.preventDefault();
        signOut();
    });

    $("#modal-signup-form").on("submit", function (e) {
        e.preventDefault();
        if ($("#modal-signup-form").valid()) {
            signUp();
        }
    });

    $("#modal-signin-form").on("submit", function (e) {
        e.preventDefault();

        if ($("#modal-signin-form").valid()) {
            signIn();
        }
    });
});
