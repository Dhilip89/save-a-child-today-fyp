﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="OrigineIT.SaveAChildToday.Web.Index" %>

<!doctype html>

<html>

<head>
    <title>Save A Child Today - Find Missing Children Worldwide</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./StyleSheets/bootstrap.min.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-custom.css">
    <link rel="stylesheet" href="./StyleSheets/font-awesome.css">
    <link rel="stylesheet" href="./StyleSheets/bootstrap-datetimepicker.min.css">

    <script type="text/javascript" src="./Scripts/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="./Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="./Scripts/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="./Scripts/date.js"></script>


    <style type="text/css">
        ul.row {
            padding: 0 0 0 0;
            margin: 0 0 0 0;
        }

            ul.row li {
                list-style: none;
                margin-bottom: 0px;
            }

                ul.row li img {
                    /*cursor: pointer;*/
                }

                    ul.row li img.thumbnail {
                        width: 128px;
                        height: 128px;
                    }
    </style>
</head>

<body>

    <div id="modal-add-child" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-add-child-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-plus-circle"></i>&nbsp;Add Child</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="name">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter child&apos;s name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="gender">Gender:&nbsp;</label><br />
                            <div class="btn-group" data-toggle="buttons">

                                <label class="btn btn-default active">
                                    <input type="radio" name="gender" value="Male" checked="">
                                    <i class="fa fa-male"></i>&nbsp;Male
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="gender" value="Female">
                                    <i class="fa fa-female"></i>&nbsp;Female
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="birthday">Birthday:&nbsp;</label>
                            <!-- dd MM yyyy -->
                            <div id="modal-add-child-form-birthday" class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-format="yyyy-mm-dd">
                                <input name="birthday" class="form-control" size="16" type="text" placeholder="Select a date" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="description">Description:&nbsp;</label>

                            <textarea name="description" class="form-control" placeholder="Tell us more about this child..." rows="3"></textarea>

                        </div>


                        <div class="form-group">
                            <label class="control-label" for="mscountry">Missing Location:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <select class="form-control" name="mscountry">
                                    <option value="" selected>Select country</option>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <option value="<%# Eval("ID") %>" data-longitude="<%# Eval("Longitude") %>" data-latitude="<%# Eval("Latitude") %>"><%# Eval("Name") %></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="msdatetime">Missing Date/Time:&nbsp;</label>
                            <div id="modal-add-child-form-msdatetime" class="input-group date form_datetime" data-date="" data-date-format="dd MM yyyy - HH:ii p" data-link-format="yyyy-mm-dd">
                                <input name="msdatetime" class="form-control" size="16" type="text" placeholder="Select a date and time" value="" readonly="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="msdescription">Missing Description:&nbsp;</label>

                            <textarea name="msdescription" class="form-control" placeholder="Describe how the child was missing..." rows="3"></textarea>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- filter modal -->

    <div id="modal-filter" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-filter-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-filter"></i>&nbsp;Filter</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="country">By Location:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <select class="form-control" name="country">
                                    <option value="" selected>All Countries</option>
                                    <asp:Repeater ID="Repeater2" runat="server">
                                        <ItemTemplate>
                                            <option value="<%# Eval("ID") %>" data-longitude="<%# Eval("Longitude") %>" data-latitude="<%# Eval("Latitude") %>"><%# Eval("Name") %></option>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="msdate" class="control-label input-group">By Missing Date:</label>
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default active">
                                    <input type="radio" name="msdate" value="all" checked="">All Time
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="msdate" value="lw">Last Week
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="msdate" value="lm">Last Month
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="msdate" value="ly">Last Year
                                </label>
                            </div>
                        </div>


                        <label for="status" class="control-label input-group">By Status:</label>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="status" value="all" checked="">All
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="status" value="ms"><i class="fa fa-question"></i>&nbsp;Missing
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="status" value="fd"><i class="fa fa-check"></i>&nbsp;Found
                            </label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="btn-clearfilter-modal" type="button" class="btn btn-default pull-left" data-dismiss="modal">Clear</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Apply</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    <!-- sign in modal -->

    <div id="modal-signin" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signin-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-sign-in"></i>&nbsp;Sign In</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default pull-left" href="#modal-signup" data-toggle="modal">Sign Up</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    <!-- sign up modal -->

    <div id="modal-signup" class="modal fade">
        <div class="modal-dialog" style="width: 550px">
            <form role="form" id="modal-signup-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-ticket"></i>&nbsp;Sign Up</h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label" for="firstname">Name:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input class="form-control" placeholder="Enter your name" name="name" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="firstname">Email Address:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input class="form-control" placeholder="user@example.com" name="email" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">Password:&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input class="form-control" placeholder="Password" name="password" type="password" />
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign Up</button>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->

    <!-- child info modal -->

    <div id="modal-child-info" class="modal fade">
        <div class="modal-dialog" style="width: 650px">
            <form role="form" id="modal-child-info-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class="fa fa-user"></i>&nbsp;About This Child</h3>
                    </div>
                    <div class="modal-body">

                        <div class="media">
                            <a class="pull-left" href="#">
                                <img style="width: 128px; height: 128px" class="media-object thumbnail" src="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"></h4>
                                <!-- child info -->
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <!--button type="submit" class="btn btn-primary">Profile</!--button-->
                        <a id="modal-child-info-btn-profile" class="btn btn-primary" href="#">Profile</a>
                    </div>
                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->


    <!-- loading modal -->

    <div id="modal-wait" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <form role="form" id="Form2">
                <div class="modal-content">

                    <div class="modal-body">
                        <h4>Processing, please wait...</h4>
                        <div class="progress progress-striped active">
                            <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>

                </div>
            </form>

            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- -->

    <div class="navbar navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand">Save A Child Today</a>
            </div>
            <ul class="nav navbar-nav navbar-right user-item hide">
                <li>
                    <a href="./MyProfile.aspx">My Profile</a>
                </li>
            </ul>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="./Photos.aspx">Photos</a>
                    </li>
                    <li>
                        <a href="./People.aspx">People</a>
                    </li>
                    <li>
                        <a href="./About.aspx">About</a>
                    </li>
                </ul>
                <form class="navbar-form navbar-right">
                    <a id="btn-signin" class="btn btn-primary guest-item hide" href="#modal-signin" data-toggle="modal"><i class="fa fa-sign-in"></i>&nbsp;Sign in</a>
                    <a id="btn-signout" class="btn btn-danger user-item hide" href="#"><i class="fa fa-sign-out"></i>&nbsp;Sign out</a>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left btn-group">
                    <a href="./TimelineMap.aspx" class="btn btn-default"><i class="fa fa-calendar"></i>&nbsp;Timeline Map</a><a href="#" class="btn btn-success"><i class="fa fa-th"></i> Gallery</a>
                </div>
                <span class="btn"><i class="fa fa-question"></i>&nbsp;<span id="children-count-missing">0</span>&nbsp;Missing&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check"></i>&nbsp;<span id="children-count-found">0</span>&nbsp;Found</span>
                <div class="btn-toolbar pull-right">
                    <div class="btn-group">
                        <a href="#modal-add-child" data-toggle="modal" class="btn btn-default user-item hide"><i class="fa fa-plus-circle"></i>&nbsp;Add Child</a>
                    </div>
                    <div class="btn-group">
                        <a id="btn-filter" class="btn btn-default" href="#modal-filter" data-toggle="modal"><i class="fa fa-filter"></i>&nbsp;Filter</a><a id="btn-filter-menu" class="btn dropdown-toggle btn-default" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a id="btn-clearfilter" href="#"><i class="fa fa-times"></i>&nbsp;Clear Filter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>



    <div class="container">
        <ul id="container-gallery" class="row">
            <!-- children -->
        </ul>
    </div>
    <!--hr-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul id="pager-gallery" class="pagination pull-left">
                    <!-- pages -->
                </ul>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $.validator.addMethod(
            "regex",
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
    </script>




    <!-- datetime picker -->

    <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1,
            startDate: -Infinity,
            endDate: new Date() 
        });

        $('.form_date').datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            startDate: -Infinity,
            endDate: new Date()
        });
    </script>


    <script type="text/javascript">
        message = function () { }
        message.show = function (target, message, type, delay) {
            var alertBox = $('<div class="alert fade in ' + type + '"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
            $(target).prepend(alertBox);
            alertBox.delay(delay).fadeOut(1000);
        }
    </script>


    <!-- core script -->
    <script type="text/javascript">

        // global vars
        var timeMap;
        var authInfo;
        var filter = false;
        var filterCriteriaQuery = "";
        var page = 1;
        var pagerAnimateDirection = "up";

        function applyFilter() {
            filter = true;
            var form = $("#modal-filter-form");

            var country = form.find("[name=country]").val();
            var msdate = form.find("[name=msdate]:checked").val();
            var status = form.find("[name=status]:checked").val();

            var date;
            //date = new Date(Date.parse("t - 7d"));
            //alert("ge::" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate());

            switch (msdate) {
                case "all":
                    filterCriteriaQuery = "";
                    break;
                case "lw":
                    date = new Date(Date.parse("t - 7d"));
                    filterCriteriaQuery = "&missingdatetime=ge::" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                    break;
                case "lm":
                    date = new Date(Date.parse("t - 7m"));
                    filterCriteriaQuery = "&missingdatetime=ge::" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                    break;
                case "ly":
                    date = new Date(Date.parse("t - 7y"));
                    filterCriteriaQuery = "&missingdatetime=ge::" + date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
                    break;
            }

               
            switch (status) {
                case "all":
                    filterCriteriaQuery += "";
                    break;
                case "ms":
                    filterCriteriaQuery += "&status=Missing";
                    break;
                case "fd":
                    filterCriteriaQuery += "&status=Found";
                    break;
            }

            if (country.length > 0)
                filterCriteriaQuery += ("&missingcountry=" + country);

            $("#modal-filter").modal("hide");
            listChildren();
            renderPager();
            updateStatistic();
            $("#btn-filter-menu").addClass("btn-info");
            $("#btn-filter-menu").removeClass("btn-default");
            $("#btn-filter").addClass("btn-info");
            $("#btn-filter").removeClass("btn-default");
        }

        function resetFilter() {
            if (filter == true) {
                filter = false;
                var form = $("#modal-filter-form");
                form.trigger("reset");
                form.find("[name=msdate]").parent("label").removeClass("active");
                form.find("[name=status]").parent("label").removeClass("active");
                form.find("[name=msdate]:checked").parent("label").addClass("active");
                form.find("[name=status]:checked").parent("label").addClass("active");
                filterCriteriaQuery = "";
                listChildren();
                renderPager();
                updateStatistic();
                $("#btn-filter-menu").removeClass("btn-info");
                $("#btn-filter-menu").addClass("btn-default");
                $("#btn-filter").removeClass("btn-info");
                $("#btn-filter").addClass("btn-default");
            }
        }

        function getAuthInfo() {

            $.getJSON(
                "./api/auth/info",
                { /*async: false*/ })

            .done(function () {
            })

            .success(function (data) {
                $(".user-item").removeClass('hide');
                $(".guest-item").addClass('hide');
                return data;
            })

            .fail(function () {
                $(".user-item").addClass('hide');
                $(".guest-item").removeClass('hide');
                return null;
            });

        }

        function signIn() {
            var form = $("#modal-signin-form");

            var email = form.find("[name=email]").val();
            var password = form.find("[name=password]").val();

            var signInData = new Object();

            signInData.Email = email;
            signInData.Password = password;
            signInData.Persistent = true;

            $.ajax({
                type: "POST",
                url: "./api/auth/signin",
                contentType: "application/json",
                async: true,
                data: JSON.stringify(signInData),

                beforeSend: function () {
                    $("#modal-wait").modal("show");
                },

                complete: function () {
                    $("#modal-wait").modal("hide");
                },

                success: function () {
                    getAuthInfo();
                    location.reload();
                },

                error: function (xmlHttpRequest, status, exception) {
                    $("#modal-wait").modal("hide"); 
                    $("#modal-signin-form").effect("shake");
                    message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Incorrect email address or password provided.', "alert-danger", 3500);
                }
            });
        }

        function signUp() {
            var form = $("#modal-signup-form");

            var name = form.find("[name=name]").val();
            var email = form.find("[name=email]").val();
            var password = form.find("[name=password]").val();

            var signUpData = new Object();

            signUpData.Name = name;
            signUpData.Email = email;
            signUpData.Password = password;

            $.ajax({
                type: "POST",
                url: "./api/users",
                contentType: "application/json",
                async: true,
                data: JSON.stringify(signUpData),

                beforeSend: function () {
                    $("#modal-wait").modal("show");
                },

                complete: function () {
                    $("#modal-wait").modal("hide");
                },

                success: function () {
                    $("#modal-signup").modal("hide");
                },

                error: function (xmlHttpRequest, status, exception) {
                    $("#modal-wait").modal("hide");
                    message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });
        }

        function signOut() {
            $.post("./api/auth/signout").done(function () {
                location.reload();
            });
        }

        function addChild() {
            var form = $("#modal-add-child-form");

            var name = form.find("[name=name]").val();
            var gender = form.find("[name=gender]:checked").val();
            var birthday = form.find("#modal-add-child-form-birthday").data("datetimepicker").getDate();
            var description = form.find("[name=description]").val();
            var missingCountry = form.find("[name=mscountry]").val();
            var missingDateTime = form.find("#modal-add-child-form-msdatetime").data("datetimepicker").getDate();;
            var missingDescription = form.find("[name=msdescription]").val();;

            var childData = new Object();

            childData.Name = name;
            childData.Gender = gender;
            childData.Birthday = birthday;
            childData.Description = description;
            childData.MissingCountry = missingCountry;
            childData.MissingDateTime = missingDateTime;
            childData.MissingDescription = missingDescription;


            $.ajax({
                type: "POST",
                url: "./api/children",
                contentType: "application/json",
                async: true,
                data: JSON.stringify(childData),

                beforeSend: function () {
                    $("#modal-wait").modal("show");
                },

                complete: function () {
                    $("#modal-wait").modal("hide");
                },

                success: function (data) {
                    $("#modal-wait").modal("hide");
                    window.location = "./ChildProfile.aspx?id=" + data;
                },

                error: function (xmlHttpRequest, status, exception) {
                    $("#modal-wait").modal("hide");
                    message.show(form.find(".modal-body"), '<i class="fa fa-warning"></i>&nbsp;Something went wrong here, please try again later.', "alert-info", 3500);
                }
            });
        }

        function listChildren() {

            $.getJSON("./api/children?page=" + page + "&limit=24&haspicture=&orderby=creationdatetime" + filterCriteriaQuery, {})
                .done(function (data) {
                    //$("#container-gallery").empty();
                })

                .success(function (data) {
                    $("#container-gallery").hide();
                    $("#container-gallery").empty();
                    $.each(data, function (key, val) {
                        var newElem = $('<li class="col-lg-2 col-md-2 col-sm-3 col-xs-4"><a href="#">' + ((val.Status === "Found") ? '<span style="position:absolute; border: 1px solid green" class="label label-success">Found</span>' : '') + '<img src="./api/photos/' + val.ProfilePicture + '/markedface/image?width=128&height=128" class="thumbnail btn-warning" data-id="' + val.ID + '"></a></li>');
                        $("#container-gallery").append(newElem);
                       // newElem.show("drop", { direction: pagerAnimateDirection }, 350);
                    });
                    $("#container-gallery").show("slide", { direction: pagerAnimateDirection }, 275).effect("bounce", { distance: 8, times: 4 }, { duration: 200 });
                    updateStatistic();
                });
        };

        function updateStatistic() {

            $.getJSON("./api/children?page=" + "1" + "&haspicture=&orderby=creationdatetime" + filterCriteriaQuery, {})
                .done(function (data) {
                })

                .success(function (data) {
                    var missing = 0;
                    var found = 0;

                    $.each(data, function (key, val) {
                        if (val.Status === "Found")
                            found++;
                        else if (val.Status === "Missing")
                            missing++;
                    });
                    $("#children-count-missing").text(missing);
                    $("#children-count-found").text(found);
                });
        }

        function renderPager() {
            $.getJSON("./api/children/info?page=" + page + "&limit=24&haspicture=true&orderby=creationdatetime" + filterCriteriaQuery, {})
                .done(function (data) {
                })

                .success(function (data) {
                    var pager = $("#pager-gallery");
                    pager.empty();

                    if (data.TotalPages <= 1)
                        return;

                    if (page == 1) {
                        pager.append('<li class="disabled"><a>Prev</a></li>');
                    } else {
                        pager.append('<li class="pager-prev"><a href="#">Prev</a></li>');
                    }

                    for (var i = 1; i <= data.TotalPages; i++) {

                        pager.append('<li data-page="' + i + '" class="pager-page ' + (i == page ? 'active' : '') + '"><a href="#">' + i + '</a></li>');
                    }

                    if (page == data.TotalPages) {
                        pager.append('<li class="disabled"><a>Next</a></li>');
                    } else {
                        pager.append('<li class="pager-next"><a href="#">Next</a></li>');
                    }
                });
        }


        function renderTimeMap() {
            timeMap = TimeMap.init({
                mapId: "map",
                timelineId: "timeline",
                options: {
                    eventIconPath: "./Scripts/TimeMap/Images/"
                },
                datasets: [
                    {
                        type: "json_string",

                        options: {
                            url: "api/timemap/children"
                        }
                    }
                ],
                bandInfo: [
                    {
                        width: "80%",
                        intervalUnit: Timeline.DateTime.MONTH,
                        intervalPixels: 125,
                        showEventText: true
                    },
                     {
                         width: "20%",
                         intervalUnit: Timeline.DateTime.YEAR,
                         intervalPixels: 125,
                         showEventText: false,
                         trackHeight: 0.2,
                         trackGap: 0.2,
                     }
                ]
            });
        }

        // validations
        $("#modal-add-child-form").validate({
            rules: {
                name: {
                    minlength: 3,
                    required: true
                },
                birthday: {
                    required: true
                },
                description: {
                    maxlength: 1000
                },
                mscountry: {
                    required: true
                },
                msdatetime: {
                    required: true
                },
                msdescription: {
                    maxlength: 1000
                }
            },
            messages: {

            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#modal-signup-form").validate({
            rules: {
                name: {
                    minlength: 3,
                    required: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                email: {
                    required: true,
                    email: true,

                    remote: {
                        url: "./api/validations/email",
                        type: "post",
                        data: {
                            email: function () {
                                return $("#modal-signup-form [name=email]").val();
                            }
                        }
                    }
                }
            },
            messages: {
                email: {
                    remote: "This email address is already in use."
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#modal-signin-form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });


        // initialize on load
        $(document).ready(function (event) {

            authInfo = getAuthInfo();
            listChildren();
            renderPager();
            //renderTimeMap();

            $("#modal-signup-form").on("submit", function (e) {
                e.preventDefault();
                if ($("#modal-signup-form").valid()) {
                    signUp();
                }
            });

            $("#modal-signin-form").on("submit", function (e) {
                e.preventDefault();

                if ($("#modal-signin-form").valid()) {
                    signIn();
                }
            });


            $("#modal-add-child-form").on("submit", function (e) {
                e.preventDefault();
                if ($("#modal-add-child-form").valid()) {
                    addChild();
                }
            });

            $("#btn-signout").on("click", function (e) {
                e.preventDefault();
                signOut();
            });


            $("#pager-gallery").on("click", ".pager-page", function (e) {
                e.preventDefault();
                var newPage = $(this).data("page");
                if (newPage > page)
                    pagerAnimateDirection = "right";
                else
                    pagerAnimateDirection = "left";
                page = newPage;
                listChildren();
                renderPager();
            });

            $("#pager-gallery").on("click", ".pager-prev", function (e) {
                e.preventDefault();
                page = page - 1;
                pagerAnimateDirection = "left";
                listChildren();
                renderPager();
            });

            $("#pager-gallery").on("click", ".pager-next", function (e) {
                e.preventDefault();
                page = page + 1;
                pagerAnimateDirection = "right";
                listChildren();
                renderPager();
            });

            $("#container-gallery").on("click", ".thumbnail", function (e) {
                e.preventDefault();
                var childId = $(this).data("id");

                $.getJSON("./api/children/" + childId, {})
                    .success(function (data) {
                        $("#modal-child-info").find(".thumbnail").attr("src", "./api/children/" + data.ID + "/picture?width=128&height=128");
                        $("#modal-child-info-btn-profile").attr("href", "./ChildProfile.aspx?id=" + data.ID);
                        $("#modal-child-info").find(".media-body").children().remove("table");

                        $("#modal-child-info").find(".media-heading").html(data.Name);

                        $("#modal-child-info").find(".media-body")
                            .append(
                            "<table>" +
                            "<tbody>" +
                            "<tr>" + "<th>Gender:&nbsp;</th>" + "<td>" + data.Gender + "</td>" + "</tr>" +
                            "<tr>" + "<th>Birthday:&nbsp;</th>" + "<td>" + new Date(data.Birthday).toString("d MMMM yyyy (") + "<abbr title='" + data.Age + " years old as today.'>" + data.Age + " years old</abbr>)</td>" + "</tr>" +
                            "<tr>" + "<th>Added On:&nbsp;</th>" + "<td>" + new Date(data.CreationDateTime).toString("d MMMM yyyy - h:mm tt") + "</td>" + "</tr>" +
                            "<tr>" + "<th>Missing On:&nbsp;</th>" + "<td>" + new Date(data.MissingDateTime).toString("d MMMM yyyy - h:mm tt (") + "<abbr title='" + data.MissingAge + " years old as missing time.'>" + data.MissingAge + " years old</abbr>)</td>" + "</tr>" +
                            "<tr>" + "<th>Missing Location:&nbsp;</th>" + "<td>" + "<a target='_blank' href='https://maps.google.com/maps?z=6&q=" + data.MissingCountry.Latitude + "," + data.MissingCountry.Longitude + "'> " + data.MissingCountry.Name + "</td>" + "</tr>" +
                            "<tr>" + "<th>Owner:&nbsp;</th>" + "<td>" + "<a href='./UserProfile.aspx?id=" + data.Owner.ID + "'>" + data.Owner.Name + "</a>" + "</td>" + "</tr>" +
                            "<tr>" + "<th>Status:&nbsp;</th>" + "<td>" + data.Status + "</td>" + "</tr>" +
                            "</tbody>" +
                            "</table>");

                        $("#modal-child-info").modal("show");
                    })
                    .fail(function () {
                        // will this fail?
                    });
            });


            $("#btn-clearfilter-modal").on("click", function (e) {
                //
                e.preventDefault();
                resetFilter();
            });

            $("#btn-clearfilter").on("click", function (e) {
                //
                e.preventDefault();
                resetFilter();
            });

            $("#modal-filter-form").on("submit", function (e) {
                //
                e.preventDefault();
                applyFilter();
            });


            // -- end --
        });

    </script>

</body>

</html>
